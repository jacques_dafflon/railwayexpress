package ch.usi.inf.saiv.railwayempire.model.trains;


import java.io.Serializable;

import ch.usi.inf.saiv.railwayempire.model.stores.IBuyable;

/** Interface describing train parts (wagons/locomotives). */
public interface TrainComponent extends IBuyable, Serializable {

    /**
     * Must return the name of the component.
     * @return the name of the component.
     */
    public String getName();

    /** Return the name of the texture of the component.
     *
     * @return The name of the texture.
     */
    public String getTextureName();
}
