package ch.usi.inf.saiv.railwayempire.gui.viewers;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.structures.Track;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.model.trains.TrainManager;
import ch.usi.inf.saiv.railwayempire.model.trains.TrainState;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.utilities.Coordinate;
import ch.usi.inf.saiv.railwayempire.utilities.IPersistent;
import ch.usi.inf.saiv.railwayempire.utilities.Pair;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;


/**
 * Represents the viewer for the trains int the game.
 */
public final class TrainViewer implements IPersistent {

    /**
     * The train manager.
     */
    private TrainManager trainManager;
    /**
     * The coordinates manager.
     */
    private CoordinatesManager coordsManager;

    /**
     * Creates a new train viewer.
     */
    public TrainViewer() {
        this.trainManager = Game.getInstance().getWorld().getTrainManager();
        this.coordsManager = CoordinatesManager.getInstance();
        Game.addPersistent(this);
    }

    /**
     * Maps train coordinates and train components.
     *
     * @return
     *      Train coordinate and train component mapping.
     */
    public Map<Coordinate, List<Pair<Train, Integer>>> getComponentsToRender() {
        final Map<Coordinate, List<Pair<Train, Integer>>> components =
                new HashMap<Coordinate, List<Pair<Train,Integer>>>();

        for (final Train train : this.trainManager.getTrains()) {
            if (this.coordsManager.isPositionInScreenRectangle(train.getCurrentX(), train.getCurrentY()) ||
                    this.coordsManager.isPositionInScreenRectangle(train.getNextX(), train.getNextY())) {
                final Track curr = train.getCurrentTrack();

                final Coordinate locCoordinate = new Coordinate(curr.getX(), curr.getY());
                final Pair<Train, Integer> locPair = new Pair<Train, Integer>(train, null);

                if (components.containsKey(locCoordinate)) {
                    components.get(locCoordinate).add(locPair);
                } else {
                    final List<Pair<Train, Integer>> pairList = new ArrayList<Pair<Train, Integer>>();
                    pairList.add(locPair);
                    components.put(locCoordinate, pairList);
                }

                for (int i = 0; i < train.getPreviousTracks().size() - 1; ++i) {
                    if (train.getState() == TrainState.MOVING || train.getState() == TrainState.WAITING) {
                        final Track wagonTrack = train.getPreviousTracks().get(i);
                        final Coordinate wagonCoordinate = new Coordinate(wagonTrack.getX(), wagonTrack.getY());
                        final Pair<Train, Integer> wagonPair = new Pair<Train,Integer>(train, i);

                        if (components.containsKey(wagonCoordinate)) {
                            components.get(wagonCoordinate).add(wagonPair);
                        } else {
                            final List<Pair<Train, Integer>> pairList = new ArrayList<Pair<Train, Integer>>();
                            pairList.add(wagonPair);
                            components.put(wagonCoordinate, pairList);
                        }
                    }
                }
            }
        }
        return components;
    }

    private Vector2f averagePoint(final Vector2f first, final Vector2f second) {
        return new Vector2f((first.getX() + second.getX()) / 2, (first.getY() + second.getY()) / 2);
    }

    /**
     * Draws locomotive.
     *
     * @param train
     *          The train for which to draw locomotive.
     */
    public void drawLocomotive(final Train train) {
        final Terrain terrain = Game.getInstance().getWorld().getTerrain();

        final ICell currCell = terrain.getCell(train.getCurrentX(), train.getCurrentY());
        final ICell nextCell = terrain.getCell(train.getNextX(), train.getNextY());
        final ICell prevCell;
        if (train.getPreviousTrack() == null) {
            prevCell = currCell;
        } else {
            prevCell = terrain.getCell(train.getPreviousX(), train.getPreviousY());
        }

        final Vector2f curr = this.coordsManager.getCoordinatesByPosition(train.getCurrentX(),
                train.getCurrentY());
        curr.set(curr.getX(), curr.getY() - this.coordsManager.getHeightOffset(currCell));

        final Vector2f prev;
        if (train.getPreviousTrack() == null) {
            prev = curr;
        } else {
            prev = this.coordsManager.getCoordinatesByPosition(train.getPreviousX(), train.getPreviousY());
            prev.set(prev.getX(), prev.getY() - this.coordsManager.getHeightOffset(prevCell));
        }

        final Vector2f next = this.coordsManager.getCoordinatesByPosition(train.getNextX(), train.getNextY());
        next.set(next.getX(), next.getY() - this.coordsManager.getHeightOffset(nextCell));

        final String textureName = train.getLocomotive().getTextureName() + train.getOrientation();
        final Image texture = ResourcesLoader.getInstance().getImage(textureName);

        this.drawComponent(prev, curr, next, train, texture);
    }

    /**
     * Draws wagons.
     *
     * @param train
     *          the train for which to draw the wagon.
     * @param index
     *          the position of the wagon in the train.
     */
    public void drawWagon(final Train train, final int index) {
        final Terrain terrain = Game.getInstance().getWorld().getTerrain();
        final IWagon wagon = train.getWagons().get(index);

        final Track currTrack = train.getPreviousTracks().get(index);
        final ICell currCell = terrain.getCell(currTrack.getX(), currTrack.getY());

        final Track prevTrack = train.getPreviousTracks().get(index + 1);
        final ICell prevCell = terrain.getCell(prevTrack.getX(), prevTrack.getY());

        final Track nextTrack;
        if (index == 0) {
            nextTrack = train.getCurrentTrack();
        } else {
            nextTrack = train.getPreviousTracks().get(index - 1);
        }
        final ICell nextCell = terrain.getCell(nextTrack.getX(), nextTrack.getY());

        final Vector2f curr = this.coordsManager.getCoordinatesByPosition(currTrack.getX(), currTrack.getY());
        curr.set(curr.getX(), curr.getY() - this.coordsManager.getHeightOffset(currCell));

        final Vector2f prev = this.coordsManager.getCoordinatesByPosition(prevTrack.getX(), prevTrack.getY());
        prev.set(prev.getX(), prev.getY() - this.coordsManager.getHeightOffset(prevCell));

        final Vector2f next = this.coordsManager.getCoordinatesByPosition(nextTrack.getX(), nextTrack.getY());
        next.set(next.getX(), next.getY() - this.coordsManager.getHeightOffset(nextCell));

        String orientation = "_";
        if (nextTrack.getY() != prevTrack.getY()) {
            orientation += nextTrack.getY() < prevTrack.getY() ? "N" : "S";
        }
        if (nextTrack.getX() != prevTrack.getX()) {
            orientation += prevTrack.getX() < nextTrack.getX() ? "E" : "W";
        }

        final String textureName = wagon.getTextureName() + orientation;
        final Image texture = ResourcesLoader.getInstance().getImage(textureName);

        this.drawComponent(prev, curr, next, train, texture);
    }

    private void drawComponent(final Vector2f prev, final Vector2f curr, final Vector2f next,
            final Train train, final Image texture) {
        if (this.coordsManager.areCoordinatesOnScreen(curr) ||
                this.coordsManager.areCoordinatesOnScreen(next)) {

            final Vector2f first = this.averagePoint(prev, curr);
            final Vector2f second = this.averagePoint(curr, next);

            final float posX = first.getX() + (second.getX() - first.getX()) * train.getDistanceCompleted();
            final float posY = first.getY() + (second.getY() - first.getY()) * train.getDistanceCompleted();
            final Vector2f pos = new Vector2f(posX, posY);

            pos.set(pos.getX() + 10 * this.coordsManager.getScale(), pos.getY() - 10 * this.coordsManager
                    .getScale());

            if (train.getState() == TrainState.WAITING) {
                texture.setAlpha(0.5f);
            } else {
                texture.setAlpha(1.0f);
            }

            texture.draw(pos.getX(), pos.getY(), 0.2f * this.coordsManager.getScale());
        }
    }

    @Override
    public void reload() {
        this.trainManager = Game.getInstance().getWorld().getTrainManager();
        this.coordsManager = CoordinatesManager.getInstance();
    }
}
