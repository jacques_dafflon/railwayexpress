/**
 * Provides the application of different states. Purpose of the state is to enable functionality
 * which is designed only for the state in which the application is currently operating.
 *
 * EndGameState signals the application that it has entered this state and is ready to display and provide
 * the functionality specific only when the game has been ended, in this case when player chose to sell company.
 *
 * GameplayState signals the application that it has entered this state and is ready to display and provide
 * the functionality specific only to the actual in game.
 *
 * MainMenuState signals the application that it has entered this state and is ready to display and provide
 * the functionality specific only to the main menu.
 *
 */
package ch.usi.inf.saiv.railwayempire.gui.states;