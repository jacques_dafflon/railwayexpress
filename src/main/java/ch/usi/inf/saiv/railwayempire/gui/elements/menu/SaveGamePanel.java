package ch.usi.inf.saiv.railwayempire.gui.elements.menu;

import java.io.IOException;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.SaveLoadController;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;

/**
 * Represent a save game to be render in the saveLoadPanel.
 */
public class SaveGamePanel extends AbstractPanel {

    /**
     * The margin.
     */
    private static int MARGIN = 10;

    /**
     * The name of the file.
     */
    private final String name;

    /**
     * The controller for notifying the container panel
     */
    private final SaveLoadController controller;

    /**
     * True if the save game is currently saved or not.
     */
    private boolean selected;

    /**
     * Object containing the information about the savegame.
     */
    private final SaveGameInfo info;

    /**
     * Constructor for the class. Initialize all the graphical elements.
     * 
     * @param newShape
     *            the shape of the panel.
     * @param saveName
     *            the name of the save game.
     * @param saveLoadController
     *            the controller to handle everything.
     * @param container
     *            slick container
     * @throws IOException
     *             can throw an exception if the file doesn't exist or it's
     *             corrupted.
     */
    public SaveGamePanel(final Shape newShape, final String saveName, final SaveLoadController saveLoadController,
            final GameContainer container) throws IOException {
        super(newShape);

        final int labelWidth = (int) (newShape.getWidth() - newShape.getHeight());
        final int labelHeight = container.getGraphics().getFont().getLineHeight();
        this.info = new SaveGameInfo(saveName);
        int labelOffset = 0;
        final Label nameLabel = new Label(this.getX() + SaveGamePanel.MARGIN, this.getY() + SaveGamePanel.MARGIN,
                labelWidth,
                labelHeight, saveName);
        labelOffset += labelHeight;
        final Label playerNamelabel = new Label(this.getX() + SaveGamePanel.MARGIN, this.getY() + SaveGamePanel.MARGIN
                + labelOffset, labelWidth, labelHeight, this.info.getPlayerName());
        labelOffset += labelHeight;
        final Label companyNamelabel = new Label(this.getX() + SaveGamePanel.MARGIN, this.getY() + SaveGamePanel.MARGIN
                + labelOffset, labelWidth, labelHeight, this.info.getCompanyName());
        labelOffset += labelHeight;
        final Label moneyLabel = new Label(this.getX() + SaveGamePanel.MARGIN, this.getY() + SaveGamePanel.MARGIN
                + labelOffset, labelWidth, labelHeight, this.info.getMoney());
        labelOffset += labelHeight;
        final Label timeLabel = new Label(this.getX() + SaveGamePanel.MARGIN,
                this.getY() + SaveGamePanel.MARGIN + labelOffset, labelWidth, labelHeight, this.info.getTime());

        labelOffset -= labelHeight;
        final Label ownedTrainslabel = new Label(this.getX() + SaveGamePanel.MARGIN, this.getY() + SaveGamePanel.MARGIN
                + labelOffset, labelWidth, labelHeight, this.info.getOwnedTrains());
        ownedTrainslabel.setAlignment(Alignment.RIGHT);
        labelOffset += labelHeight;
        final Label ownedStationslabel = new Label(this.getX() + SaveGamePanel.MARGIN, this.getY()
                + SaveGamePanel.MARGIN
                + labelOffset, labelWidth, labelHeight, this.info.getOwnedStations());
        ownedStationslabel.setAlignment(Alignment.RIGHT);

        this.add(nameLabel);
        this.add(playerNamelabel);
        this.add(companyNamelabel);
        this.add(moneyLabel);
        this.add(timeLabel);
        this.add(ownedTrainslabel);
        this.add(ownedStationslabel);

        this.name = saveName;
        this.controller = saveLoadController;
        this.selected = false;



    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        if (this.selected) {
            graphics.setColor(Color.yellow);
            graphics.drawRect(this.getX(), this.getY(), this.getWidth() - 1, this.getHeight() - 1);
        } else {
            graphics.setColor(Color.black);
            graphics.drawRect(this.getX(), this.getY(), this.getWidth() - 1, this.getHeight());
        }
        super.render(container, graphics);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        super.mousePressed(button, coordX, coordY);
        this.controller.signal(this);

    }

    /**
     * Return the name of the savegame.
     * 
     * @return the name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Notify that the savegame is actually selected.
     */
    public void select() {
        this.selected = true;
    }

    /**
     * Notify that the save game is no longer selected.
     */
    public void deselect() {
        this.selected = false;
    }
}