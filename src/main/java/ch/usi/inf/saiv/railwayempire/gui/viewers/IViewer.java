package ch.usi.inf.saiv.railwayempire.gui.viewers;


import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;

/**
 * Viewer.
 */
public interface IViewer {

    /**
     * Method to render the content of which the viewer is responsible of.
     *
     * @param container
     *         container.
     * @param graphics
     *         The graphics on which to draw.
     */
    void render(GUIContext container, Graphics graphics);
}
