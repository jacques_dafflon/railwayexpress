package ch.usi.inf.saiv.railwayempire.model.stores;

/**
 * Class to describe buyable things.
 */
public interface IBuyable {

    /**
     * Return the cost of the object.
     *
     * @return The cost.
     */
    int getCost();
}
