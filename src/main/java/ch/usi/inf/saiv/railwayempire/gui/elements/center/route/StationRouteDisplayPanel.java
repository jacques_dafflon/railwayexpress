package ch.usi.inf.saiv.railwayempire.gui.elements.center.route;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Display a station as part of a train's route.
 * 
 */
public class StationRouteDisplayPanel extends StationDisplayPanel {
    
    /**
     * The panel managing the train route.
     */
    private final TrainRoutePanel panel;
    
    /**
     * Icon of the station.
     */
    private Image icon;
    
    /**
     * State of the station (represented by the panel) within the train's route.
     * 
     */
    private enum State {
        /**
         * Normal station in the route.
         */
        NORMAL,
            /**
             * First station in the route.
             */
            FIRST,
            /**
             * Last station in the route.
             */
            LAST,
            /**
             * Single station in the route.
             */
            SINGLE
    }
    
    /**
     * Current state of the station in the route.
     */
    private State state;
    
    /**
     * Instantiate a new panel to represent a station in the train's route.
     * 
     * @param rectangle
     *            Position and location of the panel as a rectangle.
     * @param displayStation
     *            The station to display.
     * @param trainRoutePanel
     *            The panel holding this panel.
     */
    public StationRouteDisplayPanel(final Rectangle rectangle,
        final Station displayStation,
        final TrainRoutePanel trainRoutePanel) {
        
        super(rectangle, displayStation);
        this.panel = trainRoutePanel;
        this.state = State.NORMAL;
        this.updateIcon();
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        this.icon.draw(this.getX(), this.getY());
    }
    
    /**
     * Indicate to the panel that this station is the first in the route.
     */
    public void setFirst() {
        switch (this.state) {
            case FIRST:
                break;
            case LAST:
                this.state = State.SINGLE;
                break;
            case SINGLE:
            case NORMAL:
                this.state = State.FIRST;
                break;
            default:
                break;
        
        }
        this.updateIcon();
    }
    
    /**
     * Indicate to the panel that this station is the last in the route.
     */
    public void setLast() {
        switch (this.state) {
            case FIRST:
                this.state = State.SINGLE;
                break;
            case NORMAL:
            case SINGLE:
                this.state = State.LAST;
            case LAST:
                break;
            default:
                break;
        }
        this.updateIcon();
    }
    
    /**
     * Indicate to the panel that this station is a normal station in the route.
     */
    public void setNormal() {
        this.state = State.NORMAL;
        this.updateIcon();
    }
    
    /**
     * Update the icon based on the state.
     * 
     * This should be called every time the state is changed.
     */
    private void updateIcon() {
        this.icon = ResourcesLoader.getInstance().getImage("STATION_" + this.getStation().getSize()
            + "_ROUTE_ICON_"
            + this.state);
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    protected float getTextXOffset() {
        return this.icon.getWidth();
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        if (this.getShape().contains(newX, newY)) {
            this.panel.highlightStation(this.getStation());
        } else if (this.getShape().contains(oldX, oldY)) {
            this.panel.unhighlightStation(this.getStation());
        }
    }
}
