package ch.usi.inf.saiv.railwayempire.model.generators;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.geom.Vector2f;

import ch.usi.inf.saiv.railwayempire.model.City;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.production_sites.CoalMine;
import ch.usi.inf.saiv.railwayempire.model.production_sites.Farm;
import ch.usi.inf.saiv.railwayempire.model.production_sites.FoodFactory;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IronMine;
import ch.usi.inf.saiv.railwayempire.model.production_sites.OilRig;
import ch.usi.inf.saiv.railwayempire.model.production_sites.RockCave;
import ch.usi.inf.saiv.railwayempire.model.production_sites.SawMill;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;


/**
 * The production site generator is responsible for the creation
 * of different production sites across terrain in realistic/random way.
 */
public final class ProductionSiteGenerator {

    /**
     * A container for all generated production sites which are not spawned on terrain.
     */
    private static final List<IProductionSite> productionSites = new ArrayList<IProductionSite>();
    /**
     * Contains the list of cities which have been taken form the passed list
     * of the class only public method.
     */
    private static final List<City> cities = new ArrayList<City>();

    /**
     * Empty constructor for the production site generator.
     */
    private ProductionSiteGenerator() {
    }

    /**
     * Starting point of the class. It launches two other methods which generates
     * a list of production sites on the given terrain.
     *
     * @param cityList
     *         A list of cities.
     * @param terrain
     *         The terrain to generate on.
     * @return a list of all generated production sites.
     */
    public static List<IProductionSite> generateProductionSites(final List<City> cityList, final Terrain terrain) {
        if (terrain == SystemConstants.NULL) {
            throw new IllegalArgumentException("The terrain on which to place production sites cannot be null!");
        }
        for (final City city : cityList) {
            ProductionSiteGenerator.cities.add(city);
        }
        ProductionSiteGenerator.generateRawMaterialSites(terrain);
        ProductionSiteGenerator.generateMaterialSites(terrain);
        return ProductionSiteGenerator.productionSites;
    }

    /**
     * Method for generating a list of production sites which use
     * raw materials in order to produce materials on the given terrain.
     *
     * @param terrain
     *         The terrain to generate on.
     */
    private static void generateMaterialSites(final Terrain terrain) {
        final Random random = new Random();
        final int foodFactory = random.nextInt(GameConstants.MAX_CITIES - GameConstants.MIN_CITIES + 1)
                + GameConstants.MIN_CITIES;
        for (int counter = 0; counter < foodFactory; counter++) {
            final IProductionSite site = ProductionSiteGenerator.generateFoodFactory(terrain);
            if (site != null) {
                ProductionSiteGenerator.spawnProductionSite(terrain, site);
                ProductionSiteGenerator.productionSites.add(site);
            }
        }
    }

    /**
     * Method for generating a list of production sites who produce
     * only raw materials on the given terrain.
     *
     * @param terrain
     *         The terrain to generate on.
     */
    private static void generateRawMaterialSites(final Terrain terrain) {
        // compute the amount of production sites to be placed.
        final Random random = new Random();
        final int sawMills =
                random.nextInt(GameConstants.MAX_SAW_MILL - GameConstants.MIN_SAW_MILL + 1)
                        + GameConstants.MIN_SAW_MILL;
        final int coalMines =
                random.nextInt(GameConstants.MAX_COAL_MINE - GameConstants.MIN_COAL_MINE + 1)
                        + GameConstants.MIN_COAL_MINE;
        final int rockCaves =
                random.nextInt(GameConstants.MAX_ROCK_CAVE - GameConstants.MIN_ROCK_CAVE + 1)
                        + GameConstants.MIN_ROCK_CAVE;
        final int farms = random.nextInt(GameConstants.MAX_FARM - GameConstants.MIN_FARM + 1) + GameConstants.MIN_FARM;
        final int ironMines =
                random.nextInt(GameConstants.MAX_IRON_MINE - GameConstants.MIN_IRON_MINE + 1)
                        + GameConstants.MIN_IRON_MINE;
        final int drillRigs =
                random.nextInt(GameConstants.MAX_DRILL_RIG - GameConstants.MIN_DRILL_RIG + 1)
                        + GameConstants.MIN_DRILL_RIG;

        // find the spot in the world and add the structure to the list and terrain.
        for (int counter = 0; counter < rockCaves; counter++) {
            final IProductionSite site = ProductionSiteGenerator.generateRockCave(terrain);
            if (site != null) {
                ProductionSiteGenerator.spawnProductionSite(terrain, site);
                ProductionSiteGenerator.productionSites.add(site);
            }
        }
        for (int counter = 0; counter < sawMills; counter++) {
            final IProductionSite site = ProductionSiteGenerator.generateSawMill(terrain);
            if (site != null) {
                ProductionSiteGenerator.spawnProductionSite(terrain, site);
                ProductionSiteGenerator.productionSites.add(site);
            }
        }
        for (int counter = 0; counter < coalMines; counter++) {
            final IProductionSite site = ProductionSiteGenerator.generateCoalMine(terrain);
            if (site != null) {
                ProductionSiteGenerator.spawnProductionSite(terrain, site);
                ProductionSiteGenerator.productionSites.add(site);
            }
        }
        for (int counter = 0; counter < farms; counter++) {
            final IProductionSite site = ProductionSiteGenerator.generateFarm(terrain);
            if (site != null) {
                ProductionSiteGenerator.spawnProductionSite(terrain, site);
                ProductionSiteGenerator.productionSites.add(site);
            }
        }
        for (int counter = 0; counter < ironMines; counter++) {
            final IProductionSite site = ProductionSiteGenerator.generateIronMine(terrain);
            if (site != null) {
                ProductionSiteGenerator.spawnProductionSite(terrain, site);
                ProductionSiteGenerator.productionSites.add(site);
            }
        }
        for (int counter = 0; counter < drillRigs; counter++) {
            final IProductionSite site = ProductionSiteGenerator.generateDrillRig(terrain);
            if (site != null) {
                ProductionSiteGenerator.spawnProductionSite(terrain, site);
                ProductionSiteGenerator.productionSites.add(site);
            }
        }
    }

    /**
     * Generates Food factory on the given terrain.
     *
     * @param terrain
     *         The terrain to generate on.
     * @return semi randomly generated Food factory or null if could not find a valid place.
     */
    private static IProductionSite generateFoodFactory(final Terrain terrain) {
        final Vector2f currentPos = ProductionSiteGenerator.findUniqueRandomCell(terrain, StructureTypes.FOOD_FACTORY);
        if (currentPos == null) {
            return null;
        }
        return new FoodFactory((int) currentPos.getX(), (int) currentPos.getY());
    }

    /**
     * Generates Drill rig on the given terrain.
     *
     * @param terrain
     *         The terrain to generate on.
     * @return semi randomly generated Drill rig or null if could not find a valid place.
     */
    private static IProductionSite generateDrillRig(final Terrain terrain) {
        final Vector2f currentPos = ProductionSiteGenerator.findUniqueRandomCell(terrain, StructureTypes.OIL_RIG);
        if (currentPos == null) {
            return null;
        }
        return new OilRig((int) currentPos.getX(), (int) currentPos.getY());
    }

    /**
     * Generates Iron mine on the given terrain.
     *
     * @param terrain
     *         The terrain to generate on.
     * @return semi randomly generated Iron mine or null if could not find a valid place.
     */
    private static IProductionSite generateIronMine(final Terrain terrain) {
        final Vector2f currentPos = ProductionSiteGenerator.findUniqueRandomCell(terrain, StructureTypes.IRON_MINE);
        if (currentPos == null) {
            return null;
        }
        return new IronMine((int) currentPos.getX(), (int) currentPos.getY());
    }

    /**
     * Generates Saw mills on the given terrain.
     *
     * @param terrain
     *         The terrain to generate on.
     * @return semi randomly generated Saw mill or null if could not find a valid place.
     */
    private static IProductionSite generateSawMill(final Terrain terrain) {
        final Vector2f currentPos = ProductionSiteGenerator.findUniqueRandomCell(terrain, StructureTypes.SAW_MILL);
        if (currentPos == null) {
            return null;
        }
        return new SawMill((int) currentPos.getX(), (int) currentPos.getY());
    }

    /**
     * Generates Farm on the given terrain.
     *
     * @param terrain
     *         The terrain to generate on.
     * @return semi randomly generated Farm or null if could not find a valid place..
     */
    private static IProductionSite generateFarm(final Terrain terrain) {
        final Vector2f currentPos = ProductionSiteGenerator.findUniqueRandomCell(terrain, StructureTypes.FARM);
        if (currentPos == null) {
            return null;
        }
        return new Farm((int) currentPos.getX(), (int) currentPos.getY());
    }

    /**
     * Generates Rock cave on the given terrain.
     *
     * @param terrain
     *         The terrain to generate on.
     * @return semi randomly generated Rock cave or null if could not find a valid place..
     */
    private static IProductionSite generateRockCave(final Terrain terrain) {
        final Vector2f currentPos = ProductionSiteGenerator.findUniqueRandomCell(terrain, StructureTypes.ROCK_CAVE);
        if (currentPos == null) {
            return null;
        }
        return new RockCave((int) currentPos.getX(), (int) currentPos.getY());
    }

    /**
     * Generates Coal mine on the given terrain.
     *
     * @param terrain
     *         The terrain to generate on.
     * @return semi randomly generated Coal mine or null if could not find a valid place..
     */
    private static IProductionSite generateCoalMine(final Terrain terrain) {
        final Vector2f currentPos = ProductionSiteGenerator.findUniqueRandomCell(terrain, StructureTypes.COAL_MINE);
        if (currentPos == null) {
            return null;
        }
        return new CoalMine((int) currentPos.getX(), (int) currentPos.getY());
    }

    /**
     * Method compares the distance in the world between two same type production sites.
     *
     * @param posX
     *         The x coordinate in the world.
     * @param posY
     *         The y coordinate in the world.
     * @param structureType
     *         The structure type.
     * @return if the production sites are too close, returns true.
     */
    private static boolean isProductionSiteNearby(final int posX, final int posY, final StructureTypes structureType) {
        final int distance = ProductionSiteGenerator.getMinDistance(structureType);
        for (final IProductionSite site : ProductionSiteGenerator.productionSites) {
            if (site.getStructureType() == structureType
                    && ProductionSiteGenerator.distanceFromTo(posX, posY, site.getX(), site.getY()) < distance) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method that check for which distance a production site distance should be checked.
     * The only distance we care is production sites which produce raw materials.
     * Production sites which generate materials from raw materials are generated
     * inside city or very close to it.
     *
     * @param structureType
     *         The type of the structure.
     * @return return the minimum distance between two of the same type production sites which produce raw materials.
     */
    private static int getMinDistance(final StructureTypes structureType) {
        int distance;
        switch (structureType) {
            case FARM:
                distance = GameConstants.MIN_FARM_DISTANCE;
                break;
            case COAL_MINE:
                distance = GameConstants.MIN_COAL_MINE_DISTANCE;
                break;
            case ROCK_CAVE:
                distance = GameConstants.MIN_ROCK_CAVE_DISTANCE;
                break;
            case SAW_MILL:
                distance = GameConstants.MIN_SAW_MILL_DISTANCE;
                break;
            case IRON_MINE:
                distance = GameConstants.MIN_IRON_MINE_DISTANCE;
                break;
            case OIL_RIG:
                distance = GameConstants.MIN_DRILL_RIG_DISTANCE;
                break;
            default:
                distance = 4;
                break;
        }
        return distance;
    }

    /**
     * Computes the distance between two structures.
     *
     * @param posX
     *         The x coordiante of the first structure.
     * @param posY
     *         The y coordinate of the first structure.
     * @param compareX
     *         The x coordinate of the second structure.
     * @param compareY
     *         The y coordinate of the second structure.
     * @return number which represents distance between two structures.
     */
    private static double distanceFromTo(final int posX, final int posY, final int compareX, final int compareY) {
        final double distanceX = Math.abs(posX - compareX);
        final double distanceY = Math.abs(posY - compareY);
        return Math.sqrt(distanceX * distanceX + distanceY * distanceY);
    }

    /**
     * Method is finding randomly a valid ground cell without a structure.
     * It must not be too close to other production site of the same type.
     *
     * @param terrain
     *         The terrain.
     * @return valid position in the world of the random cell or null if could not find a valid place.
     */
    private static Vector2f findUniqueRandomCell(final Terrain terrain, final StructureTypes structureType) {
        final Random random = new Random();
        int deadlock = 0;
        while (deadlock++ < 1000) {
            final int posX = random.nextInt(terrain.getSize());
            final int posY = random.nextInt(terrain.getSize());
            final ICell cell = terrain.getCell(posX, posY);
            if (cell.getCellType() == CellTypes.GROUND && !cell.containsStructure()
                    && ProductionSiteGenerator.isUniquePosition(posX, posY)
                    && !ProductionSiteGenerator.isProductionSiteNearby(posX, posY, structureType)
                    && ProductionSiteGenerator.isCorrectHeight(cell.getHeight(), structureType)
                    && ProductionSiteGenerator.isForestNerby(posX, posY, terrain, structureType)
                    && ProductionSiteGenerator.isWaterNearby(posX, posY, terrain, structureType)
                    && ProductionSiteGenerator.isCityNearby(posX, posY, structureType)) {
                return new Vector2f(posX, posY);
            }
        }
        return null;
    }

    /**
     * Method that check if saw mill has a forest in a neighbor cell.
     * If the structure is not saw mill, it returns true.
     *
     * @param posX
     *         The x coordinate in the world.
     * @param posY
     *         The y coordinate in the world.
     * @param terrain
     *         The terrain.
     * @param structureType
     *         The structure type.
     * @return true for saw mill if one of the neighbor cells has a forest, otherwise false. Other structures true.
     */
    private static boolean isForestNerby(final int posX, final int posY, final Terrain terrain,
                                         final StructureTypes structureType) {
        if (structureType == StructureTypes.SAW_MILL) {
            boolean ret = false;
            final List<ICell> cells = terrain.getNeighborCells(posX, posY);
            for (final ICell cell : cells) {
                if (cell.containsStructure() && cell.getStructure().isRemovable()) {
                    ret = true;
                }
            }
            return ret;
        } else {
            return true;
        }
    }

    /**
     * Method that check if there is water in neighbor cell.
     *
     * @param posX
     *         The x coordinate in the world.
     * @param posY
     *         The y coordinate in the world.
     * @param terrain
     *         The terrain.
     * @param structureType
     *         The structure type.
     * @return true for drill rig if one of the neighbor cells is water, otherwise false. Other structures true.
     */
    private static boolean isWaterNearby(final int posX, final int posY, final Terrain terrain,
                                         final StructureTypes structureType) {
        if (structureType == StructureTypes.OIL_RIG) {
            boolean ret = false;
            final List<ICell> cells = terrain.getNeighborCells(posX, posY);
            for (final ICell cell : cells) {
                if (cell.getCellType() == CellTypes.SEA || cell.getCellType() == CellTypes.RIVER) {
                    ret = true;
                }
            }
            return ret;
        } else {
            return true;
        }
    }

    /**
     * Method that check if there is water in neighbor cell.
     *
     * @param posX
     *         The x coordinate in the world.
     * @param posY
     *         The y coordinate in the world.
     * @param structureType
     *         The structure type.
     * @return true for drill rig if one of the neighbor cells is water, otherwise false. Other structures true.
     */
    private static boolean isCityNearby(final int posX, final int posY, final StructureTypes structureType) {
        boolean ret = false;
        if (structureType == StructureTypes.FOOD_FACTORY) {
            for (final City city : ProductionSiteGenerator.cities) {
                if (ProductionSiteGenerator.distanceFromTo(posX, posY, city.getX(), city.getY()) <= 8) {
                    ret = true;
                    break;
                }
            }
            return ret;
        }
        return true;
    }

    /**
     * Check if the cell height is appropriate for the structure type.
     *
     * @param height
     *         The cell height.
     * @param structureType
     *         The structure type.
     * @return true if cell height is appropriate otherwise false.
     */
    private static boolean isCorrectHeight(final int height, final StructureTypes structureType) {
        boolean ret;
        if (structureType == StructureTypes.FARM && height < 3) {
            ret = true;
        } else if ((structureType == StructureTypes.COAL_MINE || structureType == StructureTypes.ROCK_CAVE
                || structureType == StructureTypes.IRON_MINE) && height >= 4) {
            ret = true;
        } else if (structureType == StructureTypes.OIL_RIG && height == 0) {
            ret = true;
        } else ret = structureType == StructureTypes.SAW_MILL || structureType == StructureTypes.FOOD_FACTORY;
        return ret;
    }

    /**
     * Method checks if given position in the world is unique and
     * no other production site is going to take it when spawning.
     *
     * @param posX
     *         The x coordinate on terrain.
     * @param posY
     *         The y coordinate on terrain.
     * @return false if the position is not going to be unique, otherwise true.
     */
    private static boolean isUniquePosition(final int posX, final int posY) {
        for (final IProductionSite site : ProductionSiteGenerator.productionSites) {
            if (site.getX() == posX && site.getY() == posY) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method takes production site and places it on terrain.
     *
     * @param terrain
     *         The terrain.
     * @param site
     *         The production site to place.
     */
    private static void spawnProductionSite(final Terrain terrain, final IProductionSite site) {
        if (terrain == SystemConstants.NULL || site == SystemConstants.NULL) {
            throw new IllegalArgumentException("The terrain on which to place production sites cannot be null!");
        }
        final ICell cell = terrain.getCell(site.getX(), site.getY());
        cell.setStructure(site);
    }
}
