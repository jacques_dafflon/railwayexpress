/**
 * Provides the application with objects which are treated as raw materials from which some general good can be created.
 * Raw materials extend wares.
 *
 * AbstractRawMaterial is an abstract class which provides the basic functionality shared between all the raw wares.
 *
 * Coal is raw material produced by coal mines.
 *
 * Iron is raw material produced by iron mines.
 *
 * Oil is raw material produced by oil rigs.
 *
 * Rock is raw material produced by rock mines.
 *
 * Wood is raw material produced by saw mills.
 */
package ch.usi.inf.saiv.railwayempire.model.wares.materials;