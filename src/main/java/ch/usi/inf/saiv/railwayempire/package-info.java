/**
 * Provides the entry point for the application.
 * A class which has a main method which launches the whole application.
 */
package ch.usi.inf.saiv.railwayempire;