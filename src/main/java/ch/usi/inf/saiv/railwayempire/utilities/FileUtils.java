package ch.usi.inf.saiv.railwayempire.utilities;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * Utility class for file related methods.
 */
public class FileUtils {

    /**
     * Write to a file
     *
     * @param filename
     *         The name of the file to write to.
     * @param lines
     *         The lines to write in the file.
     * @return <code>true</code> if the write operation succeeded.
     */
    public static boolean writeToFile(final String filename, final List<String> lines) {
        try {
            final FileWriter fileWriter = new FileWriter(filename);
            final BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (final String line : lines) {
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
            return true;
        } catch (final IOException exception) {
            Log.exception(exception);
            return false;
        }
    }

    /**
     * Read a file.
     *
     * @param filename
     *         The name of the file to read from.
     * @return The lines of the file as a list of strings.
     */
    public static List<String> readFromFile(final String filename) {
        final List<String> readLines = new ArrayList<String>();

        final InputStream is = ClassLoader.getSystemResourceAsStream(filename);
        final Scanner scanner = new Scanner(is);
        while (scanner.hasNext()) {
            final String line = scanner.nextLine();
            if (line.length() > 0) {
                readLines.add(line);
            }
        }
        scanner.close();
        return readLines;
    }
}
