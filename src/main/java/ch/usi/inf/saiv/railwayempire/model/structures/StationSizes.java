package ch.usi.inf.saiv.railwayempire.model.structures;

/**
 * Enum for the stations.
 */
public enum StationSizes {
    /**
     * A small station.
     */
    SMALL,

    /**
     * A medium station.
     */
    MEDIUM,

    /**
     * A large station.
     */
    LARGE

}
