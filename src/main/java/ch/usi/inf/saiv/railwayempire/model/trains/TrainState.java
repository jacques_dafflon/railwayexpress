package ch.usi.inf.saiv.railwayempire.model.trains;

/**
 * State of the train.
 */
public enum TrainState {

    /**
     * Train is moving.
     */
    MOVING,

    /**
     * Train is trading.
     */
    TRADING,

    /**
     * Train is waiting.
     */
    WAITING,

    /**
     * Train is turning.
     */
    TURNING,

    /**
     * The train is stopped at the station before the next departure.
     */
    STOPPED,

    /**
     * train is parked in the warehouse.
     */
    PARKED

}
