package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Bottom Panel for build utilities.
 */
public final class BuildingCreationUtilityPanel extends AbstractWorldInfoUtilityPanel {
    /**
     * Distance between buttons.
     */
    private static final int BUTTONS_DISTANCE = 5;
    
    /**
     * Create the panel.
     * 
     * @param rectangle
     *            rectangle.
     * @param modesController
     *            modes controller.
     */
    public BuildingCreationUtilityPanel(final Rectangle rectangle, final GameModesController modesController) {
        super(rectangle);
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        
        // Coal mine.
        this.add(new Button(loader.getImage("COAL_MINE_BUTTON"),
            loader.getImage("COAL_MINE_BUTTON_HOVER"),
            loader.getImage("COAL_MINE_BUTTON_PRESSED"),
            loader.getImage("COAL_MINE_BUTTON_DISABLED"),
            this.getButtonX(0),
            this.getButtonY(0),
            new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterBuildingCreationMode(StructureTypes.COAL_MINE);
            }
        }));
        
        // Farm
        this.add(new Button(loader.getImage("FARM_BUTTON"),
            loader.getImage("FARM_BUTTON_HOVER"),
            loader.getImage("FARM_BUTTON_PRESSED"),
            loader.getImage("FARM_BUTTON_DISABLED"),
            this.getButtonX(1),
            this.getButtonY(0),
            new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterBuildingCreationMode(StructureTypes.FARM);
            }
        }));
        
        // FOOD_FACTORY
        this.add(new Button(loader.getImage("FOOD_FACTORY_BUTTON"),
            loader.getImage("FOOD_FACTORY_BUTTON_HOVER"),
            loader.getImage("FOOD_FACTORY_BUTTON_PRESSED"),
            loader.getImage("FOOD_FACTORY_BUTTON_DISABLED"),
            this.getButtonX(0),
            this.getButtonY(1),
            new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterBuildingCreationMode(StructureTypes.FOOD_FACTORY);
            }
        }));
        
        // Iron Mine
        this.add(new Button(loader.getImage("IRON_MINE_BUTTON"),
            loader.getImage("IRON_MINE_BUTTON_HOVER"),
            loader.getImage("IRON_MINE_BUTTON_PRESSED"),
            loader.getImage("IRON_MINE_BUTTON_DISABLED"),
            this.getButtonX(1),
            this.getButtonY(1),
            new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterBuildingCreationMode(StructureTypes.IRON_MINE);
            }
        }));
        
        // Oil Rig
        this.add(new Button(loader.getImage("OIL_RIG_BUTTON"),
            loader.getImage("OIL_RIG_BUTTON_HOVER"),
            loader.getImage("OIL_RIG_BUTTON_PRESSED"),
            loader.getImage("OIL_RIG_BUTTON_DISABLED"),
            this.getButtonX(0),
            this.getButtonY(2),
            new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterBuildingCreationMode(StructureTypes.OIL_RIG);
            }
        }));
        
        // Post Office
        this.add(new Button(loader.getImage("POST_OFFICE_BUTTON"),
            loader.getImage("POST_OFFICE_BUTTON_HOVER"),
            loader.getImage("POST_OFFICE_BUTTON_PRESSED"),
            loader.getImage("POST_OFFICE_BUTTON_DISABLED"),
            this.getButtonX(1),
            this.getButtonY(2),
            new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterBuildingCreationMode(StructureTypes.POST_OFFICE);
            }
        }));
        
        // Rock Cave.
        this.add(new Button(loader.getImage("ROCK_CAVE_BUTTON"),
            loader.getImage("ROCK_CAVE_BUTTON_HOVER"),
            loader.getImage("ROCK_CAVE_BUTTON_PRESSED"),
            loader.getImage("ROCK_CAVE_BUTTON_DISABLED"),
            this.getButtonX(0),
            this.getButtonY(3),
            new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterBuildingCreationMode(StructureTypes.ROCK_CAVE);
            }
        }));
        
        // Saw Mill
        this.add(new Button(loader.getImage("SAW_MILL_BUTTON"),
            loader.getImage("SAW_MILL_BUTTON_HOVER"),
            loader.getImage("SAW_MILL_BUTTON_PRESSED"),
            loader.getImage("SAW_MILL_BUTTON_DISABLED"),
            this.getButtonX(1),
            this.getButtonY(3),
            new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                modesController.enterBuildingCreationMode(StructureTypes.SAW_MILL);
            }
        }));
    }
    
    /**
     * Utility method to get the right x coordinate of buttons.
     * 
     * @param index
     *            column of button.
     * @return x coordinate.
     */
    private int getButtonX(final int index) {
        final int buttonWidth = ResourcesLoader.getInstance().getImage("FARM_BUTTON").getWidth();
        return this.getX() + buttonWidth * index + BuildingCreationUtilityPanel.BUTTONS_DISTANCE * index;
    }
    
    /**
     * Utility method to get the right y coordinate of buttons.
     * 
     * @param index
     *            row of button.
     * @return y coordinate.
     */
    private int getButtonY(final int index) {
        final int buttonHeight = ResourcesLoader.getInstance().getImage("FARM_BUTTON").getHeight();
        return this.getY() + buttonHeight * index + BuildingCreationUtilityPanel.BUTTONS_DISTANCE * index;
    }
}
