package ch.usi.inf.saiv.railwayempire.gui.elements;


import org.newdawn.slick.geom.Shape;


/**
 * Class representing a generic GUI component.
 * Responsibility of this class is mostly to hold its Shape and to provide access to it.
 */
public abstract class AbstractGuiComponent implements IGuiComponent {
    /**
     * Shape of the component.
     */
    private Shape shape;
    
    /**
     * Constructor.
     *
     * @param newShape
     *         shape of the component.
     */
    protected AbstractGuiComponent(final Shape newShape) {
        this.shape = newShape;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setLocation(final int newPosX, final int newPosY) {
        this.shape.setLocation(newPosX, newPosY);
    }
    
    /**
     * Get the shape.
     *
     * @return shape.
     */
    protected Shape getShape() {
        return this.shape;
    }
    
    /**
     * Sets the shape.
     */
    protected void setShape(final Shape newShape) {
        this.shape = newShape;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setX(final int newX) {
        this.shape.setX(newX);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setY(final int newY) {
        this.shape.setY(newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCenterX(final float newX) {
        this.shape.setCenterX(newX);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCenterY(final float newY) {
        this.shape.setCenterY(newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final boolean contains(final int coordX, final int coordY) {
        return this.shape.contains(coordX, coordY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final int getX() {
        return (int) this.shape.getX();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final int getY() {
        return (int) this.shape.getY();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final int getWidth() {
        return (int) this.shape.getWidth();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final int getHeight() {
        return (int) this.shape.getHeight();
    }
}
