package ch.usi.inf.saiv.railwayempire.model.structures;

/**
 * Types of track.
 */
public enum TrackTypes {
    /**
     * Normal track.
     */
    NORMAL,
    
    /**
     * Sea bridge track.
     */
    SEA_BRIDGE,
    
    /**
     * River bridge track.
     */
    RIVER_BRIDGE,
    
    /**
     * Tunnel.
     */
    TUNNEL;
}
