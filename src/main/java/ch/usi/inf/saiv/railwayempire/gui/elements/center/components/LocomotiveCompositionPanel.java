package ch.usi.inf.saiv.railwayempire.gui.elements.center.components;

import org.newdawn.slick.geom.Shape;

import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.trains.Locomotive;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.model.trains.TrainComponentFactory;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Display a locomotive, some details about it and allows the player to add it to the current train.
 * 
 */
public class LocomotiveCompositionPanel extends AbstractLocomotivePanel {
    
    /**
     * Panel displaying the current train.
     */
    private final CompositionEditorPanel trainPanel;
    
    /**
     * The button used to add the locomotive t the train.
     */
    private final Button button;
    
    /**
     * Create a new locomotive composition panel
     * 
     * @param shape
     *            The shape of the panel.
     * @param locomotive
     *            The locomotive to display and use.
     * @param trainPanel
     *            The panel containing the composition of the train
     */
    public LocomotiveCompositionPanel(final Shape shape,
        final Locomotive locomotive,
        final CompositionEditorPanel trainPanel) {
        super(shape, locomotive);
        
        this.trainPanel = trainPanel;
        
        final ResourcesLoader loader = ResourcesLoader.getInstance();
        this.button = new Button(loader.getImage("PLUS_BUTTON"),
            loader.getImage("PLUS_BUTTON_HOVER"),
            loader.getImage("PLUS_BUTTON_PRESSED"),
            loader.getImage("PLUS_BUTTON_DISABLED"),
            0, 0);
        
        this.button.setListener(new MouseListener() {
            
            /**
             * {@inheritDoc}
             */
            @Override
            public void actionPerformed(final Button button) {
                LocomotiveCompositionPanel.this.buttonAction();
            }
        });
        
        this.button.setLocation(this.getX() + this.getWidth() - AbstractComponentPanel.getPadding()
            - this.button.getWidth(), this.getY() + (this.getHeight() - this.button.getHeight()) / 2);
        this.add(this.button);
        
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction() {
        final Train train = this.trainPanel.getTrain();
        if (!this.isComponentAvailable()) {
            return;
        }
        if (null == train) {
            this.createNewTrain(this.getComponent());
        } else {
            Game.getInstance().getPlayer().getWareHouse().addLocomotive(train.getLocomotive().getName());
            train.setLocomotive(TrainComponentFactory.getInstance().getLocomotive(this.getComponent().getName()));
        }
        Game.getInstance().getPlayer().getWareHouse().removeLocomotive(this.getComponent().getName());
        
    }
    
    /**
     * Ask the TrainDisplayPanel to create a new train with the given locomotive.
     * 
     * This is used as a proxy for the {@link LocomotiveCompositionPanel}.
     * 
     * @param locomotive
     *            The locomotive used to make the new train.
     */
    public void createNewTrain(final Locomotive locomotive) {
        this.trainPanel.createNewTrain(locomotive);
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    protected Button getButton() {
        return this.button;
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public boolean isComponentAvailable() {
        return this.trainPanel.getTrain() == null && this.getComponentAvailability() > 0;
    }
    
}
