/**
 * Provides Graphical User Interface for the route panel where the user is able to assign routes to the train.
 */
package ch.usi.inf.saiv.railwayempire.gui.elements.center.route;