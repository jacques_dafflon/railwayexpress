package ch.usi.inf.saiv.railwayempire.model.cells;

import ch.usi.inf.saiv.railwayempire.model.structures.IStructure;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.stores.IBuyable;

/**
 * Interface for the cells.
 */
public interface ICell extends IBuyable {

    /**
     * Method which returns whether or not the cell contains a structure.
     * <p/>
     * By default the cell cannot contain any structure, therefore the method returns false. When the setStructure
     * method is overridden this method has to be overridden too, and the other way round.
     *
     * @return True if the cell contains a structure, false otherwise.
     */
    boolean containsStructure();

    /**
     * Get cell type.
     *
     * @return CellTypes type of the cell.
     */
    CellTypes getCellType();

    /**
     * Access the height of the cell.
     *
     * @return The height of the cell.
     */
    int getHeight();

    /**
     * Get the owner station.
     *
     * @return station that owns this cell.
     */
    Station getOwner();

    /**
     * Get the structure contained in the cell.
     *
     * By default the cell cannot contain any structure, therefore the method throws an exception. When the setStructure
     * method is overridden this method has to be overridden too, and the other way round.
     *
     * @return The structure contained in the cell.
     */
    IStructure getStructure();

    /**
     * Adds structures to the cell.
     *
     * By default structures cannot be added to the cells, the method has to be overridden by subclasses. When this
     * method is overridden also the containsStructure and getStructure methods have to be overridden.
     *
     * @param structure
     *            The building to put on the cell.
     */
    void setStructure(final IStructure structure);

    /**
     * Remove the current structure from the cell.
     */
    void removeStructure();

    /**
     * Returns the name of the texture.
     *
     * @return the name of the texture.
     */
    String getTextureName();

    /**
     * Check if the cell is blocked or not.
     *
     * @return true if the cell is blocked, false if it's not
     */

    /**
     * Get if this cell is owned by a station (if its in range).
     *
     * @return true if owned by station, false otherwise.
     */
    boolean isOwned();

    /**
     * Set the ownership of this cell. Set to null if ownership is removed.
     *
     * @param station
     *            The
     */
    void setOwner(final Station station);

    /**
     * Get the money required to build tracks in this cell.
     *
     * @return cost.
     */
    int getTrackCreationCost();

    /**
     * Check if we can create a track in this cell.
     *
     * @return true if we can build.
     */
    boolean canBuildTrack();

    /**
     * Check if we can create a building in the cell.
     *
     * @return true if we can build.
     */
    boolean canBuildBuilding();

    /**
     * Get description text on hover.
     *
     * @return text.
     */
    String getHoverText();

    /**
     * Returns the Cell as mountain cell, or null if this is not possible.
     * sorry for this little hack.
     *
     * @return mountains cell.
     */
    MountainCell asMountainCell();
}
