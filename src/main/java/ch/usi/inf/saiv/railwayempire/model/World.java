package ch.usi.inf.saiv.railwayempire.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.saiv.railwayempire.model.generators.CitiesGenerator;
import ch.usi.inf.saiv.railwayempire.model.generators.ProductionSiteGenerator;
import ch.usi.inf.saiv.railwayempire.model.generators.TerrainGenerator;
import ch.usi.inf.saiv.railwayempire.model.production_sites.Building;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.structures.StationManager;
import ch.usi.inf.saiv.railwayempire.model.trains.TrainManager;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.WorldSettings;

/**
 * World class.
 */
public final class World implements Serializable {

    /**
     * Serial version for Serializable implementation.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The manager of the trains in the world.
     */
    private final TrainManager trainManager;
    /**
     * Contains the list of the cities currently in the world.
     */
    private final List<City> cities;
    /**
     * Contains the list of the extraction sites currently in the world.
     */
    private final List<IProductionSite> productionSites;
    /**
     * A representation of the terrain of the world.
     */
    private final Terrain terrain;
    /**
     * The manager for the stations.
     */
    private final StationManager stationManager;

    /**
     * Creates a new representation of the game world.
     * World is the main class of the model part.
     * It contains the list of trains, cities and the terrain.
     *
     * @param worldSettings
     *            parameters for terrain generation.
     */
    public World(final WorldSettings worldSettings) {
        this.trainManager = new TrainManager();

        List<City> newCities = null;
        Terrain newTerrain = null;

        boolean tracksPlaced = false;

        while (newCities == SystemConstants.NULL || (newCities != null && newCities.size() < GameConstants.MIN_CITIES || !tracksPlaced)) {
            newTerrain = TerrainGenerator.generateTerrain(worldSettings);
            newCities = CitiesGenerator.generateCities(newTerrain);
            tracksPlaced = CitiesGenerator.placeStartingTracks(newTerrain, newCities);
            for (final City city : newCities) {
                CitiesGenerator.spawnBuildings(city, newTerrain);
            }
        }

        this.cities = newCities;
        this.terrain = newTerrain;

        this.productionSites = new ArrayList<IProductionSite>();
        this.stationManager = new StationManager();

        for (final City city : this.cities) {
            for (final Building building : city.getBuildings()) {
                this.addExtractionSite(building);
            }
        }

        final List<IProductionSite> newSites = ProductionSiteGenerator.generateProductionSites(this.cities, this.terrain);
        for (final IProductionSite site : newSites) {
            this.addExtractionSite(site);
        }
    }

    /**
     * Returns all the cities currently in the world.
     *
     * @return A List of the cities currently in the world.
     */
    public List<City> getCities() {
        return this.cities;
    }

    /**
     * Adds a city to the world.
     *
     * @param city
     *            The city to add to the world.
     */
    public void addCity(final City city) {
        if (this.isCollidingWithOtherCity(city)) {
            throw new IllegalArgumentException("There exist another city with those coordinates");
        }
        this.cities.add(city);
    }

    /**
     * Getter for the extraction points of the world.
     *
     * @return List of bananas.
     */
    public List<IProductionSite> getProductionSites() {
        return this.productionSites;
    }

    /**
     * Adds a new extraction site to the world list.
     *
     * @param extractionSite
     *            Site to add.
     */
    public void addExtractionSite(final IProductionSite extractionSite) {
        if (extractionSite == SystemConstants.NULL) {
            throw new IllegalArgumentException();
        }
        this.productionSites.add(extractionSite);
    }

    /**
     * Detect if there exist another city in that coordinates.
     *
     * @param city
     *            The city that you want to check.
     * @return True if a collision was detected, false otherwise.
     */
    public boolean isCollidingWithOtherCity(final City city) {
        for (final City otherCity : this.cities) {
            if (city.getX() == otherCity.getX()
                    && city.getY() == otherCity.getY()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return the terrain of the world.
     *
     * @return The terrain of the world.
     */
    public Terrain getTerrain() {
        return this.terrain;
    }

    /**
     * Getter for the train manager.
     *
     * @return The train manager.
     */
    public TrainManager getTrainManager() {
        return this.trainManager;
    }

    /**
     * Returns the station manager.
     *
     * @return The manager.
     */
    public StationManager getStationManager() {
        return this.stationManager;
    }

    /**
     * Custom toSting method.
     * @return
     *          String representing the World object.
     */
    @Override
    public String toString() {
        String string = "";
        for (final City city : this.getCities()) {
            string += "X: " + city.getX() + ", Y: " + city.getY() + "\n";
        }
        return string;
    }

}
