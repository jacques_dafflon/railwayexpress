package ch.usi.inf.saiv.railwayempire.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.usi.inf.saiv.railwayempire.model.datastructures.Graph;
import ch.usi.inf.saiv.railwayempire.model.datastructures.Matrix;
import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.Track;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * The terrain class handle the map of the game.
 */
public final class Terrain implements Serializable {
    /**
     * SerialVersionUID generated by eclipse.
     */
    private static final long serialVersionUID = -673188163408465607L;

    /**
     * Holds references to Cells.
     */
    private final Matrix<ICell> map;
    /**
     * Holds reference to the track graph.
     */
    private final Graph<Track> trackGraph;

    /**
     * Creates a new representation of the terrain.
     *
     * @param width
     *         The width of the world.
     * @param height
     *         The height of the world.
     */
    public Terrain(final int width, final int height) {
        this.map = new Matrix<ICell>(width, height);
        this.trackGraph = new Graph<Track>();
    }

    /**
     * Adds a cell to the map of the world at the given coordinates.
     *
     * @param posX
     *         The x coordinate of the cell to add.
     * @param posY
     *         The y coordinate of the cell to add.
     * @param cell
     *         The cell to add to the terrain.
     */
    public void addCell(final int posX, final int posY,
            final ICell cell) {
        this.checkCoordinates(posX, posY);
        this.map.set(posX, posY, cell);
    }

    /**
     * Returns the cell pointed by the coordinates if it exists.
     *
     * @param posX
     *         The x coordinate.
     * @param posY
     *         The y coordinate.
     * @return The cell at the given coordinates.
     */
    public ICell getCell(final int posX, final int posY) {
        this.checkCoordinates(posX, posY);
        return this.map.get(posX, posY);
    }

    /**
     * Return a list of cells in range radius of the given coordinates.
     * Center cell is included in such list.
     *
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     * @param radius
     *            radius.
     * @return list of cells.
     */
    public List<ICell> getCells(final int posX, final int posY, final int radius) {
        this.checkCoordinates(posX, posY);
        final List<ICell> cells = new ArrayList<ICell>();
        for (int y = posY - radius; y <= posY + radius; ++y) {
            for (int x = posX - radius; x <= posX + radius; ++x) {
                if (this.areCoordinatesInRange(x, y)) {
                    cells.add(this.getCell(x, y));
                }
            }
        }
        return cells;
    }

    /**
     * Return a list of cells in range radius of the given coordinates.
     * DOES NOT RETURN THE CENTRAL CELL!
     *
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     * @param radius
     *            radius.
     * @return ArrayList of cells in range.
     */
    public List<ICell> getCellsInRange(final int posX, final int posY, final int radius) {
        final List<ICell> cells = new ArrayList<ICell>();
        this.checkCoordinates(posX, posY);
        for (int y = posY - radius; y <= posY + radius; ++y) {
            for (int x = posX - radius; x <= posX + radius; ++x) {
                if ((y != posY || x != posX) && this.areCoordinatesInRange(x, y)) {
                    cells.add(this.getCell(x, y));
                }
            }
        }
        return cells;
    }

    /**
     * Returns the 4 close neighbor cells.
     *
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     * @return list of cells.
     */
    public List<ICell> getCloseNeighborCells(final int posX, final int posY) {
        final List<ICell> cells = new ArrayList<ICell>();
        for (int y = posY - SystemConstants.ONE; y <= posY + SystemConstants.ONE; ++y) {
            for (int x = posX - SystemConstants.ONE; x <= posX + SystemConstants.ONE; ++x) {
                if (this.areCoordinatesInRange(x, y) && Math.abs(y - posY) + Math.abs(x - posX) == SystemConstants.ONE) {
                    cells.add(this.getCell(x, y));
                }
            }
        }
        return cells;
    }

    /**
     * Return the 8 coordinates neighbors of the given coordinate.
     *
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     * @return list of neighbors of cells.
     */
    public List<ICell> getNeighborCells(final int posX, final int posY) {
        return this.getCellsInRange(posX, posY, 1);
    }

    /**
     * Set cell in a given coordinate.
     *
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     * @param cell
     *            new cell.
     */
    public void setCell(final int posX, final int posY, final ICell cell) {
        this.map.set(posX, posY, cell);
    }

    /**
     * Returns the size of one side of the terrain.
     *
     * @return The size of the terrain.
     */
    public int getSize() {
        return this.map.getWidth();
    }

    /**
     * Getter for the Track Graph.
     *
     * @return Track Graph
     */
    public Graph<Track> getTrackGraph() {
        return this.trackGraph;
    }

    /**
     * Checks whether the coordinates to access the map are correct and throws
     * exceptions in case they are not.
     *
     * @param posX
     *         The x coordinate.
     * @param posY
     *         The y coordinate.
     */
    private void checkCoordinates(final float posX, final float posY) {
        if (posX < SystemConstants.ZERO || posY < SystemConstants.ZERO) {
            throw new IllegalArgumentException("Cannot add cells "
                    + "to at a negative coordinate!");
        }
        if (posX > this.map.getWidth() || posY > this.map.getHeight()) {
            throw new IllegalArgumentException("Cannot add "
                    + "cells outside the map limit!");
        }
    }

    /**
     * Checks whether the coordinates to access the map are inside the boundaries of the current terrain.
     *
     * @param posX
     *         X coordinate to check.
     * @param posY
     *         Y coordinate to check.
     * @return true iff the coordinates are inside the terrain.
     */
    public boolean areCoordinatesInRange(final int posX, final int posY) {
        return posX >= SystemConstants.ZERO && posY >= SystemConstants.ZERO
                && posX < this.map.getWidth()
                && posY < this.map.getHeight();
    }

    /**
     * Checks if its possible to build a station at position x,y with radius.
     *
     * @param x
     *         x coordinate.
     * @param y
     *         y coordinate.
     * @param radius
     *         radius of influence.
     * @return true if its possible to build, false otherwise.
     */
    public boolean canBuildStation(final int x, final int y, final int radius) {
        boolean ret = true;
        final ICell centerCell = this.getCell(x, y);
        if (centerCell.getCellType() != CellTypes.GROUND) {
            return false;
        }
        if (centerCell.containsStructure()) {
            ret = centerCell.getStructure().isRemovable();
        }
        for (final ICell cell : this.getCellsInRange(x, y, radius)) {
            if (cell.isOwned()) {
                ret = false;
            }
        }
        return ret;
    }

    /**
     * Return a list of ExtractionSites in the radius of the given point. This will NOT include the point itself!
     *
     * @param x
     *         x coordinate.
     * @param y
     *         y coordinate.
     * @param radius
     *         radius of influence.
     * @return list of extraction sites in range.
     */
    public List<IProductionSite> getExtractionSites(final int x, final int y, final int radius) {
        final List<IProductionSite> retList = new ArrayList<IProductionSite>();
        for (final ICell cell : this.getCellsInRange(x, y, radius)) {
            if (cell.containsStructure() && cell.getStructure().asProductionSite() != SystemConstants.NULL) {
                retList.add(cell.getStructure().asProductionSite());
            }
        }
        return retList;
    }

    /**
     * Mark cells as owned by a new station.
     *
     * @param x
     *         x coordinate of station.
     * @param y
     *         y coordinate of station.
     * @param radius
     *         radius of station.
     * @param station
     *         the station.
     */
    public void markCellsAsOwned(final int x, final int y, final int radius, final Station station) {
        for (final ICell cell : this.getCellsInRange(x, y, radius)) {
            cell.setOwner(station);
        }
        this.getCell(x, y).setOwner(station);
    }
}
