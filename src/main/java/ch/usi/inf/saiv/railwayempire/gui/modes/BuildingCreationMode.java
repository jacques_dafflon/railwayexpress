package ch.usi.inf.saiv.railwayempire.gui.modes;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.GameModes;
import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.production_sites.CoalMine;
import ch.usi.inf.saiv.railwayempire.model.production_sites.Farm;
import ch.usi.inf.saiv.railwayempire.model.production_sites.FoodFactory;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IronMine;
import ch.usi.inf.saiv.railwayempire.model.production_sites.OilRig;
import ch.usi.inf.saiv.railwayempire.model.production_sites.PostOffice;
import ch.usi.inf.saiv.railwayempire.model.production_sites.RockCave;
import ch.usi.inf.saiv.railwayempire.model.production_sites.SawMill;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.Log;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.Time;

/**
 * Mode to handle user input and visualization of building creation.
 */
public final class BuildingCreationMode implements IMode {
    
    /**
     * The terrain.
     */
    private final Terrain terrain;
    /**
     * The x coordinate of the building being placed.
     */
    private int buildingX;
    /**
     * The y coordinate of the building being placed.
     */
    private int buildingY;
    /**
     * A flag indicating whether the current position is valid for a building.
     */
    private boolean isValid;
    /**
     * The modes controller.
     */
    private final GameModesController modesController;
    /**
     * Type of building to create.
     */
    private final StructureTypes structureType;
    /**
     * Texture of the structure to create.
     */
    private final String structureTexture;
    
    /**
     * Default empty construction.
     *
     * @param structure
     *            which structure to build.
     * @param controller
     *            The game modes controller.
     */
    public BuildingCreationMode(final StructureTypes structure, final GameModesController controller) {
        this.terrain = Game.getInstance().getWorld().getTerrain();
        this.modesController = controller;
        this.structureType = structure;
        this.structureTexture = this.getProductionSiteTexture(structure);
    }
    
    /**
     * Return the right texture name.
     *
     * @return texture.
     */
    private String getProductionSiteTexture(final StructureTypes structureType) {
        switch (structureType) {
            case COAL_MINE:
                return new CoalMine(0, 0).getTexture();
            case FARM:
                return new Farm(0, 0).getTexture();
            case FOOD_FACTORY:
                return new FoodFactory(0, 0).getTexture();
            case IRON_MINE:
                return new IronMine(0, 0).getTexture();
            case OIL_RIG:
                return new OilRig(0, 0).getTexture();
            case POST_OFFICE:
                return new PostOffice(0, 0).getTexture();
            case ROCK_CAVE:
                return new RockCave(0, 0).getTexture();
            case SAW_MILL:
                return new SawMill(0, 0).getTexture();
            default:
                throw new RuntimeException("Unreachable code reached");
        }
    }
    
    /**
     * Get the right cost for the structure type we are going to build.
     * 
     * @return cost.
     */
    private long getProductionSiteCost() {
        switch (this.structureType) {
            // different costs for different structures added should be.
            default:
                return GameConstants.POST_OFFICE_COST;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int posX, final int posY, final int clickCount) {
        final Vector2f coordinates = CoordinatesManager.getInstance().getPositionByCoordinates(posX, posY);
        if (button == SystemConstants.MOUSE_BUTTON_LEFT) {
            if (coordinates.getX() < SystemConstants.ZERO
                || coordinates.getY() < SystemConstants.ZERO
                || coordinates.getX() >= GameConstants.WORLD_SIZE
                || coordinates.getY() >= GameConstants.WORLD_SIZE) {
                return;
            }
            final ICell cell = Game.getInstance().getWorld().getTerrain().getCell((int) coordinates.getX(),
                (int) coordinates.getY());
            if (cell.getCellType() != CellTypes.GROUND) {
                Log.out("You have to build on ground!");
                Log.screen("You have to build on ground!");
            } else if (cell.getStructure() != null) {
                Log.out("There is already something here!");
                Log.screen("There is already something here!");
            } else if (!cell.isOwned()) {
                Log.out("You have to build in range of a station!");
                Log.screen("You have to build in range of a station!");
            } else if (!Game.getInstance().getPlayer().canAfford(GameConstants.POST_OFFICE_COST)) {
                Log.out("You cannot afford this!");
                Log.screen("You cannot afford this!");
            } else {
                final IProductionSite productionSite = this.createProductionSite((int) coordinates.getX(),
                    (int) coordinates.getY());
                productionSite.setOwnersName(Game.getInstance().getPlayer().getPlayerName());
                cell.setStructure(productionSite);
                Game.getInstance().getWorld().addExtractionSite(productionSite);
                cell.getOwner().addProductionSites(productionSite);
                Game.getInstance().getPlayer().subtractMoney(GameConstants.POST_OFFICE_COST);
                this.modesController.enterMode(GameModes.NORMAL);
            }
        }
    }
    
    /**
     * Returns a production site with coordinates x,y.
     *
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     * @return production site.
     */
    private IProductionSite createProductionSite(final int posX, final int posY) {
        switch (this.structureType) {
            case COAL_MINE:
                return new CoalMine(posX, posY);
            case FARM:
                return new Farm(posX, posY);
            case FOOD_FACTORY:
                return new FoodFactory(posX, posY);
            case IRON_MINE:
                return new IronMine(posX, posY);
            case OIL_RIG:
                return new OilRig(posY, posY);
            case POST_OFFICE:
                return new PostOffice(posY, posY);
            case ROCK_CAVE:
                return new RockCave(posY, posY);
            case SAW_MILL:
                return new SawMill(posY, posY);
            default:
                throw new RuntimeException("Unreachable Code Reached!");
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int posX, final int posY) {
        // to implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int posX, final int posY) {
        // to implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        // to implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        if (!Time.getInstance().isPaused()) {
            final Vector2f position = CoordinatesManager.getInstance().getPositionByCoordinates(newX, newY);
            this.buildingX = (int) position.getX();
            this.buildingY = (int) position.getY();
            if (this.terrain.areCoordinatesInRange(this.buildingX, this.buildingY)) {
                final ICell cell = this.terrain.getCell(this.buildingX, this.buildingY);
                if (cell.getCellType() == CellTypes.GROUND && !cell.containsStructure() && cell.isOwned()) {
                    this.isValid = true;
                } else {
                    this.isValid = false;
                }
            } else {
                this.isValid = false;
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        // to implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        final CoordinatesManager manager = CoordinatesManager.getInstance();
        final Vector2f lowBoundaries = manager.getLowBoundaries();
        final Vector2f highBoundaries = manager.getHighBoundaries();
        for (int y = (int) lowBoundaries.getY(); y < highBoundaries.getY(); ++y) {
            for (int x = (int) lowBoundaries.getX(); x < highBoundaries.getX(); ++x) {
                final Vector2f coords = manager.getCoordinatesByPosition(x, y);
                if (manager.areCoordinatesOnScreen(coords)) {
                    final ICell cell = this.terrain.getCell(x, y);
                    if (cell.isOwned()) {
                        final Image texture = ResourcesLoader.getInstance().getImage("HIGHLIGHT_POSITIVE");
                        texture.setAlpha(0.5f);
                        texture.draw(coords.getX(), coords.getY() - manager.getHeightOffset(cell), manager.getScale());
                    }
                }
            }
        }
        
        final Vector2f coords = manager.getCoordinatesByPosition(this.buildingX, this.buildingY);
        if (manager.areCoordinatesOnScreen(coords)) {
            final Image texture;
            final ICell cell = this.terrain.getCell(this.buildingX, this.buildingY);
            if (this.isValid) {
                texture = ResourcesLoader.getInstance().getImage("BUILDING_TILE");
            } else {
                texture = ResourcesLoader.getInstance().getImage("HIGHLIGHT_NEGATIVE");
            }
            texture.setAlpha(0.5f);
            texture.draw(coords.getX(),
                coords.getY() - manager.getHeightOffset(cell) - manager.getScale() * (texture.getHeight() - 64),
                manager.getScale());
            texture.setAlpha(1);
            
            final Image structureTexture = ResourcesLoader.getInstance().getImage(this.structureTexture);
            structureTexture.setAlpha(0.5f);
            structureTexture.draw(coords.getX(), coords.getY() - manager.getHeightOffset(cell)
                -
                manager.getScale()
                * (structureTexture.getHeight() - 64), manager.getScale());
            structureTexture.setAlpha(1);
            
            if (this.isValid) {
                GraphicsUtils.drawCostBox(graphics,
                    this.getProductionSiteCost() + cell.getCost(),
                    container.getInput().getMouseX() + 50,
                    container.getInput().getMouseY() + 50);
            }
        }
    }
    
    
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        // Implement if needed.
    }
}
