/**
 * Provides the application with objects which are treated as general good in game.
 *
 * AbstractWare is an abstract class which provides the basic functionality shared between all the wares.
 *
 * Food is general good produced by food factories and farms.
 *
 * IWare provides the interface for wares.
 *
 * Mail is general good produced by post offices.
 *
 * Passengers is general good produces by buildings.
 *
 * WareTypes defines the type of each ware.
 */
package ch.usi.inf.saiv.railwayempire.model.wares;