package ch.usi.inf.saiv.railwayempire.gui.elements.center.route;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.TrainSelectionController;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.LayoutPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.route.TrainStationIcon.State;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.structures.IStationManagerObserver;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.StationManager;
import ch.usi.inf.saiv.railwayempire.model.structures.StationSizes;
import ch.usi.inf.saiv.railwayempire.model.trains.Route;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.IPersistent;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * Panel to edit the route of a train.
 */
public final class TrainRoutePanel extends AbstractPanel implements IStationManagerObserver, IPersistent {
    
    /**
     * Height of a station panel in one of the list on the left side.
     */
    private static final int STATION_LIST_ELEMENT_HEIGHT = 70;
    
    /**
     * Offset where the map is drawn in relation to the panel.
     */
    private static final int MAP_OFFSET = 50;
    
    /**
     * Height of a label on this panel.
     */
    private static final int LABEL_HEIGHT = 30;
    
    /**
     * The terrain on which stations and tracks are built
     */
    private final Terrain terrain;
    
    /**
     * The generated image of the terrain.
     */
    private final TerrainImage terrainImage;
    
    /**
     * Transformation from the terrain in the model to the image of the terrain.
     */
    private final Transform transform;
    
    /**
     * The station manager
     */
    private StationManager stationManager;
    
    /**
     * The station icons on the map.
     */
    private final Map<Station, TrainStationIcon> icons;
    
    /**
     * The selected train for which we set the route.
     */
    private Train train;
    
    /**
     * THe route of the selected train.
     */
    private Route route;
    
    /**
     * Label for the train route.
     */
    private final Label routeLabel;
    
    /**
     * Display the route of the train.
     */
    private final LayoutPanel trainRoute;
    
    /**
     * Clear the route of the train.
     */
    private final Button clearButton;
    
    /**
     * Label of clear button.
     */
    private final Label clearBg;
    
    /**
     * Label for the next stops.
     */
    private final Label stopsLabel;
    
    /**
     * Display the next reachable stops
     */
    private final LayoutPanel reachableStops;
    
    /**
     * Resources loader.
     */
    ResourcesLoader loader;
    
    /**
     * Create a new train route panel.
     * 
     * @param rectangle
     *            The rectangle containing the panel.
     * @param trainSelectionController
     *            The controller handling some of the interaction for this panel.
     */
    public TrainRoutePanel(final Rectangle rectangle, final TrainSelectionController trainSelectionController) {
        super(rectangle);
        this.loader = ResourcesLoader.getInstance();
        
        this.setBackground(this.loader.getColor("SEA_TILE_9"));
        
        final float listWidth = Math.max(230, 0.25f * this.getWidth());
        
        this.clearButton = new Button(this.loader.getImage("CLEAR_ROUTE_BUTTON"),
            this.loader.getImage("CLEAR_ROUTE_BUTTON_HOVER"),
            this.loader.getImage("CLEAR_ROUTE_BUTTON_PRESSED"),
            this.loader.getImage("CLEAR_ROUTE_BUTTON_DISABLED"),
            0, 0);
        
        this.routeLabel = new Label(this.getX(), this.getY(), (int) listWidth, TrainRoutePanel.LABEL_HEIGHT,
            "Train Itinerary", Alignment.CENTER, true);
        
        this.routeLabel.setBackground(this.loader.getImage("BACKGROUND"));
        this.routeLabel.setFont(FontUtils.LARGE_FONT);
        
        //Display the next stops which can be reached
        this.trainRoute = new LayoutPanel(new Rectangle(this.getX(),
            this.routeLabel.getY() + this.routeLabel.getHeight(),
            listWidth,
            (this.getHeight() - TrainRoutePanel.LABEL_HEIGHT * 2 - this.clearButton.getHeight()) * 0.5f));
        this.trainRoute.setBackground(this.loader.getImage("BACKGROUND_DARK"));
        this.add(this.trainRoute);
        
        this.clearButton.setLocation(this.getX() + (this.trainRoute.getWidth() - this.clearButton.getWidth()) / 2,
            this.trainRoute.getY() + this.trainRoute.getHeight());
        
        this.clearButton.setListener(new MouseListener() {
            
            /**
             * {@inheritDoc}
             */
            @Override
            public void actionPerformed(final Button button) {
                TrainRoutePanel.this.clearRoute();
            }
        });
        this.add(this.clearButton);
        this.clearBg = new Label(this.getX(),
            this.clearButton.getY(),
            this.trainRoute.getWidth(),
            this.clearButton.getHeight(),
            "");
        this.clearBg.setBackground(this.loader.getImage("BACKGROUND"));
        
        this.stopsLabel = new Label(this.getX(), this.clearButton.getY() + this.clearButton.getHeight(),
            (int) listWidth, TrainRoutePanel.LABEL_HEIGHT, "Available Stations", Alignment.CENTER, true);
        this.stopsLabel.setBackground(this.loader.getImage("BACKGROUND"));
        this.stopsLabel.setFont(FontUtils.LARGE_FONT);
        
        //Display the next stops which can be reached
        this.reachableStops = new LayoutPanel(new Rectangle(this.getX(),
            this.stopsLabel.getY() + this.stopsLabel.getHeight(), listWidth,
            (this.getHeight() - TrainRoutePanel.LABEL_HEIGHT * 2 - this.clearButton.getHeight()) * 0.5f));
        this.reachableStops.setBackground(this.loader.getImage("BACKGROUND_DARK"));
        this.add(this.reachableStops);
        
        //Map
        this.terrain = Game.getInstance().getWorld().getTerrain();
        this.terrainImage = new TerrainImage(new Rectangle(
            this.getX() + this.reachableStops.getWidth() - TrainRoutePanel.MAP_OFFSET,
            this.getY() - TrainRoutePanel.MAP_OFFSET,
            this.getWidth() - this.reachableStops.getWidth() + TrainRoutePanel.MAP_OFFSET * 2,
            this.getHeight() + TrainRoutePanel.MAP_OFFSET * 2), this.terrain);
        
        //Transform of coordinates from model to the map
        this.transform = new Transform();
        this.transform.concatenate(Transform.createTranslateTransform(-TrainRoutePanel.MAP_OFFSET,
            -TrainRoutePanel.MAP_OFFSET));
        this.transform.concatenate(Transform.createTranslateTransform(
            0.5f * (this.terrainImage.getWidth() - this.terrainImage.getImage().getWidth()) + listWidth,
            0.5f * (this.terrainImage.getHeight() - this.terrainImage.getImage().getHeight())));
        this.transform.concatenate(Transform.createScaleTransform(this.terrainImage.getScale(),
            this.terrainImage.getScale()));
        this.transform.concatenate(Transform.createRotateTransform((float) (Math.PI / 4),
            this.terrain.getSize() * 0.5f, this.terrain.getSize() * 0.5f));
        
        //Stations
        this.icons = new HashMap<Station, TrainStationIcon>();
        this.reload();
        Game.addPersistent(this);
        
        trainSelectionController.initTrainRoute(this);
        
        this.setTrain(null);
        
    }
    
    /**
     * Get the height of an element in the stations list.
     * 
     * @return The height of an element in the stations list.
     */
    public static int getStationListElementHeight() {
        return TrainRoutePanel.STATION_LIST_ELEMENT_HEIGHT;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        
        final Rectangle clip = graphics.getClip();
        graphics.setClip((Rectangle) this.getShape());
        
        if (this.getBackgroundColor() != SystemConstants.NULL) {
            final Color previousColor = graphics.getColor();
            graphics.setColor(this.getBackgroundColor());
            graphics.fill(this.getShape());
            graphics.setColor(previousColor);
        }
        
        if (this.getBackgroundImage() != SystemConstants.NULL) {
            graphics.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight(),
                this.getBackgroundImage(), 0, 0);
        }
        
        this.terrainImage.render(container, graphics);
        
        for (final TrainStationIcon icon : this.icons.values()) {
            icon.render(container, graphics);
        }
        
        this.reachableStops.render(container, graphics);
        
        this.trainRoute.render(container, graphics);
        this.clearBg.render(container, graphics);
        this.routeLabel.render(container, graphics);
        this.stopsLabel.render(container, graphics);
        this.clearButton.render(container, graphics);
        GraphicsUtils.drawShadowText(graphics, "Reachable station", this.trainRoute.getX() + this.trainRoute.getWidth()
            + 40,
            this.getY() + this.getHeight() - 105);
        GraphicsUtils.drawShadowText(graphics,
            "Reachable station (already in the itinerary)",
            this.trainRoute.getX() + this.trainRoute.getWidth() + 40,
            this.getY() + this.getHeight() - 85);
        GraphicsUtils.drawShadowText(graphics,
            "Latest station in the itnerary",
            this.trainRoute.getX() + this.trainRoute.getWidth() + 40,
            this.getY() + this.getHeight() - 65);
        GraphicsUtils.drawShadowText(graphics,
            "Unreachable station (connected to the network)",
            this.trainRoute.getX() + this.trainRoute.getWidth() + 40,
            this.getY() + this.getHeight() - 45);
        GraphicsUtils.drawShadowText(graphics,
            "Unreachable station (isolated)",
            this.trainRoute.getX() + this.trainRoute.getWidth() + 40,
            this.getY() + this.getHeight() - 25);
        
        final Color defaultColor = graphics.getColor();
        graphics.setColor(this.loader.getColor("STATION_REACHABLE"));
        graphics.fillRect(this.trainRoute.getX() + this.trainRoute.getWidth() + 12, this.getY() + this.getHeight()
            - 105, 16, 16);
        graphics.setColor(this.loader.getColor("STATION_SELECTED"));
        graphics.fillRect(this.trainRoute.getX() + this.trainRoute.getWidth() + 12, this.getY() + this.getHeight()
            - 85, 16, 16);
        graphics.setColor(this.loader.getColor("STATION_LATEST"));
        graphics.fillRect(this.trainRoute.getX() + this.trainRoute.getWidth() + 12, this.getY() + this.getHeight()
            - 65, 16, 16);
        graphics.setColor(this.loader.getColor("STATION_LINKED"));
        graphics.fillRect(this.trainRoute.getX() + this.trainRoute.getWidth() + 12, this.getY() + this.getHeight()
            - 45, 16, 16);
        graphics.setColor(this.loader.getColor("STATION_UNREACHABLE"));
        graphics.fillRect(this.trainRoute.getX() + this.trainRoute.getWidth() + 12, this.getY() + this.getHeight()
            - 25, 16, 16);
        
        GraphicsUtils.drawShadowText(graphics, "Large Station", this.getX() + this.getWidth() - 150,
            this.getY() + this.getHeight() - 65, 120, Alignment.RIGHT);
        
        GraphicsUtils.drawShadowText(graphics, "Medium Station", this.getX() + this.getWidth() - 150,
            this.getY() + this.getHeight() - 45, 120, Alignment.RIGHT);
        
        GraphicsUtils.drawShadowText(graphics, "Small Station", this.getX() + this.getWidth() - 150,
            this.getY() + this.getHeight() - 25, 120, Alignment.RIGHT);
        
        final Shape largeIcon = TrainStationIcon.generateShape(StationSizes.LARGE, 6, false);
        largeIcon.setLocation(this.getX() + this.getWidth() - largeIcon.getWidth() - 12,
            this.getY() + this.getHeight() - 63);
        final Shape mediumIcon = TrainStationIcon.generateShape(StationSizes.MEDIUM, 6, false);
        mediumIcon.setLocation(this.getX() + this.getWidth() - mediumIcon.getWidth() - 12,
            this.getY() + this.getHeight() - 43);
        final Shape smallIcon = TrainStationIcon.generateShape(StationSizes.SMALL, 5, false);
        smallIcon.setLocation(this.getX() + this.getWidth() - smallIcon.getWidth() - 12,
            this.getY() + this.getHeight() - 22);
        
        graphics.setColor(Color.white);
        graphics.fill(largeIcon);
        graphics.fill(mediumIcon);
        graphics.fill(smallIcon);
        
        graphics.setColor(defaultColor);
        if (null == clip) {
            graphics.clearClip();
        } else {
            graphics.setClip(clip);
        }
    }
    
    /**
     * Redraw the map in the background.
     * 
     * This method needs to be called when the map changes, for example when tracks are added.
     * For now it is called upon when it needs to be displayed.
     */
    public void redraw() {
        this.terrainImage.updateImage();
    }
    
    /**
     * Set the train for which we edit the route
     * 
     * @param train
     *            The train for which we edit the route.
     */
    public void setTrain(final Train train) {
        this.train = train;
        this.update();
    }
    
    /**
     * Update the view.
     */
    public void update() {
        
        //Clear the route panel
        this.trainRoute.getChildrenComponents().clear();
        
        //Clear panel
        this.reachableStops.getChildrenComponents().clear();
        
        //Rest Icons
        for (final TrainStationIcon icon : this.icons.values()) {
            icon.resetState();
        }
        
        if (null == this.train) {
            this.route = null;
            this.clearButton.disableButton();
            return;
        }
        this.route = this.train.getRoute();
        if (this.route.hasNoStops()) {
            this.clearButton.disableButton();
        } else {
            this.clearButton.enableButton();
        }
        
        //Empty Route
        if (this.route.hasNoStops()) {
            for (final TrainStationIcon icon : this.icons.values()) {
                if (icon.getState() == State.LINKED) {
                    icon.setState(State.REACHABLE);
                    this.reachableStops.add(new StationAvailableDisplayPanel(new Rectangle(0, 0,
                        this.reachableStops.getWidth(), 70), icon.getStation(), this));
                }
            }
        } else {
            for (final Station station : this.route.getPath()) {
                this.trainRoute.add(new StationRouteDisplayPanel(new Rectangle(0, 0, this.reachableStops.getWidth(),
                    TrainRoutePanel.STATION_LIST_ELEMENT_HEIGHT), station, this));
                this.icons.get(station).setState(State.SELECTED);
            }
            ((StationRouteDisplayPanel) this.trainRoute.getChildrenComponents().get(0)).setFirst();
            ((StationRouteDisplayPanel) this.trainRoute.getChildrenComponents()
                .get(this.trainRoute.getChildrenComponents().size() - 1)).setLast();
            final Station lastStation = this.route.getLastStation();
            this.icons.get(lastStation).latest();
            for (final Station neighbor : lastStation.getReachableStations()) {
                this.icons.get(neighbor).setState(State.REACHABLE);
                this.reachableStops.add(new StationAvailableDisplayPanel(new Rectangle(0, 0,
                    this.reachableStops.getWidth(), 70), neighbor, this));
            }
        }
        
    }
    
    /**
     * Add a new station to the route of the train.
     * 
     * @param station
     *            THe station to add to the route.
     */
    public void addStation(final Station station) {
        if (null != this.route) {
            this.route.addStop(station);
        }
        
        //Start the train if it is stopped
        if (this.train.isParked() && this.route.getPath().size() > 1) {
            Game.getInstance().getWorld().getTrainManager().addTrain(this.train);
        }
        
    }
    
    /**
     * Remove a station from the map.
     * 
     * @param station
     *            The station to remove from the map.
     */
    public void removeStation(final Station station) {
        //TODO Implement once the model supports this operation.
        
    }
    
    /**
     * Clear the route of the current train and stops it.
     */
    public void clearRoute() {
        if (null != this.route) {
            this.train.stop();
            this.update();
        }
    }
    
    /**
     * Highlight the given station on the map.
     * 
     * @param station
     *            The station to highlight.
     */
    public void highlightStation(final Station station) {
        this.icons.get(station).highlight();
    }
    
    /**
     * Unhighlight the given station on the map.
     * 
     * @param station
     *            The station to unhighlight.
     */
    public void unhighlightStation(final Station station) {
        this.icons.get(station).unhighlight();
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void notifyStationAdded(final Station station) {
        final Vector2f position = this.transform.transform(new Vector2f(station.getX(), station.getY()));
        final TrainStationIcon icon = new TrainStationIcon(station, this);
        icon.setCenterX(this.getX() + position.getX());
        icon.setCenterY(this.getY() + position.getY());
        this.icons.put(station, icon);
        this.add(icon);
        this.update();
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void notifyStationRemoved(final Station station) {
        this.icons.remove(station);
        this.update();
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void notifyStationLinked(final Station station) {
        final TrainStationIcon icon = this.icons.get(station);
        icon.setState(State.LINKED);
        this.update();
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void notifyStationUnlinked(final Station station) {
        final TrainStationIcon icon = this.icons.get(station);
        icon.setState(State.UNREACHABLE);
        this.update();
        
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void reload() {
        this.stationManager = Game.getInstance().getWorld().getStationManager();
        this.stationManager.registerObserver(this);
        for (final Station station : this.stationManager.getStations()) {
            this.notifyStationAdded(station);
        }
        
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        super.mouseReleased(button, coordX, coordY);
        this.update();
    }
}
