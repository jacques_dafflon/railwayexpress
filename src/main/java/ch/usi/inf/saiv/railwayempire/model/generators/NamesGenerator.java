package ch.usi.inf.saiv.railwayempire.model.generators;


import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.World;
import ch.usi.inf.saiv.railwayempire.utilities.FileUtils;


/**
 * Class which generates city, station and people names.
 */
public final class NamesGenerator {

    /**
     * The unique instance of the generator.
     */
    private static final NamesGenerator UNIQUE_INSTANCE = new NamesGenerator();
    /**
     * The limit under which a random generated number must be in order to give a prefix to the city name.
     */
    private static final float PREFIX_RANDOM_LIMIT = 0.1f;
    /**
     * The limit under which a random generated number must be in order to give a postfix to the city name.
     */
    private static final float POSTFIX_RANDOM_LIMIT = 0.1f;
    /**
     * The limit for the presence of a prefix in the train name.
     */
    private static final float TRAIN_PREFIX_LIMIT = 0.2f;
    /**
     * A set containing the generated names.
     */
    private final HashSet<String> generatedCityNames;
    /**
     * A map to the station names given the city.
     */
    private final Map<String, Set<String>> generatedStationNames;
    /**
     * A list of the available names.
     */
    private final List<String> cityNames;
    /**
     * A list of the available prefixes.
     */
    private final List<String> cityPrefixes;
    /**
     * A list of the available postfixes.
     */
    private final List<String> cityPostfixes;
    /**
     * A list of the station postfixes.
     */
    private final List<String> stationPostfixes;
    /**
     * A set containing the generated people names.
     */
    private final HashSet<String> generatedPeople;
    /**
     * A list of the available people names.
     */
    private final List<String> peopleNames;
    /**
     * A list of the available people surnames.
     */
    private final List<String> peopleSurnames;
    /**
     * A list of the prefixes for trains.
     */
    private final List<String> trainPrefixes;
    /**
     * A list of the prefixes for trains.
     */
    private final List<String> trainPostfixes;

    /**
     * Generator for the names of cities, stations and people.
     */
    private NamesGenerator() {
        this.generatedCityNames = new HashSet<String>();
        this.generatedStationNames = new HashMap<String, Set<String>>();
        this.generatedPeople = new HashSet<String>();
        this.cityNames = FileUtils.readFromFile("text_resources/cities_names");
        this.cityPrefixes = FileUtils.readFromFile("text_resources/cities_prefixes");
        this.cityPostfixes = FileUtils.readFromFile("text_resources/cities_postfixes");
        this.stationPostfixes = FileUtils.readFromFile("text_resources/stations_postfixes");
        this.peopleNames = FileUtils.readFromFile("text_resources/owner_names");
        this.peopleSurnames = FileUtils.readFromFile("text_resources/owner_surnames");
        this.trainPrefixes = FileUtils.readFromFile("text_resources/train_prefixes");
        this.trainPostfixes = FileUtils.readFromFile("text_resources/train_postfixes");
    }

    /**
     * Returns the instance of the generator.
     *
     * @return The generators.
     */
    public static NamesGenerator getInstance() {
        return NamesGenerator.UNIQUE_INSTANCE;
    }

    /**
     * Generates a name for the city. Ensures the name was not already chosen.
     *
     * @return The generated name.
     */
    public String generateCityName() {
        String name;
        int deadlock = 0;
        do {
            name = this.composeCityName();
        } while (this.generatedCityNames.contains(name) && deadlock++ < 1000);
        this.generatedCityNames.add(name);
        return name;
    }

    /**
     * Composes a name for a city.
     *
     * @return The name.
     */
    private String composeCityName() {
        String prefix = "";
        String postFix = "";
        if (Math.random() < NamesGenerator.PREFIX_RANDOM_LIMIT) {
            final int index = (int) Math.round(Math.random() * (this.cityPrefixes.size() - 1));
            prefix = this.cityPrefixes.get(index) + " ";
        } else if (Math.random() < NamesGenerator.POSTFIX_RANDOM_LIMIT) {
            final int index = (int) Math.round(Math.random() * (this.cityPostfixes.size() - 1));
            postFix = " " + this.cityPostfixes.get(index);
        }
        return prefix + this.cityNames.get((int) (Math.random() * (this.cityNames.size() - 1))) + postFix;
    }

    /**
     * Generates a unique name for a station.
     *
     * @param cityName
     *         The city to which the station belongs.
     * @return The name of the station.
     */
    public String generateStationName(final String cityName) {
        if (!this.generatedStationNames.containsKey(cityName) ||
                this.generatedStationNames.get(cityName).size() < this.stationPostfixes.size()) {
            String name;
            int deadlock = 0;
            do {
                name = this.composeStationName(cityName);
            } while (this.generatedStationNames.containsKey(cityName) &&
                    this.generatedStationNames.get(cityName)
                            .contains(name) && deadlock++ < 1000);

            if (this.generatedStationNames.containsKey(cityName)) {
                this.generatedStationNames.get(cityName).add(name);
            } else {
                final Set<String> names = new HashSet<String>();
                names.add(name);
                this.generatedStationNames.put(cityName, names);
            }
            return name;
        } else {
            final int cardinal = this.generatedStationNames.get(cityName).size() - this.stationPostfixes.size() + 1;
            final String name = cityName + " - Station " + cardinal;
            this.generatedStationNames.get(cityName).add(name);
            return name;
        }
    }

    /**
     * Composes the name of a station.
     *
     * @param cityName
     *         The city to which the station belongs.
     * @return The name of the station.
     */
    private String composeStationName(final String cityName) {
        final int index = (int) Math.round(Math.random() * (this.stationPostfixes.size() - 1));
        final String postfix = this.stationPostfixes.get(index);
        return cityName + " - " + postfix;
    }

    /**
     * Generates a name for a train.
     *
     * @return The generated name.
     */
    public String generateTrainName() {
        final World world = Game.getInstance().getWorld();
        final double rand = Math.random();
        final int index = (int) Math.round(Math.random() * (world.getCities().size() - 1));
        final String name = world.getCities().get(index).getName();

        if (rand < NamesGenerator.TRAIN_PREFIX_LIMIT) {
            final int prefixIndex = (int) Math.round(Math.random() * (this.trainPrefixes.size() - 1));
            return this.trainPrefixes.get(prefixIndex) + " " + name;
        } else {
            final int postfixIndex = (int) Math.round(Math.random() * (this.trainPostfixes.size() - 1));
            return name + " " + this.trainPostfixes.get(postfixIndex);
        }
    }

    /**
     * Generates a persons name. Ensures the name was not already chosen.
     * It can be used for production site owner name.
     *
     * @return The generated name.
     */
    public String generatePersonFullName() {
        String name;
        int deadlock = 0;
        do {
            name = this.composePersonsName();
        } while (this.generatedPeople.contains(name) && deadlock++ < 1000);
        this.generatedPeople.add(name);
        return name;
    }

    /**
     * Composes a persons name which can be used for production site owner.
     *
     * @return The name.
     */
    private String composePersonsName() {
        final int nameIndex = (int) Math.round(Math.random() * (this.peopleNames.size() - 1));
        final String name = this.peopleNames.get(nameIndex);
        final int surnameIndex = (int) Math.round(Math.random() * (this.peopleSurnames.size() - 1));
        final String surname = this.peopleSurnames.get(surnameIndex);
        return name + " " + surname;
    }
}
