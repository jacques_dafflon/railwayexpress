package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Shape;

/**
 * Train buy components info panel.
 */
public final class TrainBuyComponentsInfoPanel extends AbstractInfoPanel {
    
    /**
     * Constructor.
     * 
     * @param shape
     *            shape.
     */
    public TrainBuyComponentsInfoPanel(final Shape shape) {
        super(shape);
    }
}
