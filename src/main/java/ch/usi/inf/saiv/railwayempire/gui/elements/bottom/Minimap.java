package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractGuiComponent;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.IPersistent;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * Minimap viewer and input handler.
 */
public final class Minimap extends AbstractGuiComponent implements IPersistent {
    
    /*
     * Transform matrix.
     */
    private final Transform transform;
    /**
     * Inverse transform matrix.
     */
    private final Transform inverseTransform;
    /**
     * Store the terrain.
     */
    private Terrain terrain;
    
    /**
     * Constructor.
     * 
     * @param newX
     *            x coordinate of the topleft corner of containing rectangle.
     * @param newY
     *            y coordinate of the topleft corner of containing rectangle.
     * @param width
     *            width of the containing rectangle.
     * @param height
     *            height of the containing rectangle.
     */
    public Minimap(final int newX, final int newY, final int width, final int height) {
        super(new Polygon(new float[] { newX + width / 2, newY, newX + width, newY + height / 2, newX + width / 2,
            newY + height, newX, newY + height / 2, }));
        
        this.terrain = Game.getInstance().getWorld().getTerrain();
        Game.addPersistent(this);
        
        final float scale = this.getWidth() / (this.terrain.getSize() * 1.41f);
        this.transform = new Transform();
        this.transform.concatenate(Transform.createScaleTransform(1f, 0.5f));
        this.transform.concatenate(Transform.createTranslateTransform(this.getWidth() * 0.5f, 0));
        this.transform.concatenate(Transform.createRotateTransform((float) (Math.PI / 4), 0, 0));
        this.transform.concatenate(Transform.createScaleTransform(scale, scale));
        
        this.inverseTransform = new Transform();
        this.inverseTransform.concatenate(Transform.createScaleTransform(1 / scale, 1 / scale));
        this.inverseTransform.concatenate(Transform.createRotateTransform((float) (-Math.PI / 4), 0, 0));
        this.inverseTransform.concatenate(Transform.createTranslateTransform(-this.getWidth() * 0.5f, 0));
        this.inverseTransform.concatenate(Transform.createScaleTransform(1f, 2f));
    }
    
    /**
     * Jump the world to given coordinates.
     * 
     * @param coordX
     *            x coordinate.
     * @param coordY
     *            y coordinate.
     */
    private void jumpTo(final int coordX, final int coordY) {
        final Vector2f pos = this.inverseTransform.transform(new Vector2f(coordX - this.getX(), coordY - this.getY()));
        CoordinatesManager.getInstance().centerCameraOn((int) pos.getX(), (int) pos.getY());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        this.jumpTo(newX, newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        this.jumpTo(coordX, coordY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void reload() {
        this.terrain = Game.getInstance().getWorld().getTerrain();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        
        // Terrain
        Vector2f coords;
        for (int y = SystemConstants.ONE; y < this.terrain.getSize() - 4; y += SystemConstants.THREE) {
            for (int x = SystemConstants.ONE; x < this.terrain.getSize() - 4; x += SystemConstants.THREE) {
                final ICell current = this.terrain.getCell(x, y);
                
                // Set cell color
                final List<ICell> cells = this.terrain.getCells(x, y, SystemConstants.ONE);
                graphics.setColor(GraphicsUtils.getCellColor(cells, current));
                coords = this.transform.transform(new Vector2f(x - 1, y - 1));
                graphics.fillRect(this.getX() + coords.getX(),
                    this.getY() + coords.getY(), SystemConstants.THREE, SystemConstants.THREE);
            }
        }
        
        // Stations
        for (final Station station : Game.getInstance().getWorld().getStationManager().getStations()) {
            coords = this.transform.transform(new Vector2f(station.getX() - 1, station.getY() - 1));
            graphics.setColor(ResourcesLoader.getInstance().getColor("STATION"));
            graphics.fillRect(this.getX() + coords.getX(),
                this.getY() + coords.getY(),
                SystemConstants.THREE,
                SystemConstants.THREE);
        }
        
        // Camera frame
        //TODO Buggy near borders, not exactly exact...
        final CoordinatesManager coordinatManager = CoordinatesManager.getInstance();
        final Vector2f low = coordinatManager.getLowBoundaries();
        final Vector2f high = coordinatManager.getHighBoundaries();
        
        final Polygon cameraFrame = new Polygon();
        cameraFrame.addPoint(low.getX() + (high.getX() - low.getX()) * 0.5f, low.getY());
        cameraFrame.addPoint(high.getX(), low.getY() + (high.getY() - low.getY()) * 0.5f);
        cameraFrame.addPoint(low.getX() + (high.getX() - low.getX()) * 0.5f, high.getY());
        cameraFrame.addPoint(low.getX(), low.getY() + (high.getY() - low.getY()) * 0.5f);
        
        final Shape cameraFrameTransformed = cameraFrame.transform(this.transform);
        cameraFrameTransformed.setX(this.getX() + cameraFrame.getX());
        cameraFrameTransformed.setY(this.getY() + cameraFrame.getY());
        graphics.setColor(Color.orange);
        graphics.setLineWidth(1.5f);
        graphics.draw(cameraFrameTransformed);
    }
}
