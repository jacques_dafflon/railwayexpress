package ch.usi.inf.saiv.railwayempire.utilities.serialization;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.utilities.FileUtils;
import ch.usi.inf.saiv.railwayempire.utilities.Log;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;


/**
 * The class responsible for saving the game.
 */
public final class Save {

    /**
     * The unique instance of the save.
     */
    private static final Save UNIQUE_INSTANCE = new Save();
    /**
     * Indicates whether the game changed since the last save or not.
     */
    private boolean needToSaveAgain;

    /**
     * Empty constructor for save class.
     */
    private Save() {

    }

    /**
     * Returns the unique instance of the save.
     *
     * @return The instance of the save.
     */
    public static Save getInstance() {
        return Save.UNIQUE_INSTANCE;
    }

    /**
     * Saves the game.
     *
     * @param customName
     *         Possible custom name for the file
     * @return Success of the save
     */
    public boolean saveGame(final String customName) {
        final String trimmed = customName.replace("/", "");
        try {
            final File saveDir = new File(SystemConstants.SAVE_PATH);
            final boolean created = saveDir.mkdir();
            final String saveGame;
            if (trimmed.length() > 1 && !trimmed.matches("\\s+")) {
                saveGame = String.format("%s/%s.ser", saveDir.getPath(), trimmed);
            } else {
                saveGame = String.format("%s/%s.ser", saveDir.getPath(), this.getDefaultSaveName());
            }
            if (created || saveDir.exists()) {
                final FileOutputStream fileOut = new FileOutputStream(saveGame);
                final ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(Game.getInstance());
                out.close();
                fileOut.close();
                this.writeSaveGameInfo(saveGame);
                this.setNeedToSaveAgain(false);
                return true;
            }
        } catch (final IOException exception) {
            Log.exception(exception);
        }
        return false;
    }

    /**
     * Writes on file player information about the current save game.
     *
     * @param filename
     *         Matching name for save game and profile file.
     */
    public void writeSaveGameInfo(final String filename) {
        final List<String> lines = new ArrayList<String>();
        lines.add("Player: " + Game.getInstance().getPlayer().getPlayerName());
        lines.add("Company: " + Game.getInstance().getPlayer().getCompanyName());
        lines.add("Money: " + Game.getInstance().getPlayer().getAvailableMoney());
        lines.add("Time: " + Game.getInstance().getTime().toDate());
        lines.add("Owned trains: " + Game.getInstance().getWorld().getTrainManager().getTrains().size());
        lines.add("Owned stations " + Game.getInstance().getWorld().getStationManager().getStations().size());
        FileUtils.writeToFile(filename.replace(".ser", ".pd"), lines);
    }

    /**
     * Returns the name of a default saved game.
     *
     * @return The name of the file.
     */
    public String getDefaultSaveName() {
        final String timeStamp = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss", Locale.ITALIAN).format(
                Calendar.getInstance().getTime());
        return String.format("%s@%s", Game.getInstance().getPlayer().getPlayerName(), timeStamp);
    }

    /**
     * Setter for the value indicating the need to save again.
     *
     * @param bool
     *         Value to set it to.
     */
    public void setNeedToSaveAgain(final boolean bool) {
        this.needToSaveAgain = bool;
    }

    /**
     * Tells whether there is the need to save again before exiting.
     *
     * @return True if another save is needed, false otherwise.
     */
    public boolean needToSave() {
        return this.needToSaveAgain;
    }
}
