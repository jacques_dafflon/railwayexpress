package ch.usi.inf.saiv.railwayempire.utilities;

import java.util.HashMap;
import java.util.Map;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.World;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;

/**
 * Manager for the end game statistics.
 */
public class StatisticsManager {
    
    /**
     * The unique instance of the statistics manager.
     */
    private static final StatisticsManager UNIQUE_INSTANCE = new StatisticsManager();
    /**
     * The map containing the trains and their respective statistics holders.
     */
    private final Map<Train, TrainStatistics> trainStatisticsMap;

    /**
     * Number of traded goods.
     */
    private double totalTradedGoods;

    /**
     * Standard constructor.
     */
    private StatisticsManager() {
        this.trainStatisticsMap = new HashMap<Train, TrainStatistics>();
    }

    /**
     * Adds values to the total.
     * @param value
     *              number of goods to add.
     */
    public void addTotalTradedGoods(final double value) {
        this.totalTradedGoods += value;
    }
    
    /**
     * Returns the unique instance of the statistics manager.
     *
     * @return The manager.
     */
    public static StatisticsManager getInstance() {
        return StatisticsManager.UNIQUE_INSTANCE;
    }
    
    /**
     * Returns the train statistics object for the given train.
     * If the train has no statistics yet it creates a new object for him.
     *
     * @param train
     *            The train to get the statistics for.
     * @return The statistics of the given train.
     */
    public TrainStatistics getTrainStatistics(final Train train) {
        if (!this.trainStatisticsMap.containsKey(train)) {
            this.trainStatisticsMap.put(train, new TrainStatistics());
        }
        return this.trainStatisticsMap.get(train);
    }
    
    /**
     * Tells whether a train has statistics or not.
     *
     * @param train
     *            train.
     * @return True if the train has statistics, false otherwise.
     */
    public boolean hasStatistics(final Train train) {
        return this.trainStatisticsMap.containsKey(train);
    }
    
    /**
     * Returns the points given by the money gained during the game.
     *
     * @return The points.
     */
    public long pointsFromGainedMoney() {
        return Game.getInstance().getPlayer().getAvailableMoney() - GameConstants.INITIAL_MONEY;
    }
    
    /**
     * Returns the points given by the number of stations the player owns.
     *
     * @return The points.
     */
    public long pointsFromOwnedStations() {
        final World world = Game.getInstance().getWorld();
        return GameConstants.POINTS_FOR_LINKED_STATION * world.getStationManager().getStations().size();
    }
    
    /**
     * Returns the total points given by the trade of wares performed by the players trains.
     *
     * @return The points.
     */
    public long pointsFromTradedWares() {
        return (long) this.totalTradedGoods;
    }
    
    /**
     * Returns the total points given by reaching different cities with the players trains.
     *
     * @return The points.
     */
    public long pointsForCitiesReached() {
        final World world = Game.getInstance().getWorld();
        int score = 0;
        for (final Train train : world.getTrainManager().getTrains()) {
            if (this.hasStatistics(train)) {
                final TrainStatistics stats = this.getTrainStatistics(train);
                score += GameConstants.POINTS_FOR_REACHED_CITY * stats.getReachedCities().size();
            }
        }
        return score;
    }
    
    /**
     * Returns the score of the player.
     *
     * @return The score.
     */
    public long computeScore() {
        long score = this.pointsFromGainedMoney();
        score += this.pointsFromOwnedStations();
        score += this.pointsFromTradedWares();
        
        return Math.max(0, score);
    }
}
