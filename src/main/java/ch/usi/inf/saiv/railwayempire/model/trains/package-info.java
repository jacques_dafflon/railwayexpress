/**
 * Provides application with the functionality of having trains and managing them.
 *
 * ITrainManagerObserver is a design patter for observing the actions of train manager.
 *
 * Locomotive provides the train with an object or car which is able to pull wagons.
 *
 * Route defines the sequence of track and stations for which the train has to move.
 *
 * Stop provides with the functionality of the state when the train is stopped.
 *
 * Trade provides with the functionality of the state when the train is trading.
 *
 * Train combines both locomotive and wagons under one single object.
 *
 * TrainComponent
 *
 * TrainComponentFactory assembles each train component to his place in order to have a valid composition of locomotives and wagons.
 *
 * TrainManager manages trains.
 *
 * TrainState defines the states of the train.
 *
 * WareHouse works as global place where the player has all his train components that are not currently assigned to the route.
 */
package ch.usi.inf.saiv.railwayempire.model.trains;