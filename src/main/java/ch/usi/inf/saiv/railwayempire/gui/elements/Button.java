package ch.usi.inf.saiv.railwayempire.gui.elements;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * Button class.
 * 
 * A button usually have three possible visualizations: normal, hover and clicked.
 * It is also possible to have the disabled visualization.
 * 
 * It is possible to trigger events by registering a MouseListener.
 * Only one listener can be set for each button.
 */
public class Button extends AbstractGuiComponent {
    
    /** The default state. */
    private static final int NORMAL = 1;
    /** The mouse down state. */
    private static final int MOUSE_DOWN = 2;
    /** The mouse over state. */
    private static final int MOUSE_OVER = 3;
    
    /** Listener. **/
    private MouseListener listener;
    /** Current image displayed. */
    private Image currentImage;
    /** Image for normal state. */
    private final Image normalImage;
    /** Image when hovered. */
    private final Image hoverImage;
    /** Image when clicked. */
    private final Image clickedImage;
    /** Image when disabled. */
    private final Image disabledImage;
    /** True if the mouse is over the area. */
    private boolean over;
    /** True if the mouse button is pressed. */
    private boolean mouseDown;
    /** The state of the area. */
    private int state = Button.NORMAL;
    /** True if the mouse has been up since last press. */
    private boolean mouseUp;
    /** True if button is disabled. */
    private boolean disabled;
    /** True if button is in hold state. */
    private boolean hold;
    
    /**
     * Constructor for a single image of the button.
     * The same image will be used for all states of the button.
     * 
     * @param image
     *            the image.
     * @param topLeftX
     *            x coordinate.
     * @param topLeftY
     *            y coordinate.
     */
    public Button(final Image image, final int topLeftX, final int topLeftY) {
        this(image, image, image, null, topLeftX, topLeftY);
    }
    
    /**
     * Constructor for two images of the button: one normal and one for when
     * it's pressed or clicked.
     * 
     * @param image
     *            image of the normal state.
     * @param image2
     *            image when clicked or hovered.
     * @param topLeftX
     *            x coordinate.
     * @param topLeftY
     *            y coordinate.
     */
    public Button(final Image image, final Image image2, final int topLeftX, final int topLeftY) {
        this(image, image2, image2, null, topLeftX, topLeftY);
    }
    
    /**
     * Constructor for three images of the button: one normal, one for the hover
     * and one for the click.
     * 
     * @param image
     *            image of the normal state.
     * @param image2
     *            image when hovered.
     * @param image3
     *            image when clicked.
     * @param topLeftX
     *            x coordinate.
     * @param topLeftY
     *            y coordinate.
     */
    public Button(final Image image, final Image image2, final Image image3, final int topLeftX, final int topLeftY) {
        this(image, image2, image3, null, topLeftX, topLeftY);
    }
    
    /**
     * Constructor for four images of the button: one normal, one for the hover,
     * one for the click and one for the disabled state.
     * 
     * @param image
     *            image of the normal state.
     * @param image2
     *            image when hovered.
     * @param image3
     *            image when clicked.
     * @param image4
     *            image when disabled.
     * @param topLeftX
     *            x coordinate.
     * @param topLeftY
     *            y coordinate.
     */
    public Button(final Image image, final Image image2, final Image image3, final Image image4, final int topLeftX,
        final int topLeftY) {
        this(image, image2, image3, image4, topLeftX, topLeftY, null);
    }
    
    /**
     * Constructor for four images of the button: one normal, one for the hover,
     * one for the click and one for the disable. Also immediately set the mouse listener.
     * 
     * @param image
     *            image of the normal state.
     * @param image2
     *            image when hovered.
     * @param image3
     *            image when clicked.
     * @param image4
     *            image when disabled.
     * @param topLeftX
     *            x coordinate.
     * @param topLeftY
     *            y coordinate.
     * @param listener
     *            The mouse listener.
     */
    public Button(final Image image,
        final Image image2,
        final Image image3,
        final Image image4,
        final int topLeftX,
        final int topLeftY,
        final MouseListener listener) {
        
        super(new Rectangle(topLeftX, topLeftY, image.getWidth(), image.getHeight()));
        this.normalImage = image;
        this.hoverImage = image2;
        this.clickedImage = image3;
        this.disabledImage = image4;
        this.currentImage = this.normalImage;
        this.mouseDown = false;
        this.over = false;
        this.disabled = false;
        this.listener = listener;
    }
    
    /**
     * Set the listener.
     *
     * @param listener
     *            the listener to set.
     */
    public final void setListener(final MouseListener listener) {
        this.listener = listener;
    }
    
    /**
     * Disable button.
     * After this method is called the button will not trigger events and will display the disabled image until the
     * enableButton() method is called.
     */
    public final void disableButton() {
        if (SystemConstants.NULL == this.disabledImage) {
            throw new NullPointerException("Cannot use a NULL image");
        }
        this.disabled = true;
        this.currentImage = this.disabledImage;
    }
    
    /**
     * Enable button.
     */
    public final void enableButton() {
        this.disabled = false;
        this.currentImage = this.normalImage;
    }
    
    /**
     * Keep the button in pressed state.
     */
    public final void holdButton() {
        this.hold = true;
        this.currentImage = this.clickedImage;
    }
    
    /**
     * Release the button.
     */
    public final void unHoldButton() {
        this.hold = false;
        this.currentImage = this.normalImage;
    }
    
    /**
     * Checks if the button is pressed or not.
     * 
     * @return true if button is pressed, false otherwise.
     */
    public final boolean isButtonPressed(){
        return this.hold;
    }
    
    /**
     * Update the current normalImage based on the mouse state.
     */
    private void updateImage() {
        if (!this.disabled && !this.hold) {
            if (this.over) {
                if (this.mouseDown) {
                    if (this.state != Button.MOUSE_DOWN && this.mouseUp) {
                        this.currentImage = this.clickedImage;
                        this.state = Button.MOUSE_DOWN;
                        this.mouseUp = false;
                    }
                    return;
                } else {
                    this.mouseUp = true;
                    if (this.state != Button.MOUSE_OVER) {
                        this.currentImage = this.hoverImage;
                        this.state = Button.MOUSE_OVER;
                    }
                }
            } else {
                this.currentImage = this.normalImage;
                this.state = Button.NORMAL;
                this.mouseUp = false;
            }
            
            this.mouseDown = false;
            this.state = Button.NORMAL;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void render(final GUIContext container, final Graphics graphics) {
        this.updateImage();
        graphics.drawImage(this.currentImage, this.getX(), this.getY());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mousePressed(final int button, final int coordX, final int coordY) {
        this.over = this.contains(coordX, coordY);
        if (button == SystemConstants.MOUSE_BUTTON_LEFT) {
            this.mouseDown = true;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseReleased(final int button, final int coordX, final int coordY) {
        this.over = this.contains(coordX, coordY);
        if (button == SystemConstants.MOUSE_BUTTON_LEFT) {
            this.actionPerformed();
            this.mouseDown = false;
            this.over = false;
            this.updateImage();
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        this.mouseMoved(oldX, oldY, newX, newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        this.over = this.contains(newX, newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void keyPressed(final int key, final char character) {
        // Implement if needed.
    }
    
    /**
     * Signal the listener.
     */
    private void actionPerformed() {
        if (this.listener != SystemConstants.NULL) {
            this.listener.actionPerformed(this);
        }
    }
}
