/**
 * Encapsulates one of the major sub sections of the application which is Graphical User Interface.
 */
package ch.usi.inf.saiv.railwayempire.gui;