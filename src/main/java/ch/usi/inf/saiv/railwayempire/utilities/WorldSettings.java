package ch.usi.inf.saiv.railwayempire.utilities;


/**
 * Represents the settings used to generate the world.
 */
public final class WorldSettings {

    /**
     * The size of the world to generate.
     */
    private int worldSize;
    /**
     * The number of octaves to use in the noise.
     */
    private int octaves;
    /**
     * The persistence value to use in the noise.
     */
    private double persistence;
    /**
     * The number of rivers to generate.
     */
    private int riversNumber;

    /**
     * Constructor for world settings.
     */
    public WorldSettings() {
        this.worldSize = GameConstants.WORLD_SIZE;
        this.octaves = GameConstants.OCTAVES;
        this.persistence = GameConstants.PERSISTENCE;
        this.riversNumber = GameConstants.RIVERS_NUMBER;
    }

    /**
     * Getter for the world size.
     * @return The world size.
     */
    public int getWorldSize() {
        return this.worldSize;
    }

    /**
     * Setter for the size of the world.
     * @param newWorldSize The size of the world.
     */
    public void setWorldSize(final int newWorldSize) {
        this.worldSize = newWorldSize;
    }

    /**
     * Getter for the octaves to use in the noise.
     * @return The octaves.
     */
    public int getOctaves() {
        return this.octaves;
    }

    /**
     * Setter for the octaves to use in the noise.
     * @param newOctaves The octaves.
     */
    public void setOctaves(final int newOctaves) {
        this.octaves = newOctaves;
    }

    /**
     * Getter for the persistence to use in the noise.
     * @return The persistence.
     */
    public double getPersistence() {
        return this.persistence;
    }

    /**
     * Setter for the persistence to use in the noise.
     * @param newPersistence The persistence.
     */
    public void setPersistence(final double newPersistence) {
        this.persistence = newPersistence;
    }

    /**
     * Getter for the number of rivers to generate.
     * @return The number of rivers.
     */
    public int getRiversNumber() {
        return this.riversNumber;
    }

    /**
     * Setter for the number of rivers to generate.
     * @param newRiversNumber The number of rivers.
     */
    public void setRiversNumber(final int newRiversNumber) {
        this.riversNumber = newRiversNumber;
    }
}
