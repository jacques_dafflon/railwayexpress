package ch.usi.inf.saiv.railwayempire.controllers;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

import ch.usi.inf.saiv.railwayempire.gui.GameState;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.utilities.Time;
import ch.usi.inf.saiv.railwayempire.utilities.serialization.Load;
import ch.usi.inf.saiv.railwayempire.utilities.serialization.Save;

/**
 * Abstract controller to handle things in the game.
 */
public abstract class AbstractGameController {

    /**
     * Slick Container.
     */
    protected final GameContainer container;
    /**
     * Slick game.
     */
    protected final StateBasedGame game;

    /**
     * Construct for the controller.
     *
     * @param container
     *            the slick container.
     * @param game
     *            the slick game instance.
     */
    public AbstractGameController(final GameContainer container, final StateBasedGame game) {
        this.container = container;
        this.game = game;
    }

    /**
     * Show the default panel of the state.
     */
    public abstract void setMainPanel();

    /**
     * Show the load panel.
     */
    public abstract void setLoadPanel();

    /**
     * Load a game.
     *
     * @param loadName
     *            name of the file.
     */
    public void load(final String loadName) {
        Game.getInstance().loadGame(Load.getInstance().loadGame(loadName + ".ser"));
        this.game.enterState(GameState.GAMEPLAY.ordinal());
    }

    /**
     * Save the game
     *
     * @param saveName
     *            name of the file.
     */
    public void save(final String saveName) {
        final boolean saved = Save.getInstance().saveGame(saveName);
        if (saved) {
            this.setMainPanel();
            Time.getInstance().togglePause();
        }
    }

    /**
     * Exits the game.
     */
    public void exitGame() {
        this.container.exit();
    }
}
