package ch.usi.inf.saiv.railwayempire.gui.elements.menu;

import java.io.IOException;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;
import org.newdawn.slick.gui.TextField;

import ch.usi.inf.saiv.railwayempire.controllers.AbstractGameController;
import ch.usi.inf.saiv.railwayempire.controllers.SaveLoadController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.LayoutPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.serialization.Load;
import ch.usi.inf.saiv.railwayempire.utilities.serialization.Save;


/**
 * Save and Load panel.
 */
public class SaveLoadPanel extends SimplePanel {

    /**
     * width of the list.
     */
    private static int LOADWIDTH = 600;
    /**
     * Height of the list.
     */
    private static int LOADHEIGHT = 500;

    /**
     * Height of the single save game.
     */
    private static int SAVEGAMEHEIGHT = 100;

    /**
     * Distance between buttons and elements.
     */
    private static int BUTTONDISTANCE = 10;

    /**
     * Width of the labels.
     */
    private static int TEXTWIDTH = 250;

    /**
     * Color to hide the background.
     */
    private static Color FADECOLOR = new Color(0, 0, 0, 150);

    /**
     * The currently selected savegame.
     */
    private SaveGamePanel file;

    /**
     * Text field to input the name of the file of the save game.
     */
    private TextField fileName;

    /**
     * List of saves.
     */
    private final LayoutPanel saveLoadList;

    /**
     * Button for save and load functions.
     */
    private final Button saveLoadButton;

    /**
     * Slick container.
     */
    private final GameContainer container;

    /**
     * True if the object is handling save, false if load.
     */
    private final boolean save;

    /**
     * Initialize the graphical element. In case save it's true, a text field
     * will be added.
     *
     * @param newShape
     *            shape of the panel.
     * @param gameController
     *            controller for handling everything.
     * @param offset
     *            offset for drawing the panels of the list.
     * @param container
     *            slick container.
     * @param save
     *            true if it handles save, false if load.
     */
    public SaveLoadPanel(final Shape newShape, final AbstractGameController gameController, final int offset,
            final GameContainer container,final boolean save) {
        super(newShape);
        this.save = save;
        this.container = container;
        this.saveLoadList = new LayoutPanel(new Rectangle(this.getWidth() / SystemConstants.TWO
                - SaveLoadPanel.LOADWIDTH / SystemConstants.TWO, this.getHeight() / SystemConstants.TWO
                - SaveLoadPanel.LOADHEIGHT / offset, SaveLoadPanel.LOADWIDTH, SaveLoadPanel.LOADHEIGHT));
        this.saveLoadList.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND"));

        this.add(this.saveLoadList);


        final Image cancelImage = ResourcesLoader.getInstance().getImage("CANCEL_BUTTON");
        if (save) {
            final Image saveLoadImage = ResourcesLoader.getInstance().getImage("SAVE_BUTTON");
            this.saveLoadButton = new Button(saveLoadImage, ResourcesLoader.getInstance().getImage("SAVE_BUTTON_HOVER"),
                    ResourcesLoader.getInstance().getImage("SAVE_BUTTON_PRESSED"), ResourcesLoader.getInstance()
                    .getImage("SAVE_BUTTON_DISABLED"), this.saveLoadList.getX()
                    + this.saveLoadList.getWidth() - cancelImage.getWidth() - saveLoadImage.getWidth()
                    - SaveLoadPanel.BUTTONDISTANCE, this.saveLoadList.getY() + this.saveLoadList.getHeight()
                    + SaveLoadPanel.BUTTONDISTANCE);

            this.saveLoadButton.setListener(new MouseListener() {
                @Override
                public void actionPerformed(final Button button) {
                    gameController.save(SaveLoadPanel.this.fileName.getText());
                    SaveLoadPanel.this.refresh();
                }
            });
            this.fileName = new TextField(container, container.getGraphics().getFont(), this.saveLoadButton.getX()
                    - TEXTWIDTH - BUTTONDISTANCE, this.saveLoadButton.getY(), TEXTWIDTH,
                    this.saveLoadButton.getHeight() - 5) {
                @Override
                public void keyPressed(final int arg0, final char arg1) {
                    super.keyPressed(arg0, arg1);
                    if (SaveLoadPanel.this.fileName.getText().isEmpty()) {
                        SaveLoadPanel.this.saveLoadButton.disableButton();
                    } else {
                        SaveLoadPanel.this.saveLoadButton.enableButton();
                    }
                }
            };
            this.fileName.setFocus(true);
            this.fileName.setBorderColor(Color.black);
            this.add(this.saveLoadButton);
        } else {
            final Image saveLoadImage = ResourcesLoader.getInstance().getImage("LOAD_BUTTON");
            this.saveLoadButton = new Button(saveLoadImage, ResourcesLoader.getInstance().getImage("LOAD_BUTTON_HOVER"),
                    ResourcesLoader.getInstance().getImage("LOAD_BUTTON_PRESSED"), ResourcesLoader.getInstance()
                    .getImage("LOAD_BUTTON_DISABLED"), this.saveLoadList.getX()
                    + this.saveLoadList.getWidth() - cancelImage.getWidth() - saveLoadImage.getWidth()
                    - SaveLoadPanel.BUTTONDISTANCE, this.saveLoadList.getY() + this.saveLoadList.getHeight()
                    + SaveLoadPanel.BUTTONDISTANCE);

            this.saveLoadButton.setListener(new MouseListener() {
                @Override
                public void actionPerformed(final Button button) {
                    gameController.load(SaveLoadPanel.this.file.getName());
                    SaveLoadPanel.this.refresh();
                    gameController.setMainPanel();

                }
            });
            this.add(this.saveLoadButton);
            this.saveLoadButton.enableButton();
        }

        final Button cancelButton = new Button(cancelImage, ResourcesLoader.getInstance().getImage(
                "CANCEL_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage("CANCEL_BUTTON_PRESSED"),
                this.saveLoadList.getX() + this.saveLoadList.getWidth() - cancelImage.getWidth(), this.saveLoadList.getY() + this.saveLoadList.getHeight()
                + SaveLoadPanel.BUTTONDISTANCE);

        cancelButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                gameController.setMainPanel();

            }
        });
        this.add(cancelButton);

        final List<String> savegames = Load.getInstance().getSavedGamesList();
        if (savegames.isEmpty()) {
            this.saveLoadList.add(new Label(this.saveLoadList.getX(), this.saveLoadList.getY(),
                    SaveLoadPanel.LOADWIDTH, SaveLoadPanel.SAVEGAMEHEIGHT, "No savegames found", 10));
        } else {
            final SaveLoadController saveLoadController = new SaveLoadController(this);
            for (final String savedGame : savegames) {
                try {
                    this.saveLoadList.add(new SaveGamePanel(new Rectangle(this.saveLoadList.getX(), this.saveLoadList
                            .getY(), SaveLoadPanel.LOADWIDTH, SaveLoadPanel.SAVEGAMEHEIGHT), savedGame,
                            saveLoadController,
                            this.container));
                } catch (final IOException e) {
                    if (!this.save) {
                        this.saveLoadList.add(new Label(this.saveLoadList.getX(), this.saveLoadList.getY(),
                                SaveLoadPanel.LOADWIDTH, SaveLoadPanel.SAVEGAMEHEIGHT, savedGame
                                + "\nCorrupted Savegame", 10) {
                            @Override
                            public void render(final GUIContext container, final Graphics graphics) {
                                super.render(container, graphics);
                                graphics.setColor(Color.black);
                                graphics.drawRect(this.getX(), this.getY(),
                                        this.getWidth() - 1, this.getHeight());
                            }
                        });
                    }
                }
            }
        }


        super.setBackground(SaveLoadPanel.FADECOLOR);
    }

    /**
     * Set the file selected when the player select another one.
     *
     * @param saveGamePanel
     *            the savegame selected
     */
    public void setFile(final SaveGamePanel saveGamePanel) {
        if (this.file != null) {
            this.file.deselect();
        }
        this.file = saveGamePanel;
        this.file.select();
        if (this.fileName != null) {
            this.fileName.setText(this.file.getName());
        }
        this.saveLoadButton.enableButton();
    }

    /**
     * Refresh the list of saves.
     */
    public void refresh() {
        this.saveLoadList.getChildrenComponents().clear();
        final List<String> savegames = Load.getInstance().getSavedGamesList();
        if (savegames.isEmpty()) {
            this.saveLoadList.add(new Label(this.saveLoadList.getX(), this.saveLoadList.getY(),
                    SaveLoadPanel.LOADWIDTH, SaveLoadPanel.SAVEGAMEHEIGHT, "No savegames found", 10));
        } else {
            final SaveLoadController saveLoadController = new SaveLoadController(this);
            for (final String savedGame : savegames) {
                try {
                    this.saveLoadList
                    .add(new SaveGamePanel(new Rectangle(this.saveLoadList.getX(), this.saveLoadList.getY(),
 SaveLoadPanel.LOADWIDTH, SaveLoadPanel.SAVEGAMEHEIGHT), savedGame,
                            saveLoadController,
                            this.container));
                } catch (final IOException e) {
                    if (!this.save) {
                        this.saveLoadList.add(new Label(this.saveLoadList.getX(), this.saveLoadList.getY(),
                                SaveLoadPanel.LOADWIDTH, SaveLoadPanel.SAVEGAMEHEIGHT, savedGame
                                + "\nCorrupted Savegame", 10) {
                            @Override
                            public void render(final GUIContext container, final Graphics graphics) {
                                super.render(container, graphics);
                                graphics.setColor(Color.black);
                                graphics.drawRect(this.getX(), this.getY(),
                                        this.getWidth() - 1, this.getHeight());
                            }
                        });
                    }
                }
            }
        }
        if (null != this.fileName) {
            this.fileName.setText(Save.getInstance().getDefaultSaveName());
            this.saveLoadButton.enableButton();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        if (this.fileName != null) {
            graphics.setColor(Color.white);
            this.fileName.render(container, graphics);
        }
    }

    /**
     * Return the name of the file selected.
     *
     * @return the name of the file.
     */
    public TextField getFileName() {
        return this.fileName;
    }
}
