package ch.usi.inf.saiv.railwayempire.controllers;

import java.util.HashMap;
import java.util.Map;

import ch.usi.inf.saiv.railwayempire.gui.elements.ISelectableObserver;
import ch.usi.inf.saiv.railwayempire.gui.elements.TrainListPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.TrainStatusPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.components.ComponentEditorPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.route.TrainRoutePanel;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.trains.ITrainsObserver;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.model.trains.WareHouse;
import ch.usi.inf.saiv.railwayempire.utilities.Log;

/**
 * Controls which train is currently selected and handles the creation and deletion of train panels based on the model.
 * 
 * It takes care of notifying the elements concerned when the selected train changes.
 */
public class TrainSelectionController implements ITrainsObserver, ISelectableObserver<TrainStatusPanel> {
    
    /**
     * The individual panels displaying the player's trains.
     */
    private final Map<Train, TrainStatusPanel> panels;
    
    /**
     * The container panel for the individual panels of the player's trains.
     */
    private TrainListPanel trainListPanel;
    
    /**
     * The panel handling the creation of a train's route.
     */
    private TrainRoutePanel trainRoutePanel;
    
    /**
     * The panel handling the edition of a train's composition.
     */
    private ComponentEditorPanel trainEditorPanel;
    
    /**
     * Indicates if this is the first {@link TrainStatusPanel} added to the list.
     */
    private boolean firstTrainPanelAdded;
    
    /**
     * Instantiate the controller
     * 
     * This must instantiated before the following classes:
     * 
     * <ul>
     * <li>{@link TrainListPanel}</li>
     * <li>{@link ComponentEditorPanel}</li>
     * <li>{@link TrainRoutePanel}</li>
     * </ul>
     */
    public TrainSelectionController() {
        this.panels = new HashMap<Train, TrainStatusPanel>();
    }
    
    /**
     * Register the train list panel with the controller.
     * 
     * This should be called once, in the train list panel constructor.
     * 
     * @param panel
     *            The train list panel to register with the controller.
     */
    public void initTrainList(final TrainListPanel panel) {
        this.trainListPanel = panel;
        this.firstTrainPanelAdded = true;
        final WareHouse wareHouse = Game.getInstance().getPlayer().getWareHouse();
        wareHouse.registerObserver(this);
        for (final Train train : wareHouse.getAvailableTrains()) {
            this.notifyTrainAdded(train);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void notifyTrainAdded(final Train train) {
        this.panels.put(train, this.trainListPanel.addNewTrainPanel(train));
        if (this.firstTrainPanelAdded) {
            final TrainStatusPanel firstPanel = this.panels.get(train);
            firstPanel.registerObserver(this);
            this.notifySelected(firstPanel);
            this.firstTrainPanelAdded = !this.firstTrainPanelAdded;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void notifySelected(final TrainStatusPanel selected) {
        final Train train = selected.getTrain();
        this.trainEditorPanel.setTrain(train);
        this.trainRoutePanel.setTrain(train);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void notifyUnSelected() {
        if (null == TrainStatusPanel.getStaticSelected()) {
            return;
        }
        TrainStatusPanel.getStaticSelected().unselect();
        this.trainEditorPanel.setTrain(null);
        this.trainRoutePanel.setTrain(null);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void notifyTrainRemoved(final Train train) {
        this.notifyUnSelected();
        final TrainStatusPanel panel = this.panels.remove(train);
        if (panel == null) {
            Log.out("Train is null");
        }
        this.trainListPanel.remove(panel);
    }
    
    /**
     * Register the train route panel with the controller.
     * 
     * This should be called once, in the train route panel constructor.
     * 
     * @param panel
     *            The train route panel to register with the controller.
     */
    public void initTrainRoute(final TrainRoutePanel panel) {
        this.trainRoutePanel = panel;
    }
    
    /**
     * Register the train editor panel with the controller.
     * 
     * This should be called once, in the train editor panel constructor.
     * 
     * @param panel
     *            The train editor panel to register with the controller.
     */
    public void initTrainEditor(final ComponentEditorPanel panel) {
        this.trainEditorPanel = panel;
    }
}
