package ch.usi.inf.saiv.railwayempire.controllers;

/**
 * List the clicks to be performed or possible modes to be entered in the world view.
 */
public enum GameModes {
    /**
     * Normal mode.
     */
    NORMAL,
    /**
     * Tracks creation mode.
     */
    TRACKS,
    /**
     * Station creation mode.
     */
    STATIONS_ALL,
    /**
     * Build a small station.
     */
    STATION_SMALL,
    /**
     * Build a medium station.
     */
    STATION_MEDIUM,
    /**
     * Build a large station.
     */
    STATION_LARGE,
    /**
     * Building creation mode.
     */
    BUILDING;
}
