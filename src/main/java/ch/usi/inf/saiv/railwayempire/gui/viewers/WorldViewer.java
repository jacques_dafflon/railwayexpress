package ch.usi.inf.saiv.railwayempire.gui.viewers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.City;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.cells.MountainCell;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.production_sites.OilRig;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.model.structures.TrackTypes;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.utilities.Coordinate;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.IPersistent;
import ch.usi.inf.saiv.railwayempire.utilities.Pair;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Represents the viewer for the world.
 */
public final class WorldViewer implements IViewer, IPersistent {

    /**
     * The filter for the image drawing of mountains when they are supposed to be transparent.
     */
    private static final Color MOUNTAINS_TRANSPARENT_FILTER = new Color(0, 0, 0, .5f);
    /**
     * The coordinates manager.
     */
    private final CoordinatesManager coordinatesManager;
    /**
     * The Resource loader.
     */
    private final ResourcesLoader resourcesLoader;
    /**
     * background color.
     */
    private final Color backGroundColor;
    /**
     * Terrain.
     */
    private Terrain terrain;
    /**
     * Transition viewer.
     */
    private final TransitionViewer transitionViewer;
    /**
     * Train viewer.
     */
    private final TrainViewer trainViewer;
    /**
     * True if mountains should be rendered half transparent.
     */
    private boolean transparentMountains;
    /**
     * Selected station cells to highlight.
     */
    private final List<Vector2f> highlightedCells;
    /**
     * Selected station.
     */
    private Station station;
    /**
     * Selected structure.
     */
    private IProductionSite productionSite;

    /**
     * Creates a new viewer for the world.
     */
    public WorldViewer() {
        this.coordinatesManager = CoordinatesManager.getInstance();
        this.resourcesLoader = ResourcesLoader.getInstance();
        this.backGroundColor = new Color(17, 32, 45);
        this.terrain = Game.getInstance().getWorld().getTerrain();
        this.transitionViewer = new TransitionViewer();
        this.trainViewer = new TrainViewer();
        this.highlightedCells = new ArrayList<Vector2f>();
        Game.addPersistent(this);
    }

    /**
     * Checks if we can show city names. Modify this method if needed.
     *
     * @return true or false.
     */
    private boolean canShowCityName() {
        return true;
    }

    /**
     * Checks if we can show station names. Modify this method if needed.
     *
     * @return true or false.
     */
    private boolean canShowExtractionSiteAmount() {
        return this.coordinatesManager.getScale() > .5;
    }

    /**
     * Checks if we can show station names. Modify this method if needed.
     *
     * @return true or false.
     */
    private boolean canShowStationName() {
        return this.coordinatesManager.getScale() > .4;
    }

    /**
     * Renders the world on the given graphics.
     *
     * @param container
     *            game container.
     * @param graphics
     *            The graphics on which to draw.
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        graphics.setBackground(this.backGroundColor);
        final Vector2f lowBoundaries = this.coordinatesManager.getLowBoundaries();
        final Vector2f highBoundaries = this.coordinatesManager.getHighBoundaries();

        // Drawing terrain end tracks
        for (int y = (int) lowBoundaries.getY(); y < highBoundaries.getY(); ++y) {
            for (int x = (int) lowBoundaries.getX(); x < highBoundaries.getX(); ++x) {
                final Vector2f coords = this.coordinatesManager.getCoordinatesByPosition(x, y);
                if (this.coordinatesManager.areCoordinatesOnScreen(coords)) {
                    final ICell cell = this.terrain.getCell(x, y);
                    this.renderCell(graphics, coords.getX(), coords.getY(), cell);
                }
            }
        }

        // Getting list of components possibly on screen
        final Map<Coordinate, List<Pair<Train, Integer>>> components = this.trainViewer.getComponentsToRender();

        // Draw structures including track "back structure" before train
        for (int y = (int) lowBoundaries.getY(); y < highBoundaries.getY(); ++y) {
            for (int x = (int) lowBoundaries.getX(); x < highBoundaries.getX(); ++x) {
                final ICell cell = this.terrain.getCell(x, y);
                if (cell.containsStructure() && cell.getStructure().getStructureType() != StructureTypes.TRACK) {
                    final Vector2f coords = this.coordinatesManager.getCoordinatesByPosition(x, y);
                    if (this.coordinatesManager.areCoordinatesOnScreen(coords)) {
                        Image structureTexture;
                        // animate rig
                        if (cell.getStructure().getStructureType() == StructureTypes.OIL_RIG) {
                            final OilRig rig = (OilRig) cell.getStructure().asProductionSite();
                            // if animated rig selected
                            if (this.productionSite != null && this.productionSite.equals(rig)) {
                                this.highlightSelectedSite(graphics, coords,
                                        this.coordinatesManager.getHeightOffset(cell),
                                        this.coordinatesManager.getScale());
                            }
                            structureTexture = this.resourcesLoader.getImage(rig.getAnimatedtexture());
                        }
                        // not animated selection
                        else if (this.productionSite != null && cell.getStructure().asProductionSite() != null
                                && this.productionSite.equals(cell.getStructure().asProductionSite())) {
                            structureTexture = this.resourcesLoader.getImage(cell.getStructure().getTexture());
                            this.highlightSelectedSite(graphics, coords,
                                    this.coordinatesManager.getHeightOffset(cell), this.coordinatesManager.getScale());
                        }

                        else {
                            structureTexture = this.resourcesLoader.getImage(cell.getStructure().getTexture());
                        }
                        final float vOffset = structureTexture.getHeight() - 64;
                        structureTexture.draw(coords.getX(),
                                coords.getY() - this.coordinatesManager.getHeightOffset(cell)
                                        - vOffset
                                        * this.coordinatesManager.getScale(), this.coordinatesManager.getScale());
                    }
                }
                // Draw "back structure" of the bridges
                if (cell.containsStructure() && cell.getStructure().getStructureType() == StructureTypes.TRACK) {
                    final Vector2f coords = this.coordinatesManager.getCoordinatesByPosition(x, y);
                    if (cell.getStructure().asTrack().getType() == TrackTypes.SEA_BRIDGE) {
                        final Image image =
                                this.resourcesLoader.getImage(cell.getStructure().getTexture() + "_BACK");
                        final float vOffset = image.getHeight() - 120;
                        image.draw(coords.getX(),
                                coords.getY() - this.coordinatesManager.getHeightOffset(cell)
                                        - vOffset
                                        * this.coordinatesManager.getScale(),
                                this.coordinatesManager.getScale());
                    } else if (cell.getStructure().asTrack().getType() == TrackTypes.RIVER_BRIDGE) {
                        final Image image =
                                this.resourcesLoader.getImage(cell.getStructure().getTexture() + "_BACK");
                        final float vOffset = image.getHeight() - 110;
                        image.draw(coords.getX(),
                                coords.getY() - this.coordinatesManager.getHeightOffset(cell)
                                        - vOffset
                                        * this.coordinatesManager.getScale(),
                                this.coordinatesManager.getScale());
                    }
                }

                final Coordinate coordinate = new Coordinate(x, y);
                if (components.containsKey(coordinate)) {
                    final List<Pair<Train, Integer>> pairList = components.get(new Coordinate(x, y));
                    for (final Pair<Train, Integer> pair : pairList) {
                        if (pair.getSecond() == null) {
                            this.trainViewer.drawLocomotive(pair.getFirst());
                        } else {
                            this.trainViewer.drawWagon(pair.getFirst(), pair.getSecond());
                        }
                    }
                }

                // Draw Mountains! after train (hides the train more)
                if (cell.getCellType() == CellTypes.MOUNTAIN) {
                    final Vector2f coords = this.coordinatesManager.getCoordinatesByPosition(x, y);
                    final Image cellTexture = this.resourcesLoader.getImage(cell.getTextureName());
                    final float vOffset = cellTexture.getHeight() - 64;
                    if (this.transparentMountains) {
                        cellTexture.draw(coords.getX(),
                                coords.getY() - this.coordinatesManager.getHeightOffset(cell)
                                        - vOffset
                                        * this.coordinatesManager.getScale(),
                                this.coordinatesManager.getScale(),
                                WorldViewer.MOUNTAINS_TRANSPARENT_FILTER);
                    } else {
                        cellTexture.draw(coords.getX(), coords.getY() - this.coordinatesManager.getHeightOffset(cell)
                                - vOffset
                                * this.coordinatesManager.getScale(),
                                this.coordinatesManager.getScale());
                    }
                }


            }
        }


        // Renders selected station radius cells
        if (this.station != null) {
            this.renderSelectedStationRadius(graphics);
            final Vector2f coords =
                    this.coordinatesManager.getCoordinatesByPosition(this.station.getX(), this.station.getY());
            final Image structureTexture = this.resourcesLoader.getImage(this.station.getTexture());
            final float vOffset = structureTexture.getHeight() - 64;
            structureTexture.draw(
                    coords.getX(),
                    coords.getY()
                            - this.coordinatesManager.getHeightOffset(this.terrain.getCell(this.station.getX(),
                                    this.station.getY()))
                            - vOffset
                            * this.coordinatesManager.getScale(), this.coordinatesManager.getScale());
        }

        // draw top (front structure) texture of the tracks (makes the train go behind it)
        for (int y = (int) lowBoundaries.getY(); y < highBoundaries.getY(); ++y) {
            for (int x = (int) lowBoundaries.getX(); x < highBoundaries.getX(); ++x) {
                final Vector2f coords = this.coordinatesManager.getCoordinatesByPosition(x, y);
                if (this.coordinatesManager.areCoordinatesOnScreen(coords)) {
                    final ICell cell = this.terrain.getCell(x, y);
                    this.renderCellTopTexture(graphics, coords.getX(), coords.getY(), cell);
                }
            }
        }

        // Draw extraction sites amount.
        if (this.canShowExtractionSiteAmount()) {
            this.renderProductionSitesAmount(graphics);
        }

        // draw station names.
        if (this.canShowStationName()) {
            this.renderStationNames(graphics);
        }

        // draw city names.
        if (this.canShowCityName()) {
            this.renderCityNames(graphics);
        }
    }

    /**
     * Render production sites amounts.
     *
     * @param graphics
     *            graphics.
     */
    private void renderProductionSitesAmount(final Graphics graphics) {
        for (final IProductionSite productionSite : Game.getInstance().getWorld().getProductionSites()) {
            final Vector2f coords = this.coordinatesManager.getCoordinatesByPosition(productionSite.getX(),
                    productionSite.getY());
            if (this.coordinatesManager.areCoordinatesOnScreen(coords)) {
                GraphicsUtils.drawShadowText(graphics,
                        productionSite.getWare().toString(),
                        coords.getX() - 20,
                        coords.getY() - 20);
            }
        }
    }

    /**
     * Renders selected station radius cells as highlighted.
     *
     * @param graphics
     *            graphics.
     */
    private void renderSelectedStationRadius(final Graphics graphics) {
        final CoordinatesManager manager = CoordinatesManager.getInstance();
        final Vector2f lowBoundaries = manager.getLowBoundaries();
        final Vector2f highBoundaries = manager.getHighBoundaries();
        for (int y = (int) lowBoundaries.getY(); y < highBoundaries.getY(); ++y) {
            for (int x = (int) lowBoundaries.getX(); x < highBoundaries.getX(); ++x) {
                final Vector2f coords = manager.getCoordinatesByPosition(x, y);
                if (manager.areCoordinatesOnScreen(coords)) {
                    final ICell cell = this.terrain.getCell(x, y);
                    if (cell.isOwned()
                            && (x > this.station.getX() - this.station.getRadius()
                                    || x < this.station.getX() + this.station.getRadius()
                                    || y > this.station.getY() - this.station.getRadius() || y < this.station.getY()
                                    + this.station.getRadius())) {
                        final Image texture = ResourcesLoader.getInstance().getImage("HIGHLIGHT_POSITIVE");
                        texture.setAlpha(0.5f);
                        texture.draw(coords.getX(), coords.getY() - manager.getHeightOffset(cell), manager.getScale());
                    }
                }
            }
        }
    }

    /**
     * Render station names.
     *
     * @param graphics
     *            graphics.
     */
    private void renderStationNames(final Graphics graphics) {
        for (final Station station : Game.getInstance().getWorld().getStationManager().getStations()) {
            final Vector2f coords = this.coordinatesManager.getCoordinatesByPosition(station.getX(), station.getY());
            if (this.coordinatesManager.areCoordinatesOnScreen(coords)) {
                GraphicsUtils.drawShadowText(graphics, station.getName(), coords.getX() - 20, coords.getY() - 20);
            }
        }
    }

    /**
     * Render city names.
     *
     * @param graphics
     *            graphics.
     */
    private void renderCityNames(final Graphics graphics) {
        for (final City city : Game.getInstance().getWorld().getCities()) {
            final Vector2f coords = this.coordinatesManager.getCoordinatesByPosition(city.getX(), city.getY());
            if (this.coordinatesManager.areCoordinatesOnScreen(coords)) {
                GraphicsUtils.drawShadowText(graphics, city.getName(), coords.getX() - 20, coords.getY() - 40);
            }
        }
    }

    /**
     * Render a single cell.
     *
     * @param graphics
     *            graphics.
     * @param coordX
     *            x coordinate.
     * @param coordY
     *            y coordinate.
     * @param cell
     *            cell to draw.
     */
    private void renderCell(final Graphics graphics, final float coordX, final float coordY, final ICell cell) {
        // draw cell texture.
        Image cellTexture = this.resourcesLoader.getImage(cell.getTextureName());
        if (cell.getCellType() == CellTypes.MOUNTAIN) {
            // mountains are weird.
            final MountainCell mountainCell = cell.asMountainCell();
            cellTexture = this.resourcesLoader.getImage(mountainCell.getGroundTextureName());
        }

        cellTexture.draw(coordX,
                coordY - this.coordinatesManager.getHeightOffset(cell),
                this.coordinatesManager.getScale());

        this.transitionViewer.render(graphics, coordX, coordY, cell);

        // draw tracks.
        if (cell.containsStructure() && cell.getStructure().getStructureType() == StructureTypes.TRACK) {
            final Image texture = this.resourcesLoader.getImage(cell.getStructure().getTexture());
            texture.draw(coordX, coordY - this.coordinatesManager.getHeightOffset(cell)
                    - this.coordinatesManager.getScale(), this.coordinatesManager.getScale());
        }
    }

    /**
     * Render a single cell texture which has to be drawn after the train.
     *
     * @param graphics
     *            graphics.
     * @param coordX
     *            x coordinate.
     * @param coordY
     *            y coordinate.
     * @param cell
     *            cell to draw.
     */
    private void
            renderCellTopTexture(final Graphics graphics, final float coordX, final float coordY, final ICell cell) {
        if (cell.containsStructure() && cell.getStructure().getStructureType() == StructureTypes.TRACK) {
            if (cell.getStructure().asTrack().getType() == TrackTypes.SEA_BRIDGE) {
                final Image topImage = this.resourcesLoader.getImage(cell.getStructure().getTexture() + "_TOP");
                final float vOffset = topImage.getHeight() - 120;
                topImage.draw(coordX, coordY - this.coordinatesManager.getHeightOffset(cell)
                        - vOffset
                        * this.coordinatesManager.getScale(), this.coordinatesManager.getScale());
            } else if (cell.getStructure().asTrack().getType() == TrackTypes.RIVER_BRIDGE) {
                final Image topImage = this.resourcesLoader.getImage(cell.getStructure().getTexture() + "_TOP");
                final float vOffset = topImage.getHeight() - 110;
                topImage.draw(coordX, coordY - this.coordinatesManager.getHeightOffset(cell)
                        - vOffset
                        * this.coordinatesManager.getScale(), this.coordinatesManager.getScale());
            }
        }

    }

    /**
     * Updates the selected station cells to highlight.
     *
     * @param station
     *            selected station.
     */
    private void updateHighlightedCells(final Station station) {
        this.highlightedCells.clear();
        for (int y = -station.getRadius(); y <= station.getRadius(); ++y) {
            for (int x = -station.getRadius(); x <= station.getRadius(); ++x) {
                if (this.terrain.areCoordinatesInRange(x + station.getX(), y + station.getY()) && (x != 0 || y != 0)) {
                    this.highlightedCells.add(new Vector2f(x + station.getX(), y + station.getY()));
                }
            }
        }
    }

    /**
     * Render a single cell texture which has to be drawn after the train.
     *
     * @param graphics
     *            graphics.
     * @param coords
     *            both x and y coordinate.
     * @param cellOffset
     *            cells offset.
     * @param scale
     *            scale.
     */
    public void highlightSelectedSite(final Graphics graphics, final Vector2f coords,
            final float cellOffset, final float scale) {
        if (this.productionSite != null) {
            final Image image = this.resourcesLoader.getImage("BUILDING_TILE_GLOW");
            final float vOffset = image.getHeight() - 64;
            final float slaceOffsetX = scale * 15;
            final float slaceOffsetY = scale * 18;
            image.draw(coords.getX() - slaceOffsetX, coords.getY() - cellOffset - vOffset * scale + slaceOffsetY, scale);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reload() {
        this.terrain = Game.getInstance().getWorld().getTerrain();
    }

    /**
     * Set transparency of mountains.
     *
     * @param value
     *            true for transparent mountains, false for default.
     */
    public void setMountainTransparent(final boolean value) {
        this.transparentMountains = value;
    }

    /**
     * Sets the radius of the station highlighted, visible.
     *
     * @param station
     *            selected station.
     */
    public void enableStationRadius(final Station station) {
        this.station = station;
        if (station != null) {
            this.updateHighlightedCells(station);
            this.productionSite = null;
        }

    }

    /**
     * Sets selected production site.
     *
     * @param productionSite
     *            selected production site.
     */
    public void highlightSelected(final IProductionSite productionSite) {
        if (productionSite != null) {
            this.productionSite = productionSite;
            this.station = null;
        } else {
            this.productionSite = null;
        }
    }
}
