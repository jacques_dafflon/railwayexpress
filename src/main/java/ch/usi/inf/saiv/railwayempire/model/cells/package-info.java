/**
 * Provides the functionality of having different types of structures and areas (cells) in the world.
 *
 * GroundCell - Every structure is placed on ground cell, including forests and other structures.
 *
 * SeaCell - they are water cells which are too deep for constructing any bridge if the depth is more than 2,
 * otherwise you are allowed to build bridge.
 *
 * RiverCell - represent a river which flows from the highest ground cell into the sea cell if
 * relatively close. It is possible to build bridge on the river.
 *
 * MountainCell - represents cells which contain mountains. It is possible to build a tunnel
 * through the mountains.
 */
package ch.usi.inf.saiv.railwayempire.model.cells;