package ch.usi.inf.saiv.railwayempire.model.production_sites;

import ch.usi.inf.saiv.railwayempire.model.structures.IStructure;
import ch.usi.inf.saiv.railwayempire.model.wagons.CoalWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.FoodWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.IronWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.MailWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.OilWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.PassengersWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.RockWagon;
import ch.usi.inf.saiv.railwayempire.model.wagons.WoodWagon;
import ch.usi.inf.saiv.railwayempire.model.wares.IWare;

/**
 * Interface for raw materials extraction sites.
 */
public interface IProductionSite extends IStructure {
    
    /**
     * Returns the amount of material produced in a minute
     *
     * @return the amount produced each minute
     */
    double getAmountPerMinute();
    
    /**
     * Returns the amount currently stored.
     *
     * @return amount of ware.
     */
    double getAmount();
    
    /**
     * Return the maximal amount a site can store.
     *
     * @return max capacity.
     */
    double getAmountCapacity();
    
    /**
     * Get the ware.
     *
     * @return ware.
     */
    IWare getWare();
    
    /**
     * Load mail into the mail wagon.
     *
     * @param wagon
     *            mail wagon.
     * @return time spent to load.
     */
    double load(MailWagon wagon);
    
    /**
     * Load passengers into the passengers wagon.
     *
     * @param wagon
     *            passengers wagon.
     * @return time spent to load.
     */
    double load(PassengersWagon wagon);
    
    /**
     * Load coal into the coal wagon.
     *
     * @param wagon
     *            coal wagon.
     * @return time spent to load.
     */
    double load(CoalWagon wagon);
    
    /**
     * Load food into the food wagon.
     *
     * @param wagon
     *            food wagon.
     * @return time spent to load.
     */
    double load(FoodWagon wagon);
    
    /**
     * Load oil into the oil wagon.
     *
     * @param wagon
     *            oil wagon.
     * @return time spent to load.
     */
    double load(OilWagon wagon);
    
    /**
     * Load wood into the wood wagon.
     *
     * @param wagon
     *            wood wagon.
     * @return time spent to load.
     */
    double load(WoodWagon wagon);
    
    /**
     * Load iron into the iron wagon.
     *
     * @param wagon
     *            iron wagon.
     * @return time spent to load.
     */
    double load(IronWagon wagon);
    
    /**
     * Load rock into the rock wagon.
     *
     * @param wagon
     *            rock wagon.
     * @return time spent to load.
     */
    double load(RockWagon wagon);
    
    /**
     * Tells whether the production site is owned by the player.
     * @return True if the player owns the production site false otherwise.
     */
    boolean isOwnedByPlayer();
    /**
     * Gets the name of the site owner.
     *
     * @return the name of the owner.
     */
    String getOwnersName();
    
    /**
     * Changes the name of the site owner.
     *
     * @param ownersName
     *            name of the new owner.
     */
    void setOwnersName(final String ownersName);
    
    /**
     * The cost of buying production site.
     *
     * @return cost of the site.
     */
    int getCost();
}
