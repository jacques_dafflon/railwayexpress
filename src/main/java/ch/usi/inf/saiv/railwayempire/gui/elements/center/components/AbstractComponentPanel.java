package ch.usi.inf.saiv.railwayempire.gui.elements.center.components;

import java.text.NumberFormat;
import java.util.Locale;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.model.trains.TrainComponent;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;

/**
 * Displays the details of a train component and allows the user to buy it or to add it to the currently selected train.
 * 
 */
public abstract class AbstractComponentPanel extends AbstractPanel {
    
    /**
     * The height of a line of text.
     */
    private static final int LINE_HEIGHT = 25;
    
    /**
     * Padding around the panel.
     */
    private static final int PADDING = 10;
    
    /**
     * Ratio image : icon of the component texture image.
     */
    private static final float IMAGE_SCALE = 2f;
    
    /**
     * The width of the icon.
     */
    private static final int ICON_WIDTH = 156;
    
    /**
     * The height of the icon.
     */
    private static final int ICON_HEIGHT = 120;
    
    /**
     * The x offset of the component image at which to start to extract the icon.
     */
    private static final int IMAGE_OFFSET_X = 100;
    
    /**
     * The y offset of the component image at which to start to extract the icon
     */
    private static final int IMAGE_OFFSET_Y = 30;
    
    /**
     * Available Header.
     */
    private static final String AVAILABLE = "Available";
    
    /**
     * Price header.
     */
    private static final String PRICE = "Price";
    
    /**
     * The bigger font, for the name of the component.
     */
    private static final UnicodeFont COMPONENT_FONT = FontUtils.resizeFont(FontUtils.FONT, 18);
    
    /**
     * The icon of the component.
     */
    private final Image componentIcon;
    
    /**
     * Formatter used for the price tag.
     */
    private final NumberFormat formatter;
    
    /**
     * Constructor for an abstract component panel.
     * 
     * @param newShape
     *            Some shape.
     * @param componentImage
     *            The image of the component.
     */
    protected AbstractComponentPanel(final Shape newShape, final Image componentImage) {
        super(newShape);
        this.componentIcon = componentImage;
        
        this.formatter = NumberFormat.getCurrencyInstance(Locale.US);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        //Buttons
        if (this.isComponentAvailable()) {
            this.getButton().enableButton();
        } else {
            this.getButton().disableButton();
        }
        
        super.render(container, graphics);
        
        //Icon
        graphics.setColor(Color.white);
        int posX = this.getX() + AbstractComponentPanel.PADDING;
        float posY = this.getY();
        graphics.drawImage(this.componentIcon, posX, posY,
            posX + AbstractComponentPanel.ICON_WIDTH, posY + AbstractComponentPanel.ICON_HEIGHT,
            AbstractComponentPanel.IMAGE_OFFSET_X, AbstractComponentPanel.IMAGE_OFFSET_Y,
            AbstractComponentPanel.IMAGE_OFFSET_X
                + AbstractComponentPanel.ICON_WIDTH * AbstractComponentPanel.IMAGE_SCALE,
            AbstractComponentPanel.IMAGE_OFFSET_Y
                + AbstractComponentPanel.ICON_HEIGHT * AbstractComponentPanel.IMAGE_SCALE);
        
        //Name
        posX += 156 + AbstractComponentPanel.PADDING;
        posY = this.getY() + AbstractComponentPanel.PADDING;
        FontUtils.drawLeft(AbstractComponentPanel.COMPONENT_FONT, this.getComponent().getName(), posX, (int) posY);
        
        //Availability
        final int rightMargin = graphics.getFont().getWidth(AbstractComponentPanel.AVAILABLE);
        posX = this.getButton().getX() - rightMargin - AbstractComponentPanel.PADDING;
        posY = this.getY() + AbstractComponentPanel.PADDING;
        graphics.drawString(AbstractComponentPanel.AVAILABLE, posX, posY);
        posY += AbstractComponentPanel.LINE_HEIGHT;
        final String availability = this.getComponentAvailability() + " units";
        graphics.drawString(availability,
            posX + rightMargin - graphics.getFont().getWidth(availability), posY);
        
        //Price
        posY += AbstractComponentPanel.LINE_HEIGHT;
        graphics.drawString(AbstractComponentPanel.PRICE, posX, posY);
        posY += AbstractComponentPanel.LINE_HEIGHT;
        final String priceTag = this.formatter.format(this.getComponent().getCost());
        graphics.drawString(priceTag, posX + rightMargin - graphics.getFont().getWidth(priceTag), posY);
        
    }
    
    /**
     * @return The button.
     */
    protected abstract Button getButton();
    
    /**
     * Get the image of the component.
     * 
     * @return The image of the component.
     */
    public Image getComponentImage() {
        return this.componentIcon;
    }
    
    /**
     * Indicates if the player has this component available or not.
     * 
     * @return <code>true</code> if the player has at least one instance of the component available.
     */
    public abstract boolean isComponentAvailable();
    
    /**
     * Get the component as a generic train component.
     * 
     * @return The component related to the panel.
     */
    public abstract TrainComponent getComponent();
    
    /**
     * Get the number of instances of the component available to the player.
     * 
     * @return The number of instances available.
     */
    public abstract int getComponentAvailability();
    
    /**
     * The action to perform when the button of the panel is clicked.
     */
    public abstract void buttonAction();
    
    /**
     * Get the scale at which to draw the icon.
     * 
     * @return the scale
     */
    protected static float getIconScale() {
        return AbstractComponentPanel.IMAGE_SCALE;
    }
    
    /**
     * Get the padding.
     * 
     * @return The padding
     */
    protected static int getPadding() {
        return AbstractComponentPanel.PADDING;
    }
    
    /**
     * Get the height of a line.
     * 
     * @return The height of a line.
     */
    protected static int getLineHeight() {
        return AbstractComponentPanel.LINE_HEIGHT;
    }
    
    /**
     * Get the width of the icon.
     * 
     * @return The width of the icon.
     */
    protected static int getIconWidth() {
        return AbstractComponentPanel.ICON_WIDTH;
    }
    
    /**
     * Get the height of the icon.
     * 
     * @return The height of the icon.
     */
    public static int getIconHeight() {
        return AbstractComponentPanel.ICON_HEIGHT;
    }
}
