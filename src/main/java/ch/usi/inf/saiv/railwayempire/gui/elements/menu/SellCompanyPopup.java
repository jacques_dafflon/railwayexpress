package ch.usi.inf.saiv.railwayempire.gui.elements.menu;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.GameplayController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Pop up shown to the user when he ask to sell the company.
 */
public class SellCompanyPopup extends SimplePanel {

    /**
     * Background color.
     */
    private static Color BACKGROUNDCOLOR = new Color(57, 51, 46);

    /**
     * Distance between buttons.
     */
    private static int BUTTONDISTANCE = 20;

    /**
     * Create a pop up to help the user exit if he doesn't like this awesome
     * game.
     * 
     * @param newShape
     *            Shape of the pop up.
     * @param controller
     *            controller.
     * @param container
     *            container.
     * @param pauseMenuPanel
     *            the parent.
     */
    public SellCompanyPopup(final Shape newShape, final GameplayController controller, final GameContainer container,
            final PauseMenuPanel pauseMenuPanel) {
        super(newShape);

        final Image exitImage = ResourcesLoader.getInstance().getImage("OK_BUTTON");
        final Image cancelImage = ResourcesLoader.getInstance().getImage("CANCEL_BUTTON");

        final Button exitButton = new Button(exitImage, ResourcesLoader.getInstance().getImage("OK_BUTTON_HOVER"),
                ResourcesLoader.getInstance().getImage("OK_BUTTON_PRESSED"), this.getX() + this.getWidth() / 2
                + BUTTONDISTANCE, this.getY() + this.getHeight() / 2 + BUTTONDISTANCE);
        final Button cancelButton = new Button(cancelImage,
                ResourcesLoader.getInstance().getImage("CANCEL_BUTTON_HOVER"), ResourcesLoader.getInstance().getImage(
                        "CANCEL_BUTTON_PRESSED"), this.getX() + this.getWidth() / 2 - cancelImage.getWidth()
                        - BUTTONDISTANCE, this.getY() + this.getHeight() / 2 + BUTTONDISTANCE);

        exitButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                controller.endGame();
            }
        });

        cancelButton.setListener(new MouseListener() {

            @Override
            public void actionPerformed(final Button button) {
                pauseMenuPanel.toggleSellCompanyPopup();
            }
        });

        this.add(exitButton);
        this.add(cancelButton);

        final String text = "Are you sure you want to sell your company and end the game?";
        this.add(new Label(this.getX(), this.getY() + this.getHeight() / 2
                - container.getGraphics().getFont().getLineHeight() - BUTTONDISTANCE, this.getWidth(),
                this.getHeight(), text, Alignment.CENTER));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        graphics.setColor(BACKGROUNDCOLOR);
        graphics.fillRoundRect(this.getX(), this.getY(), this.getWidth(), this.getHeight(), 15);
        graphics.setColor(Color.black);
        graphics.drawRoundRect(this.getX(), this.getY(), this.getWidth() - 1, this.getHeight() - 1, 15);
        super.render(container, graphics);
    }


}
