package ch.usi.inf.saiv.railwayempire.utilities;

import ch.usi.inf.saiv.railwayempire.gui.viewers.LogViewer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Class used to log everything from everywhere.
 */
public final class Log {

    /**
     * Singleton pattern: variable.
     */
    private static final Log LOG_INSTANCE = new Log();
    /**
     * Redirection for output, standard set to console.
     */
    private static PrintStream outputPrintStream = System.out;
    /**
     * Redirection for error output.
     */
    private static PrintStream errorPrintStream = System.err;

    /**
     * Empty constructor for Log class, never to be used.
     */
    private Log() {
    }

    /**
     * Singleton pattern: getInstance.
     *
     * @return Log instance.
     */
    public static Log getInstance() {
        return LOG_INSTANCE;
    }

    /**
     * Logs the received object to the standard output.
     *
     * @param obj
     *         The object to log.
     */
    public static void out(final Object obj) {
        if (SystemConstants.LOG_OUT) {
            outputPrintStream.println(obj);
        }
    }

    /**
     * Prints stack trace.
     *
     * @param exception
     *         Exception to print.
     */
    public static void exception(final Exception exception) {
        if (SystemConstants.LOG_EXCEPTION) {
            exception.printStackTrace();
        }
    }

    /**
     * Prints error message.
     *
     * @param obj
     *         Error message to print.
     */
    public static void error(final Object obj) {
        if (SystemConstants.LOG_ERROR) {
            errorPrintStream.println(obj);
        }
    }

    /**
     * Prints the log message to the screen.
     *
     * @param obj
     *         The message to print.
     */
    public static void screen(final Object obj) {
        LogViewer.getInstance().addMessage(obj);
    }

    /**
     * Logs the received object to the file /tmp/RailwayManiaLog.txt.
     *
     * @param obj
     *         String to log
     */
    public static void toFile(final Object obj) {
        toFile(obj, "/tmp/RailwayManiaLog.txt");
    }

    /**
     * Logs the received object to the given file. The file will be located in /tmp/.
     *
     * @param obj
     *         the object to log
     * @param fileName
     *         the filename of the file to make.
     */
    public static void toFile(final Object obj, final String fileName) {
        try {
            final File file = new File("/tmp/" + fileName);

            if (!file.exists() && !file.createNewFile()) {
                Log.out("ERROR: Cannot create file");
            }

            final FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            final BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(obj.toString());
            bufferedWriter.close();
        } catch (final IOException exception) {
            Log.out("ERROR: Cannot write file");
        }
    }
}
