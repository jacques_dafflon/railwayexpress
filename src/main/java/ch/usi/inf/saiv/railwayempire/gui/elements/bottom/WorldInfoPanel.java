package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.GameModes;
import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.IGuiComponent;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Info panel for the world view.
 */
public final class WorldInfoPanel extends AbstractInfoPanel {
    
    /**
     * The margin between buttons.
     */
    private static final int BUTTONS_MARGIN = 5;
    /**
     * The width of a small button.
     */
    private static final int BUTTON_WIDTH = ResourcesLoader.getInstance().getImage("BUILD_STRUCTURE_BUTTON").getWidth();
    /**
     * The height of a small button.
     */
    private static final int BUTTON_HEIGHT = ResourcesLoader.getInstance().getImage("NORMAL_MODE_BUTTON").getHeight();
    /**
     * The left panel
     */
    private AbstractWorldInfoUtilityPanel utilityPanel;
    /**
     * The right panel
     */
    private final AbstractPanel rightPanel;
    
    /**
     * Constructor.
     *
     * @param rectangle
     *            yes it's a shape!.
     * @param modesController
     *            controller.
     */
    public WorldInfoPanel(final Rectangle rectangle, final GameModesController modesController) {
        super(rectangle);
        
        modesController.initInfoPanel(
            new Rectangle(
                this.getX(),
                this.getY() + WorldInfoPanel.BUTTONS_MARGIN,
                this.getWidth() - WorldInfoPanel.BUTTON_WIDTH,
                this.getHeight()),
                this);
        
        this.rightPanel = new SimplePanel(
            new Rectangle(
                this.getX() + this.getWidth() - WorldInfoPanel.BUTTON_WIDTH,
                this.getY() + WorldInfoPanel.BUTTONS_MARGIN,
                WorldInfoPanel.BUTTON_WIDTH,
                this.getHeight()));
        
        final Button buildingButton = new Button(
            ResourcesLoader.getInstance().getImage("BUILD_STRUCTURE_BUTTON"),
            ResourcesLoader.getInstance().getImage("BUILD_STRUCTURE_BUTTON_HOVER"),
            ResourcesLoader.getInstance().getImage("BUILD_STRUCTURE_BUTTON_PRESSED"),
            this.rightPanel.getX(),
            this.getButtonVerticalPosition(1));
        buildingButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                if(!buildingButton.isButtonPressed()){
                    WorldInfoPanel.this.releaseAllButtons();
                    buildingButton.holdButton();
                    modesController.enterMode(GameModes.BUILDING);
                } else {
                    WorldInfoPanel.this.releaseAllButtons();
                    modesController.enterMode(GameModes.NORMAL);
                }
            }
        });
        this.rightPanel.add(buildingButton);
        
        final Button stationButton = new Button(
            ResourcesLoader.getInstance().getImage("BUILD_STATION_BUTTON"),
            ResourcesLoader.getInstance().getImage("BUILD_STATION_BUTTON_HOVER"),
            ResourcesLoader.getInstance().getImage("BUILD_STATION_BUTTON_PRESSED"),
            this.rightPanel.getX(),
            this.getButtonVerticalPosition(2));
        stationButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                if(!stationButton.isButtonPressed()){
                    WorldInfoPanel.this.releaseAllButtons();
                    stationButton.holdButton();
                    modesController.enterMode(GameModes.STATIONS_ALL);
                } else {
                    WorldInfoPanel.this.releaseAllButtons();
                    modesController.enterMode(GameModes.NORMAL);
                }
            }
        });
        this.rightPanel.add(stationButton);
        
        final Button trackButton = new Button(
            ResourcesLoader.getInstance().getImage("BUILD_TRACKS_BUTTON"),
            ResourcesLoader.getInstance().getImage("BUILD_TRACKS_BUTTON_HOVER"),
            ResourcesLoader.getInstance().getImage("BUILD_TRACKS_BUTTON_PRESSED"),
            this.rightPanel.getX(),
            this.getButtonVerticalPosition(3));
        trackButton.setListener(new MouseListener() {
            
            @Override
            public void actionPerformed(final Button button) {
                if(!trackButton.isButtonPressed()){
                    WorldInfoPanel.this.releaseAllButtons();
                    trackButton.holdButton();
                    modesController.enterMode(GameModes.TRACKS);
                } else {
                    WorldInfoPanel.this.releaseAllButtons();
                    modesController.enterMode(GameModes.NORMAL);
                }
            }
        });
        this.rightPanel.add(trackButton);
        
        this.add(this.rightPanel);
    }
    
    /**
     * Set current panel.
     *
     * @param newPanel
     *            DUDE! :D
     *
     */
    public void setUtilityPanel(final AbstractWorldInfoUtilityPanel newPanel) {
        this.utilityPanel = newPanel;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        super.keyPressed(key, character);
        this.utilityPanel.keyPressed(key, character);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        this.utilityPanel.render(container, graphics);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        super.mouseClicked(button, coordX, coordY, clickCount);
        if (this.utilityPanel.contains(coordX, coordY)) {
            this.utilityPanel.mouseClicked(button, coordX, coordY, clickCount);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        super.mousePressed(button, coordX, coordY);
        if (this.utilityPanel.contains(coordX, coordY)) {
            this.utilityPanel.mousePressed(button, coordX, coordY);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        super.mouseReleased(button, coordX, coordY);
        if (this.utilityPanel.contains(coordX, coordY)) {
            this.utilityPanel.mouseReleased(button, coordX, coordY);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        super.mouseDragged(oldX, oldY, newX, newY);
        if (this.utilityPanel.contains(newX, newY) || this.utilityPanel.contains(oldX, oldY)) {
            this.utilityPanel.mouseDragged(oldX, oldY, newX, newY);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        super.mouseMoved(oldX, oldY, newX, newY);
        if (this.utilityPanel.contains(newX, newY) || this.utilityPanel.contains(oldX, oldY)) {
            this.utilityPanel.mouseMoved(oldX, oldY, newX, newY);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        super.mouseWheelMoved(charge, coordX, coordY);
        if (this.utilityPanel.contains(coordX, coordY)) {
            this.utilityPanel.mouseWheelMoved(charge, coordX, coordY);
        }
    }
    
    /**
     * Returns the vertical position of the button given the button number.
     *
     * @param buttonNumber The number of the button.
     * @return The vertical offset.
     */
    private int getButtonVerticalPosition(final int buttonNumber) {
        return this.getY() + WorldInfoPanel.BUTTONS_MARGIN * buttonNumber + WorldInfoPanel.BUTTON_HEIGHT * (buttonNumber - 1);
    }
    
    /**
     * Unhold all buttons.
     */
    private void releaseAllButtons() {
        for (final IGuiComponent button : this.rightPanel.getChildrenComponents()) {
            ((Button) button).unHoldButton();
        }
    }
}
