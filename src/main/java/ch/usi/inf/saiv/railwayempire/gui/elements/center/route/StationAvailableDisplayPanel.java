package ch.usi.inf.saiv.railwayempire.gui.elements.center.route;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Panel representing an available station which can be added to the current train's route.
 * 
 */
public class StationAvailableDisplayPanel extends StationDisplayPanel {
    
    /**
     * The icon of the station.
     */
    private final Image icon;
    
    /**
     * The panel managing the train route.
     */
    private final TrainRoutePanel panel;
    
    /**
     * Instantiate a new panel representing the given available station.
     * 
     * @param rectangle
     *            Position and location of the panel as a rectangle.
     * @param displayStation
     *            The station to display.
     * @param trainRoutePanel
     *            The panel holding this panel.
     */
    public StationAvailableDisplayPanel(final Rectangle rectangle,
        final Station displayStation,
        final TrainRoutePanel trainRoutePanel) {
        super(rectangle, displayStation);
        this.icon = ResourcesLoader.getInstance().getImage(this.getStation().getTexture());
        this.panel = trainRoutePanel;
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        this.icon.draw(this.getX() + StationDisplayPanel.getInternalMargin() * 0.5f,
            this.getY() + StationDisplayPanel.getInternalMargin() - (this.icon.getHeight() - 64) * 0.25f,
            0.5f);
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    protected float getTextXOffset() {
        return TrainRoutePanel.getStationListElementHeight();
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        if (this.getShape().contains(newX, newY)) {
            this.panel.highlightStation(this.getStation());
        } else if (this.getShape().contains(oldX, oldY)) {
            this.panel.unhighlightStation(this.getStation());
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        this.panel.addStation(this.getStation());
        this.panel.unhighlightStation(this.getStation());
    }
    
}
