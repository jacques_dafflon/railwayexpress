package ch.usi.inf.saiv.railwayempire.gui.elements.center.components;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Panel representing the details of a wagon.
 * 
 */
public abstract class AbstractWagonPanel extends AbstractComponentPanel {
    
    /**
     * The wagon represented by the panel.
     */
    private final IWagon component;
    
    /**
     * Instantiate a new panel.
     * 
     * @param newShape
     *            The shape of the panel.
     * @param wagon
     *            The wagon to represent.
     */
    public AbstractWagonPanel(final Shape newShape, final IWagon wagon) {
        super(newShape, ResourcesLoader.getInstance().getImage(wagon.getTextureName() + "_E"));
        this.component = wagon;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        super.render(container, graphics);
        
        //Details
        final float posX = this.getX() + 2
            * AbstractComponentPanel.getPadding()
            + AbstractComponentPanel.getIconWidth();
        float posY = this.getY() + AbstractComponentPanel.getPadding()
            + 2 * AbstractComponentPanel.getLineHeight();
        
        graphics.drawString("Capacity " + (int) this.component.getUnitLimit(), posX, posY);
        posY += AbstractComponentPanel.getLineHeight();
        graphics.drawString("Carries " + this.component.getWare().getTypeName(), posX, posY);
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public IWagon getComponent() {
        return this.component;
    }
    
    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public int getComponentAvailability() {
        return Game.getInstance().getPlayer().getWareHouse().getComponentAvailability(this.component);
    }
}
