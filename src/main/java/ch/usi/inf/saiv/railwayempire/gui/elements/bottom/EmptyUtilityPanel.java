package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;

/**
 * A placeholder panel.
 */
public final class EmptyUtilityPanel extends AbstractWorldInfoUtilityPanel {
    
    /**
     * Create the panel.
     *
     * @param rectangle
     *            rectangle.
     * @param modesController
     *            modes controller.
     */
    public EmptyUtilityPanel(final Rectangle rectangle, final GameModesController modesController) {
        super(rectangle);
    }
}
