package ch.usi.inf.saiv.railwayempire.gui.viewers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * Represents a logger for the screen.
 */
public final class LogViewer implements IViewer {
    /**
     * Message uptime in milliseconds.
     */
    private static final float MESSAGE_UPTIME = 5000;
    /**
     * Fade out duration in milliseconds.
     */
    private static final int FADE_OUT_DURATION = 500;
    /**
     * The unique instance of the logger.
     */
    private static final LogViewer UNIQUE_INSTANCE = new LogViewer();
    /**
     * The number of lines to keep logged.
     */
    private static final int LINES = 15;
    /**
     * The height for each message.
     */
    private static final int MESSAGE_HEIGHT = 16;
    /**
     * The margin to leave from the sides.
     */
    private static final int MARGIN = 10;
    /**
     * The data to log on screen.
     */
    private final List<Object> logs;
    /**
     * Save the time each object has been there.
     */
    private final List<Date> uptime;
    
    /**
     * The constructor for the logger.
     */
    private LogViewer() {
        this.logs = new ArrayList<Object>();
        this.uptime = new ArrayList<Date>();
    }
    
    /**
     * Getter for the unique instance of the logger.
     *
     * @return The logger.
     */
    public static LogViewer getInstance() {
        return LogViewer.UNIQUE_INSTANCE;
    }
    
    /**
     * Adds the given message to the log.
     *
     * @param message
     *         The message to add.
     */
    public void addMessage(final Object message) {
        if (this.logs.size() == LogViewer.LINES) {
            this.logs.remove(0);
            this.uptime.remove(0);
        }
        this.logs.add(message);
        this.uptime.add(new Date());
    }
    
    /**
     * Renders the log messages on screen.
     * @param graphics
     *         The graphics on which to draw.
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        final int startY = container.getHeight() - SystemConstants.BOTTOM_PANE_HEIGHT
                - LogViewer.MARGIN
                - this.logs.size()
                * LogViewer.MESSAGE_HEIGHT;
        final Date now = new Date();
        for (int index = 0; index < this.logs.size(); ++index) {
            final Date then = this.uptime.get(index);
            float opacity = 1;
            if (now.getTime() - then.getTime() > LogViewer.MESSAGE_UPTIME) {
                opacity = 1 - ((now.getTime() - then.getTime() - LogViewer.MESSAGE_UPTIME) / LogViewer.FADE_OUT_DURATION);
            }
            this.drawShadowOpaqueText(graphics,
                    this.logs.get(index).toString(),
                    LogViewer.MARGIN,
                    startY + LogViewer.MESSAGE_HEIGHT * index,
                    opacity);
        }
        
        this.removeOldMessages();
    }
    
    /**
     * Remove messages that has been there for a while.
     */
    private void removeOldMessages() {
        final Date now = new Date();
        for (int i = 0; i < this.logs.size(); ++i) {
            final Date then = this.uptime.get(i);
            if (now.getTime() - then.getTime() > LogViewer.MESSAGE_UPTIME + LogViewer.FADE_OUT_DURATION) {
                this.logs.remove(i);
                this.uptime.remove(i);
                --i;
            }
        }
    }
    
    /**
     * Draw shadowed opaque text.
     * 
     * @param graphics
     *            graphics.
     * @param text
     *            string to draw.
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     * @param opacity
     *            opacity between 0 and 1.
     */
    private void drawShadowOpaqueText(final Graphics graphics,
            final String text,
            final int posX,
            final int posY,
            final float opacity) {
        graphics.setColor(new Color(0, 0, 0, opacity));
        graphics.drawString(text, posX + 2, posY + 2);
        graphics.setColor(new Color(255, 255, 255, opacity));
        graphics.drawString(text, posX, posY);
    }
}
