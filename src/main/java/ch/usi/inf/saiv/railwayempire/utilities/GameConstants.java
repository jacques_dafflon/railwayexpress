package ch.usi.inf.saiv.railwayempire.utilities;


/**
 * Auxiliary class for constant value and methods,
 * useful to avoid the annoying code quality warnings.
 */
public final class GameConstants {

    // TERRAIN
    /**
     * Preset size of the world.
     */
    public static final int WORLD_SIZE = 512;
    /**
     * The limit for the water.
     */
    public static final double WATER_LEVEL = 0.4;
    /**
     * The number of rivers generated in a terrain.
     */
    public static final int RIVERS_NUMBER = 5;
    /**
     * Limit for the number of iterations when creating the cities.
     */
    public static final int CITIES_ITERATION_LIMIT = 200;
    /**
     * Minimum number of cities to be created.
     */
    public static final int MIN_CITIES = 16;
    /**
     * Maximum number of cities to be created.
     */
    public static final int MAX_CITIES = 40;
    /**
     * Minimum distance between two cities.
     */
    public static final int MIN_CITIES_DISTANCE = 20;
    /**
     * The default octaves number used in the noise.
     */
    public static final int OCTAVES = 5;
    /**
     * The default amount of persistence used in the noise.
     */
    public static final double PERSISTENCE = 0.45;

    // GAME
    /**
     * The default start year for a new game.
     */
    public static final long START_YEAR = 1980;
    /**
     * The initial amount of money available to the player.
     */
    public static final long INITIAL_MONEY = 50000000;
    /**
     * The default name for a new player.
     */
    public static final String DEFAULT_PLAYER_NAME = "John Geek";
    /**
     * The default name for a new company.
     */
    public static final String DEFAULT_COMPANY_NAME = "Geek's and Co.";
    /**
     * The default number of how many different sea heights are allowed.
     */
    public static final int SEA_LEVELS = 9;

    // BUILDINGS
    /**
     * The radius of a small station.
     */
    public static final int SMALL_STATION_RADIUS = 2;
    /**
     * The radius of a medium station.
     */
    public static final int MEDIUM_STATION_RADIUS = 4;
    /**
     * The radius of a large station.
     */
    public static final int LARGE_STATION_RADIUS = 8;

    //PRODUCTION SITES
    /**
     * Minimum number of saw mills to be created.
     */
    public static final int MIN_SAW_MILL = 120;
    /**
     * Maximum number of saw mills to be created.
     */
    public static final int MAX_SAW_MILL = 150;
    /**
     * Minimum distance between two saw mills.
     */
    public static final int MIN_SAW_MILL_DISTANCE = 6;
    /**
     * Minimum number of coal mines to be created.
     */
    public static final int MIN_COAL_MINE = 30;
    /**
     * Maximum number of coal mines to be created.
     */
    public static final int MAX_COAL_MINE = 50;
    /**
     * Minimum distance between two coal mines.
     */
    public static final int MIN_COAL_MINE_DISTANCE = 6;
    /**
     * Minimum number of rock caves to be created.
     */
    public static final int MIN_ROCK_CAVE = 30;
    /**
     * Maximum number of rock caves to be created.
     */
    public static final int MAX_ROCK_CAVE = 50;
    /**
     * Minimum distance between two rock caves.
     */
    public static final int MIN_ROCK_CAVE_DISTANCE = 6;
    /**
     * Minimum number of farms to be created.
     */
    public static final int MIN_FARM = 150;
    /**
     * Maximum number of farms to be created.
     */
    public static final int MAX_FARM = 200;
    /**
     * Minimum distance between two farms.
     */
    public static final int MIN_FARM_DISTANCE = 2;
    /**
     * Minimum number of iron mines to be created.
     */
    public static final int MIN_IRON_MINE = 30;
    /**
     * Maximum number of iron mines to be created.
     */
    public static final int MAX_IRON_MINE = 50;
    /**
     * Minimum distance between two iron mines.
     */
    public static final int MIN_IRON_MINE_DISTANCE = 10;
    /**
     * Minimum number of drill rigs to be created.
     */
    public static final int MIN_DRILL_RIG = 50;
    /**
     * Maximum number of drill rigs to be created.
     */
    public static final int MAX_DRILL_RIG = 100;
    /**
     * Minimum distance between two drill rigs.
     */
    public static final int MIN_DRILL_RIG_DISTANCE = 10;

    // COSTS
    /**
     * The cost of cutting down 1 cell of forest.
     */
    public static final int FORES_CUT_DOWN_COST = 8000;
    /**
     * Cost of building a single track unit on the ground tile.
     */
    public static final int TRACK_COST = 2000;
    /**
     * Cost of building a single track unit on the tile which contains mountain structure.
     */
    public static final int TRACK_TUNNEL_COST = 50000;
    /**
     * Cost to buy a sea cell.
     */
    public static final int SEA_CELL_COST = 20000;
    /**
     * Cost to buy a ground cell.
     */
    public static final int GROUND_CELL_COST = 10000;
    /**
     * The cost of a small station.
     */
    public static final int SMALL_STATION_COST = 10000000;
    /**
     * The cost of a medium station.
     */
    public static final int MEDIUM_STATION_COST = 25000000;
    /**
     * The cost of a large station.
     */
    public static final int LARGE_STATION_COST = 50000000;
    /**
     * The cost of clearing out the forest before creating station.
     */
    public static final int STATION_FOREST_CUT = 100000;
    /**
     * The cost of a post office.
     */
    public static final int POST_OFFICE_COST = 20000;
    /**
     * The production rate of buildings.
     */
    public static final double BUILDING_PRODUCTION_RATE = 2.0;
    /**
     * The maximum amount of units per building.
     */
    public static final double BUILDING_MAX_UNIT = 200.0;
    /**
     * The production rate of farm.
     */
    public static final double FARM_PRODUCTION_RATE = 5.0;
    /**
     * The maximum amount of units per farm.
     */
    public static final double FARM_MAX_UNIT = 1000.0;
    /**
     * The production rate of coal mine.
     */
    public static final double COAL_MINE_PRODUCTION_RATE = 1.0;
    /**
     * The maximum amount of units per coal mine.
     */
    public static final double COAL_MINE_MAX_UNIT = 100.0;
    /**
     * The production rate of rock cave.
     */
    public static final double ROCK_CAVE_PRODUCTION_RATE = 1.0;
    /**
     * The maximum amount of units per rock cave.
     */
    public static final double ROCK_CAVE_MAX_UNIT = 100.0;
    /**
     * The production rate of saw mill.
     */
    public static final double SAW_MILL_PRODUCTION_RATE = 2.0;
    /**
     * The maximum amount of units per saw mill.
     */
    public static final double SAW_MILL_MAX_UNIT = 200.0;
    /**
     * The production rate of iron mine.
     */
    public static final double IRON_MINE_PRODUCTION_RATE = 1.0;
    /**
     * The maximum amount of units per iron mine.
     */
    public static final double IRON_MINE_MAX_UNIT = 100.0;
    /**
     * The production rate of drill rig.
     */
    public static final double OIL_RIG_PRODUCTION_RATE = 0.5;
    /**
     * The maximum amount of units per drill rig.
     */
    public static final double OIL_RIG_MAX_UNIT = 100.0;
    /**
     * The maximum amount of units per food factory.
     */
    public static final double FOOD_FACTORY_MAX_UNIT = 500.0;
    /**
     * The production rate of food factory.
     */
    public static final double FOOD_FACTORY_PRODUCTION_RATE = 4.0;
    /**
     * A happy pony.
     */
    public static final double POST_OFFICE_MAX_UNIT = 100;
    /**
     * A happy unicorn.
     */
    public static final double POST_OFFICE_PRODUCTION_RATE = 1;
    /**
     * The reduction of costs given by the ownership of production sites.
     */
    public static final double OWNERSHIP_REDUCTION = 0.5;

    // TILE TEXTURES
    /**
     * The tile theme to use.
     * The theme must be the string <XXX> from the file tiles-<XXX>.xml in the resources folder. (Case sensitive)
     */
    public static final String TILES_THEME = "smooth";
    /**
     * The number of different ground textures for the selected theme.
     */
    public static final int GROUND_TEXTURES_NUMBER = 3;
    /**
     * The number of different sea textures for the selected theme.
     */
    public static final int SEA_TEXTURES_NUMBER = 4;
    /**
     * The number of different forest textures for the selected theme.
     */
    public static final int FOREST_TEXTURES_NUMBER = 7;

    // POINTS
    /**
     * The points given for each linked station.
     */
    public static final int POINTS_FOR_LINKED_STATION = 10000;
    /**
     * The points given for each reached station. (by the a train)
     */
    public static final int POINTS_FOR_REACHED_CITY = 1000;

    /**
     * The slowdown factor applied to the train speed by one wagon.
     */
    public static final double WAGON_SLOWDOWN_FACTOR = 0.05;

    /**
     * Empty constructor.
     */
    public GameConstants() {
    }
}
