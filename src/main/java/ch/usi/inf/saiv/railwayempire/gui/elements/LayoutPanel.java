package ch.usi.inf.saiv.railwayempire.gui.elements;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;

/**
 * A layout panel holds GUI components and lays them out vertically or horizontally.
 * 
 */
public class LayoutPanel extends AbstractPanel {
    /**
     * Defines a horizontal panel.
     */
    public static final int HORIZONTAL = 1;
    /**
     * Defines a vertical panel.
     */
    public static final int VERTICAL = 2;
    /**
     * Orientation of the panel.
     */
    private final int orientation;
    /**
     * Padding.
     */
    private final int padding;
    /**
     * Offset.
     */
    private int offset;
    /**
     * Length.
     */
    private int length;
    
    /**
     * Create a new layout panel with the given dimensions and with inner components staked vertically, without padding.
     * 
     * @param rectangle
     *            The dimension and position of the panel.
     */
    public LayoutPanel(final Rectangle rectangle) {
        this(rectangle, LayoutPanel.VERTICAL, 0);
    }
    
    /**
     * Create a new layout panel with the given dimension and the with the inner components stacked with the given
     * orientation, without padding.
     * 
     * @param rectangle
     *            The dimension and position of the rectangle.
     * @param orientation
     *            The orientation of the panel, corresponds to the direction in which the inner components will be
     *            stacked.
     */
    public LayoutPanel(final Rectangle rectangle, final int orientation) {
        this(rectangle, orientation, 0);
    }
    
    /**
     * Create a new layout panel with the given dimension and the with the inner components stacked with the given
     * orientation and with the given padding.
     * 
     * @param rectangle
     *            The dimension and position of the rectangle.
     * @param orientation
     *            The orientation of the panel, corresponds to the direction in which the inner components will be
     *            stacked.
     * @param padding
     *            The padding between two inner components.
     */
    public LayoutPanel(final Rectangle rectangle, final int orientation, final int padding) {
        super(rectangle);
        this.orientation = orientation;
        this.padding = padding;
        this.offset = 0;
        this.length = 0;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        if (this.getBackgroundColor() != SystemConstants.NULL) {
            final Color previousColor = graphics.getColor();
            graphics.setColor(this.getBackgroundColor());
            graphics.fill(this.getShape());
            graphics.setColor(previousColor);
        }
        
        if (this.getBackgroundImage() != SystemConstants.NULL) {
            graphics.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight(),
                this.getBackgroundImage(), 0, 0);
        }
        
        graphics.setClip((Rectangle) this.getShape());
        for (final IGuiComponent component : this.getChildrenComponents()) {
            component.render(container, graphics);
        }
        graphics.clearClip();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void add(final IGuiComponent component) {
        super.add(component);
        this.updateAbsoluteLength();
        this.updateInnerComponentsPosition();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void remove(final IGuiComponent component) {
        super.remove(component);
        this.updateAbsoluteLength();
        this.updateInnerComponentsPosition();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseWheelMoved(final int change, final int coordX, final int coordY) {
        if (!this.isScrollable()) {
            return;
        }
        final int scrollChange = change / 4;
        this.scroll(scrollChange);
    }
    
    /**
     * Scroll the panel by the specified value.
     * 
     * Negative values scrolls down (or right) to the latest element in the panel.
     * Positive values scrolls up (or left) to the first element in the panel.
     * 
     * @param scrollChange
     *            The value by which to scroll.
     */
    public void scroll(final int scrollChange) {
        this.offset = this.offset + scrollChange < this.getLength(this, this.orientation) - this.length ?
            this.getLength(this, this.orientation) - this.length
            : this.offset + scrollChange > 0 ? 0 : this.offset + scrollChange;
        this.updateInnerComponentsPosition();
    }
    
    /**
     * Update Inner components position.
     */
    private void updateInnerComponentsPosition() {
        int index = this.getCoordinate(this, this.orientation) + this.offset;
        for (final IGuiComponent component : this.getChildrenComponents()) {
            this.setPosition(component, index, this.orientation);
            index += this.padding + this.getLength(component, this.orientation);
        }
        
    }
    
    /**
     * Get x or y coordinate of a specific component.
     * 
     * @param component
     *            component.
     * @param side
     *            side.
     * @return coordinate.
     */
    private int getCoordinate(final IGuiComponent component, final int side) {
        final int coord;
        switch (side) {
            case HORIZONTAL:
                coord = component.getX();
                break;
            case VERTICAL:
                coord = component.getY();
                break;
            default:
                coord = SystemConstants.ZERO;
                break;
        }
        return coord;
    }
    
    /**
     * Get width or height of a specific component.
     * 
     * @param component
     *            component.
     * @param side
     *            side.
     * @return width or height.
     */
    protected int getLength(final IGuiComponent component, final int side) {
        final int length;
        switch (side) {
            case HORIZONTAL:
                length = component.getWidth();
                break;
            case VERTICAL:
                length = component.getHeight();
                break;
            default:
                length = SystemConstants.ZERO;
                break;
        }
        return length;
    }
    
    /**
     * Set position of given component.
     * 
     * @param component
     *            component.
     * @param position
     *            position.
     * @param side
     *            side.
     */
    private void setPosition(final IGuiComponent component, final int position, final int side) {
        switch (side) {
            case HORIZONTAL:
                component.setLocation(position, this.getY());
                break;
            case VERTICAL:
                component.setLocation(this.getX(), position);
                break;
            default:
                // should never happen.
                component.setLocation(this.getX(), this.getY());
                break;
        }
    }
    
    /**
     * Update absolute length.
     */
    private void updateAbsoluteLength() {
        this.length = 0;
        for (final IGuiComponent component : this.getChildrenComponents()) {
            this.length += this.getLength(component, this.orientation) + this.padding;
        }
    }
    
    /**
     * Check if can scroll the panel.
     * 
     * @return true if we can/should scroll.
     */
    protected boolean isScrollable() {
        return this.length > this.getLength(this, this.orientation);
    }
    
    /**
     * The absolute length of all the children of the panel.
     * 
     * The length is the sum of the width or the height (depending on the orientation) of all the children components.
     * If {@link #isScrollable()} returns <code>false</code> this should return a value smaller than the length of the
     * panel.
     * If {@link #isScrollable()} returns <code>true</code> this should return a value larger than the length of the
     * panel.
     * 
     * @return The absolute length of the panel.
     */
    protected int getAbsoluteLength() {
        return this.length;
    }
    
    /**
     * Return the offset at which the children panels are located.
     * 
     * @return the offset of the children panels location.
     */
    public int getOffset() {
        return this.offset;
    }
}
