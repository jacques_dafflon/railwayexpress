package ch.usi.inf.saiv.railwayempire.gui.states;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import ch.usi.inf.saiv.railwayempire.controllers.CenterPanelsController;
import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.controllers.GameplayController;
import ch.usi.inf.saiv.railwayempire.controllers.TrainSelectionController;
import ch.usi.inf.saiv.railwayempire.gui.GameState;
import ch.usi.inf.saiv.railwayempire.gui.elements.IGuiComponent;
import ch.usi.inf.saiv.railwayempire.gui.elements.SimplePanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.TrainListPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.BottomPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.CenterPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.menu.PauseMenuPanel;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.IPersistent;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.Time;

/**
 * Game play state.
 */
public final class GameplayState extends BasicGameState implements IPersistent {

    /**
     * List of gui elements.
     */
    private final List<IGuiComponent> guiComponents;
    /**
     * The instance of the game.
     */
    private Game game;
    /**
     * The controller of this screen.
     */
    private GameplayController controller;
    /**
     * Game container, used to get input, coordinates, screen size, etc
     */
    private GameContainer gameContainer;
    /**
     * Additional panel to be rendered.
     */
    private SimplePanel additionalPanel;
    /**
     * Some panel.
     */
    private PauseMenuPanel pauseMenuPanel;

    private StateBasedGame dick;

    /**
     * Creates the game state.
     */
    public GameplayState() {
        this.guiComponents = new ArrayList<IGuiComponent>();
        Game.addPersistent(this);
        this.game = Game.getInstance();
    }

    /**
     * Returns the id of this state.
     *
     * @return The id of the state.
     */
    @Override
    public int getID() {
        return GameState.GAMEPLAY.ordinal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(final GameContainer container, final StateBasedGame stateBasedGame) throws SlickException {
        this.dick = stateBasedGame;
    }

    /**
     * Automatically called when keys are pressed.
     *
     * @param key
     *            The integer corresponding to the key.
     * @param character
     *            The character corresponding to the key.
     */
    @Override
    public void keyPressed(final int key, final char character) {
        switch (key) {
            case Input.KEY_ESCAPE:
                this.controller.togglePauseScreen();
                break;
            case Input.KEY_M:
                this.game.getPlayer().addMoney(20000000);
                break;
            case Input.KEY_C:
                CoordinatesManager.getInstance().centerCameraOnTracks();
                break;

            case Input.KEY_N:
                this.dick.enterState(GameState.ENDGAME.ordinal());
                break;
            default:
                for (final IGuiComponent component : this.guiComponents) {
                    component.keyPressed(key, character);
                }
                break;
        }
    }

    /**
     * Automatically called when the mouse is dragged.
     *
     * @param oldX
     *            The x coordinate before the drag.
     * @param oldY
     *            The y coordinate before the drag.
     * @param newX
     *            The x coordinate after the drag.
     * @param newY
     *            The y coordinate after the drag.
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        if (Time.getInstance().isPaused()) {
            if (this.additionalPanel != null) {
                this.additionalPanel.mouseMoved(oldX, oldY, newX, newY);
            } else {
                this.pauseMenuPanel.mouseMoved(oldX, oldY, newX, newY);
            }
        } else {
            for (final IGuiComponent component : this.guiComponents) {
                if (component.contains(newX, newY) || component.contains(oldX, oldY)) {
                    component.mouseMoved(oldX, oldY, newX, newY);
                }
            }
        }
    }

    /**
     * Automatically called when the mouse is dragged.
     *
     * @param oldX
     *            The x coordinate before the drag.
     * @param oldY
     *            The y coordinate before the drag.
     * @param newX
     *            The x coordinate after the drag.
     * @param newY
     *            The y coordinate after the drag.
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        if (Time.getInstance().isPaused()) {
            if (this.additionalPanel != null) {
                this.additionalPanel.mouseDragged(oldX, oldY, newX, newY);
            } else {
                this.pauseMenuPanel.mouseDragged(oldX, oldY, newX, newY);
            }
        } else {

        }
        for (final IGuiComponent component : this.guiComponents) {
            if (component.contains(newX, newY) || component.contains(oldX, oldY)) {
                component.mouseDragged(oldX, oldY, newX, newY);
            }
        }
    }

    /**
     * Automatically called when the mouse is clicked.
     *
     * @param button
     *            The button which was clicked.
     * @param coordX
     *            The x coordinate of the click.
     * @param coordY
     *            The y coordinate of the click.
     * @param clickCount
     *            The number of clicks.
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        if (Time.getInstance().isPaused()) {
            if (this.additionalPanel != null) {
                this.additionalPanel.mouseClicked(button, coordX, coordY, clickCount);
            } else {
                this.pauseMenuPanel.mouseClicked(button, coordX, coordY, clickCount);
            }
        } else {
            for (final IGuiComponent component : this.guiComponents) {
                if (component.contains(coordX, coordY)) {
                    component.mouseClicked(button, coordX, coordY, clickCount);
                }
            }
        }
    }

    /**
     * Automatically called when the mouse is pressed.
     *
     * @param button
     *            The button which was clicked.
     * @param coordX
     *            The x coordinate of the click.
     * @param coordY
     *            The y coordinate of the click.
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        if (Time.getInstance().isPaused()) {
            if (this.additionalPanel != null) {
                this.additionalPanel.mousePressed(button, coordX, coordY);
            } else {
                this.pauseMenuPanel.mousePressed(button, coordX, coordY);
            }
        } else {
            for (final IGuiComponent component : this.guiComponents) {
                if (component.contains(coordX, coordY)) {
                    component.mousePressed(button, coordX, coordY);
                }
            }
        }
    }

    /**
     * Automatically called when the mouse is released.
     *
     * @param button
     *            The button which was clicked.
     * @param coordX
     *            The x coordinate of the click.
     * @param coordY
     *            The y coordinate of the click.
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        if (Time.getInstance().isPaused()) {
            if (this.additionalPanel != null) {
                this.additionalPanel.mouseReleased(button, coordX, coordY);
            } else {
                this.pauseMenuPanel.mouseReleased(button, coordX, coordY);
            }
        } else {
            for (final IGuiComponent component : this.guiComponents) {
                if (component.contains(coordX, coordY)) {
                    component.mouseReleased(button, coordX, coordY);
                }
            }
        }
    }

    /**
     * Automatically called when entering the state, starts the game.
     *
     * @param container
     *            The container of the game.
     * @param stateBasedGame
     *            The instance of the game.
     */
    @Override
    public void enter(final GameContainer container, final StateBasedGame stateBasedGame) {
        this.game.startGame();
        CoordinatesManager.getInstance().setViewSize(container.getWidth() - SystemConstants.RIGHT_PANE_WIDTH,
            container.getHeight() - SystemConstants.BOTTOM_PANE_HEIGHT);
        CoordinatesManager.getInstance().centerCameraOnTracks();

        this.controller = new GameplayController(stateBasedGame, container, this);

        this.pauseMenuPanel = new PauseMenuPanel(new Rectangle(0, 0, container.getWidth(), container.getHeight()),
            this.controller, container);
        this.gameContainer = container;

        final int mainViewWidth = container.getWidth() - SystemConstants.RIGHT_PANE_WIDTH;
        final int mainViewHeight = container.getHeight() - SystemConstants.BOTTOM_PANE_HEIGHT;

        final GameModesController modesController = new GameModesController();
        final TrainSelectionController trainSelectionController = new TrainSelectionController();
        final CenterPanelsController centerPanelsController = new CenterPanelsController(modesController,
            trainSelectionController, container);

        final CenterPanel CenterPanel = new CenterPanel(new Rectangle(SystemConstants.ZERO,
            SystemConstants.ZERO,
            mainViewWidth,
            mainViewHeight), centerPanelsController);
        this.guiComponents.add(CenterPanel);

        //Train panel
        final TrainListPanel trainPanel = new TrainListPanel(new Rectangle(mainViewWidth, 0,
            SystemConstants.RIGHT_PANE_WIDTH, container.getHeight() - SystemConstants.BOTTOM_PANE_HEIGHT),
            trainSelectionController);
        trainPanel.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND"));
        this.guiComponents.add(trainPanel);

        final BottomPanel bottomPanel = new BottomPanel(new Rectangle(SystemConstants.ZERO,
            mainViewHeight,
            container.getWidth(),
            SystemConstants.BOTTOM_PANE_HEIGHT),
            modesController,
            centerPanelsController);
        this.guiComponents.add(bottomPanel);

    }

    /**
     * Automatically called when the mouse wheel is moved.
     *
     * @param charge
     *            The number of ticks the wheel has moved.
     */
    @Override
    public void mouseWheelMoved(final int charge) {
        final int coordX = this.gameContainer.getInput().getAbsoluteMouseX();
        final int coordY = this.gameContainer.getInput().getAbsoluteMouseY();
        if (Time.getInstance().isPaused()) {
            if (this.additionalPanel != null) {
                this.additionalPanel.mouseWheelMoved(charge, coordX, coordY);
            } else {
                this.pauseMenuPanel.mouseWheelMoved(charge, coordX, coordY);
            }
        } else {
            for (final IGuiComponent component : this.guiComponents) {
                if (component.contains(coordX, coordY)) {
                    component.mouseWheelMoved(charge, coordX, coordY);
                }
            }
        }
    }

    /**
     * Renders the game and the gui on top of it.
     *
     * @param container
     *            The container of the game.
     * @param stateBasedGame
     *            The instance of the game.
     * @param graphics
     *            The graphics to draw on.
     * @throws SlickException
     *             An exception.
     */
    @Override
    public void render(final GameContainer container, final StateBasedGame stateBasedGame,
        final Graphics graphics) throws SlickException {

        graphics.setFont(FontUtils.FONT);

        for (final IGuiComponent component : this.guiComponents) {
            component.render(container, graphics);
        }
        if (Time.getInstance().isPaused()) {
            if (this.additionalPanel != null) {
                this.additionalPanel.render(container, graphics);
            } else {
                this.pauseMenuPanel.render(container, graphics);
            }
        }
    }

    /**
     * Updates the model according to the time passed.
     *
     * @param container
     *            The container of the game.
     * @param stateBasedGame
     *            The instance of the game.
     * @param delta
     *            The time passed since the last update in milliseconds.
     * @throws SlickException
     *             An exception.
     */
    @Override
    public void update(final GameContainer container, final StateBasedGame stateBasedGame,
        final int delta) throws SlickException {
        Time.getInstance().step(delta);
        final Input input = container.getInput();
        final int mouseX = input.getAbsoluteMouseX();
        final int mouseY = input.getAbsoluteMouseY();

        if (input.isKeyDown(Input.KEY_LEFT)) {
            CoordinatesManager.getInstance().updateOffset(
                SystemConstants.KEYBOARD_SCROLLING_SPEED * delta / 1000, 0);
        }
        if (input.isKeyDown(Input.KEY_RIGHT)) {
            CoordinatesManager.getInstance().updateOffset(
                -SystemConstants.KEYBOARD_SCROLLING_SPEED * delta / 1000, 0);
        }
        if (input.isKeyDown(Input.KEY_UP)) {
            CoordinatesManager.getInstance().updateOffset(0,
                SystemConstants.KEYBOARD_SCROLLING_SPEED * delta / 1000);
        }
        if (input.isKeyDown(Input.KEY_DOWN)) {
            CoordinatesManager.getInstance().updateOffset(0,
                -SystemConstants.KEYBOARD_SCROLLING_SPEED * delta / 1000);
        }

        this.scrollScreen(mouseX, mouseY, delta);
    }

    /**
     * Updates the offset of the world based on the position of the mouse and the time passed since the last update.
     *
     * @param mouseX
     *            The x coordinate of the mouse.
     * @param mouseY
     *            The y coordinate of the mouse.
     * @param delta
     *            The time in milliseconds since the last update.
     */
    public void scrollScreen(final int mouseX, final int mouseY, final int delta) {
        int horizontalScroll = 0;
        int verticalScroll = 0;

        // Calculate displacement based on delta from last update
        final int displacement = new Float(SystemConstants.MOUSE_SCROLLING_SPEED * delta / 1000).intValue();
        if (mouseX >= this.gameContainer.getWidth() - SystemConstants.SCROLLING_TOLERANCE) {
            horizontalScroll = -displacement;
        } else if (mouseX <= SystemConstants.SCROLLING_TOLERANCE) {
            horizontalScroll = displacement;
        }
        if (mouseY >= this.gameContainer.getHeight() - SystemConstants.SCROLLING_TOLERANCE) {
            verticalScroll = -displacement;
        } else if (mouseY <= SystemConstants.SCROLLING_TOLERANCE) {
            verticalScroll = displacement;
        }
        CoordinatesManager.getInstance().updateOffset(horizontalScroll, verticalScroll);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reload() {
        this.game = Game.getInstance();
    }

    /**
     * Sets additional panel.
     *
     * @param panel
     *          The panel to set as additional panel.
     */
    public void setAdditionalPanel(final SimplePanel panel) {
        this.additionalPanel = panel;
    }
}
