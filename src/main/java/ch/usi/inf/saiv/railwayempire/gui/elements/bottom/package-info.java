/**
 * Represent all of the Graphical User Interface components used at the lower part of the application window.
 */
package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;