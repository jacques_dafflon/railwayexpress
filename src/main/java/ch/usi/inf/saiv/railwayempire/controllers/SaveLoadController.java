package ch.usi.inf.saiv.railwayempire.controllers;

import ch.usi.inf.saiv.railwayempire.gui.elements.menu.SaveGamePanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.menu.SaveLoadPanel;

/**
 * Controller for the save and load.
 */
public class SaveLoadController {

    /**
     * The related panel.
     */
    private final SaveLoadPanel savePanel;

    /**
     * Construct for the controller.
     *
     * @param savePanel
     *            the panel related.
     */
    public SaveLoadController(final SaveLoadPanel savePanel) {
        this.savePanel = savePanel;
    }

    /**
     * Signal the saveLoadPanel that a new file has been selected by the user.
     * This file will be used in case the save/load functions are called. In
     * addition calls the update of the graphics.
     *
     * @param saveGamePanel
     *              newly selected panel.
     */
    public void signal(final SaveGamePanel saveGamePanel) {
        this.savePanel.setFile(saveGamePanel);
    }
}
