package ch.usi.inf.saiv.railwayempire.gui.viewers;


import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.geom.Vector2f;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.IPersistent;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;


/**
 * The class responsible for converting the coordinates from the in-game system to the screen coordinate.
 */
public final class CoordinatesManager implements IPersistent {
    
    /**
     * The unique instance of the coordinates manager.
     */
    private static final CoordinatesManager UNIQUE_INSTANCE = new CoordinatesManager();
    /**
     * The offset at which the terrain is drawn.
     */
    private final Vector2f offset;
    /**
     * The minimum position from which to start rendering the terrain.
     */
    private final Vector2f lowBoundaries;
    /**
     * The maximum position, where to stop rendering the terrain.
     */
    private final Vector2f highBoundaries;
    /**
     * The terrain of the world.
     */
    private Terrain terrain;
    /**
     * The current scale factor.
     */
    private float scale;
    /**
     * The transformation to go from terrain to screen.
     */
    private Transform transform;
    /**
     * The transformation to go from screen to terrain.
     */
    private Transform inverseTransform;
    /**
     * Width of the visible part of the screen.
     */
    private int viewWidth;
    /**
     * Height of the visible part of the screen.
     */
    private int viewHeight;
    
    /**
     * Creates a new coordinates manager.
     */
    private CoordinatesManager() {
        this.terrain = Game.getInstance().getWorld().getTerrain();
        Game.addPersistent(this);
        this.offset = new Vector2f();
        this.lowBoundaries = new Vector2f();
        this.highBoundaries = new Vector2f();
        this.scale = SystemConstants.DEFAULT_SCALE;
        this.updateTransforms();
        this.computeBoundaries();
    }
    
    /**
     * Set the visible part of the screen.
     * 
     * @param width
     *            width.
     * @param height
     *            height.
     */
    public void setViewSize(final int width, final int height) {
        this.viewWidth = width;
        this.viewHeight = height;
    }
    
    /**
     * The getter for the unique instance.
     *
     * @return CoordinatesManager instance.
     */
    public static CoordinatesManager getInstance() {
        return CoordinatesManager.UNIQUE_INSTANCE;
    }
    
    /**
     * Set new terrain.
     *
     * @param newTerrain
     *         new terrain.
     */
    public void updateTerrain(final Terrain newTerrain) {
        this.terrain = newTerrain;
        this.updateTransforms();
        this.computeBoundaries();
    }
    
    /**
     * Getter for the low boundaries.
     *
     * @return The low boundaries.
     */
    public Vector2f getLowBoundaries() {
        return this.lowBoundaries;
    }
    
    /**
     * Getter for the high boundaries.
     *
     * @return The high boundaries.
     */
    public Vector2f getHighBoundaries() {
        return this.highBoundaries;
    }
    
    /**
     * Getter for the scale.
     *
     * @return The scale.
     */
    public float getScale() {
        return this.scale;
    }
    
    /**
     * The getter for the transformation matrix.
     *
     * @return The transformation matrix.
     */
    public Transform getTransform() {
        return this.transform;
    }
    
    /**
     * Returns whether the given screen coordinate would be visible (not hidden by the gui or outside the screen).
     *
     * @param point
     *         The coordinate to check.
     * @return True if the coordinate is visible on screen, false otherwise.
     */
    public boolean areCoordinatesOnScreen(final Vector2f point) {
        return point.getX() >= -130
            && point.getY() >= -130
            && point.getX() < this.viewWidth + 50
            && point.getY() < this.viewHeight + 50;
    }
    
    /**
     * Tells whether a position on the map might be on screen when converted to screen coordinates.
     *
     * @param posX
     *         The x coordinate of the point.
     * @param posY
     *         The y coordinate of the point.
     * @return Returns true when the point might be on screen, false otherwise.
     */
    public boolean isPositionInScreenRectangle(final float posX, final float posY) {
        return posX >= this.lowBoundaries.getX() && posY >= this.lowBoundaries.getY() &&
            posX <= this.highBoundaries.getX() && posY <= this.highBoundaries.getY();
    }
    
    /**
     * Tells whether the given coordinate is strictly in the area of the screen which can be seen,
     * i.e. which is not covered by the gui.
     *
     * @param point
     *         The point to check.
     * @return True if the point is visible, false otherwise.
     */
    public boolean isCoordinateStrictlyOnScreen(final Vector2f point) {
        return point.getX() >= 0 && point.getY() >= 0
            && point.getX() < this.viewWidth
            && point.getY() < this.viewHeight;
    }
    
    /**
     * Returns the on-screen coordinates of the tile given the coordinates in the internal representation of the world.
     *
     * @param posX
     *         The x coordinate of the position on the terrain.
     * @param posY
     *         The y coordinate of the position on the terrain.
     * @return A vector representing coordinates on the screen.
     */
    public Vector2f getCoordinatesByPosition(final float posX, final float posY) {
        return this.transform.transform(new Vector2f(posX, posY));
    }
    
    /**
     * Returns the position on the terrain given by the coordinates on screen.
     *
     * @param coordX
     *         The x coordinate on screen.
     * @param coordY
     *         The y coordinate on screen.
     * @return A vectore storing the coordinates on the terrain.
     */
    public Vector2f getPositionByCoordinates(final float coordX, final float coordY) {
        final Vector2f wrong = this.inverseTransform.transform(new Vector2f(coordX - 64 * this.scale, coordY));
        wrong.set(Math.min(wrong.getX(), GameConstants.WORLD_SIZE - 1),
            Math.min(wrong.getY(),GameConstants.WORLD_SIZE - 1));
        wrong.set(Math.max(wrong.getX(), 0f), Math.max(wrong.getY(), 0f));
        
        final float cellHeightOffset = this.getHeightOffset(this.terrain.getCell((int) wrong.getX(), (int) wrong.getY()));
        final float yOff = coordY + cellHeightOffset;
        
        final Vector2f pos = this.inverseTransform.transform(new Vector2f(coordX - 64 * this.scale, yOff));
        pos.set(Math.min(pos.getX(), GameConstants.WORLD_SIZE - 1),
            Math.min(pos.getY(), GameConstants.WORLD_SIZE - 1));
        pos.set(Math.max(pos.getX(), 0f), Math.max(pos.getY(), 0f));
        return pos;
    }
    
    /**
     * Returns the y-coordinate offset in pixels derived from the height.
     *
     * @param modelHeight
     *         the height in the model.
     * @return the height offset in the screen.
     */
    public float getHeightOffset(final int modelHeight) {
        return modelHeight * this.scale * SystemConstants.TILE_HEIGHT_MULTIPLIER;
    }
    
    /**
     * Returns the y-coordinate offset in pixels derived from the cell height.
     *
     * @param cell
     *         cell.
     * @return the height offset in the screen.
     */
    public float getHeightOffset(final ICell cell) {
        if (cell.getCellType() == CellTypes.SEA) {
            return 0;
        } else {
            return cell.getHeight() * this.scale * SystemConstants.TILE_HEIGHT_MULTIPLIER;
        }
    }
    
    /**
     * Updates the offset at which the terrain is drawn.
     *
     * @param xDrag
     *         The horizontal displacement to apply to the terrain.
     * @param yDrag
     *         The vertical displacement to apply to the terrain.
     */
    public void updateOffset(final float xDrag, final float yDrag) {
        float newX = this.offset.getX() + xDrag;
        newX = Math.min(newX, (int) (GameConstants.WORLD_SIZE * 64 * this.getScale()));
        final float newY = this.offset.getY() + yDrag;
        this.offset.set(newX, newY);
        //Log.screen(this.offset);
        this.updateTransforms();
        this.computeBoundaries();
    }
    
    /**
     * Updates the transformations.
     */
    public void updateTransforms() {
        this.transform = new Transform();
        this.transform.concatenate(Transform.createTranslateTransform(this.offset.getX(), this.offset.getY()));
        this.transform.concatenate(Transform.createScaleTransform(90 * this.scale, 45 * this.scale));
        this.transform.concatenate(Transform.createRotateTransform((float) (Math.PI / 4)));
        
        this.inverseTransform = new Transform();
        this.inverseTransform.concatenate(Transform.createRotateTransform((float) (-Math.PI / 4)));
        this.inverseTransform.concatenate(Transform.createScaleTransform(1 / (90 * this.scale),
            1 / (45 * this.scale)));
        this.inverseTransform.concatenate(Transform.createTranslateTransform(-this.offset.getX(),
            -this.offset.getY()));
    }
    
    /**
     * Zooms in with the virtual camera.
     */
    public void zoomIn() {
        if (this.scale * SystemConstants.SCALE_FACTOR <= SystemConstants.MAX_SCALE) {
            final Vector2f pos = this.getPositionByCoordinates(this.viewWidth / 2, this.viewHeight / 2);
            
            this.scale = Math.min(SystemConstants.MAX_SCALE, this.scale * SystemConstants.SCALE_FACTOR);
            final double deltaX = Math.abs(0.5 * (this.viewWidth - this.viewWidth * this.scale));
            final double deltaY = Math.abs(0.5 * (this.viewHeight - this.viewHeight * this.scale));
            final int newX = (int) Math.round(this.getOffset().getX() - 0.5f * deltaX);
            final int newY = (int) Math.round(this.getOffset().getY() - deltaY);
            this.offset.set(newX, newY);
            this.updateTransforms();
            this.computeBoundaries();
            this.centerCameraOn(Math.round(pos.getX()), Math.round(pos.getY()));
        }
    }
    
    /**
     * Zooms out with the virtual camera.
     */
    public void zoomOut() {
        if (this.scale / SystemConstants.SCALE_FACTOR >= SystemConstants.MIN_SCALE) {
            final Vector2f pos = this.getPositionByCoordinates(this.viewWidth / 2, this.viewHeight / 2);
            
            this.scale = Math.max(SystemConstants.MIN_SCALE, this.scale / SystemConstants.SCALE_FACTOR);
            final double deltaX = Math.abs(0.5 * (this.viewWidth - this.viewWidth * this.scale));
            final double deltaY = Math.abs(0.5 * (this.viewHeight - this.viewHeight * this.scale));
            final int newX = (int) Math.round(this.getOffset().getX() + 0.5 * deltaX);
            final int newY = (int) Math.round(this.getOffset().getY() + deltaY);
            this.offset.set(newX, newY);
            this.updateTransforms();
            this.computeBoundaries();
            
            this.centerCameraOn(Math.round(pos.getX()), Math.round(pos.getY()));
        }
    }
    
    /**
     * Updates the boundaries of the terrain for which it makes sense to render the graphics.
     */
    public void computeBoundaries() {
        final int minX = (int) Math.max(this.getPositionByCoordinates(0, 0).getX() - 1, 0);
        final int maxX = (int) Math.min(this.getPositionByCoordinates(this.viewWidth, this.viewHeight).getX() + 1,
            this.terrain.getSize());
        final int minY = (int) Math.max(this.getPositionByCoordinates(this.viewWidth, 0).getY() - 1, 0);
        final int maxY = (int) Math.min(this.getPositionByCoordinates(0, this.viewHeight).getY() + 1,
            this.terrain.getSize());
        this.lowBoundaries.set(minX, minY);
        this.highBoundaries.set(maxX, maxY);
    }
    
    /**
     * Center camera on any track found in the world.
     */
    public void centerCameraOnTracks() {
        int posX = 0;
        int posY = 0;
        for (int i = 0; i < this.terrain.getSize(); ++i) {
            for (int j = 0; j < this.terrain.getSize(); ++j) {
                if (this.terrain.getCell(i, j).containsStructure()
                    && this.terrain.getCell(i, j).getStructure().getStructureType() == StructureTypes.TRACK) {
                    posX = i;
                    posY = j;
                }
            }
        }
        this.centerCameraOn(posX, posY);
    }
    
    /**
     * Centers the camera on the given position on the terrain.
     *
     * @param posX
     *         position x.
     * @param posY
     *         position y.
     */
    public void centerCameraOn(final float posX, final float posY) {
        final Vector2f zeros = this.getCoordinatesByPosition(0, 0);
        final Vector2f coords = this.getCoordinatesByPosition(posX, posY);
        final int offX = (int) (zeros.getX() - coords.getX() + this.viewWidth / SystemConstants.TWO);
        final int offY = (int) (zeros.getY() - coords.getY() + this.viewHeight / SystemConstants.TWO);
        this.offset.set(offX, offY);
        this.updateTransforms();
        this.computeBoundaries();
    }
    
    /**
     * Get the offset at which the terrain is drawn.
     *
     * @return The offset at which the terrain is drawn.
     */
    public Vector2f getOffset() {
        return this.offset;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void reload() {
        this.terrain = Game.getInstance().getWorld().getTerrain();
        this.scale = SystemConstants.DEFAULT_SCALE;
        this.updateTransforms();
        this.computeBoundaries();
    }
}
