package ch.usi.inf.saiv.railwayempire.model.wagons;

import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.trains.TrainComponent;
import ch.usi.inf.saiv.railwayempire.model.wares.IWare;

/**
 * Represents the interface for a wagon.
 */
public interface IWagon extends TrainComponent {

    /**
     * Getter for the amount of goods on the wagon.
     *
     * @return Amount of goods on the wagon.
     */
    double getLoadedAmount();

    /**
     * Getter for the limit of units.
     *
     * @return The limit of units.
     */
    double getUnitLimit();

    /**
     * Add goods on top of already present ones.
     *
     * @param toAdd
     *            amount to add.
     */
    void increaseAmountBy(final double toAdd);

    /**
     * Gets the time it takes to load/unload a whole wagon.
     *
     * @return The time.
     */
    double getLoadWaitingTime();

    /**
     * Tells whether the wagon is full.
     *
     * @return True if the wagon is full, false otherwise.
     */
    boolean isFull();

    /**
     * Tells whether the wagon is empty.
     *
     * @return True if the wagon is empty, false otherwise.
     */
    boolean isEmpty();

    /**
     * Returns the free space in the wagon.
     *
     * @return The free space.
     */
    double getFreeSpace();

    /**
     * Copy this wagon.
     *
     * @return a new wagon with same attributes as this one.
     */
    IWagon copy();

    /**
     * Load from a given production site.
     *
     * @param productionSite
     *            the production site.
     * @return time that it took to load (can be 0 if nothing happened).
     */
    double loadFrom(final IProductionSite productionSite);

    /**
     * Unload wares.
     *
     * @return The quantity of unloaded wares.
     */
    double unload();

    /**
     * Helper.
     *
     * @param ware
     *            IWare.
     * @return false
     */
    boolean checkIfValid(final IWare ware);

    /**
     * Tells whether the wagon carries passengers or not.
     *
     * @return True if this is a passenger wagon, false otherwise.
     */
    boolean isPassengerWagon();

    /**
     * return the ware.
     *
     * @return the Ware.
     */
    IWare getWare();

    /**
     * {@inheritDoc}
     */
    @Override
    String getTextureName();
}
