/**
 * City - provides an area of cells on terrain which are considered as part of the city. Initially city contains
 * people buildings and starting city also contains tracks.
 *
 * Terrain - represents the main "object" where everything is happening. All different types of cells are added
 * to terrain. Also all structures and trains are placed on the terrain.
 *
 * World - provides the environment for everything. World knows terrain, all cities, all production sites,
 * station manager and train manager.
 */
package ch.usi.inf.saiv.railwayempire.model;