package ch.usi.inf.saiv.railwayempire.model.wares;


/**
 * Enum for the ware types.
 */
public enum WareTypes {

    /**
     * Food.
     */
    FOOD,

    /**
     * Mail.
     */
    MAIL,

    /**
     * Passengers.
     */
    PASSENGERS,

    /**
     * Coal.
     */
    COAL,

    /**
     * Rock.
     */
    ROCK,

    /**
     * Wood.
     */
    WOOD,

    /**
     * Iron.
     */
    IRON,

    /**
     * Oil.
     */
    OIL
}
