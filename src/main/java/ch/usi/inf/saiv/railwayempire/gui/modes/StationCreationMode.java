package ch.usi.inf.saiv.railwayempire.gui.modes;


import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.controllers.GameModes;
import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Terrain;
import ch.usi.inf.saiv.railwayempire.model.cells.CellTypes;
import ch.usi.inf.saiv.railwayempire.model.cells.ICell;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.StationSizes;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.Log;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;
import ch.usi.inf.saiv.railwayempire.utilities.SystemConstants;
import ch.usi.inf.saiv.railwayempire.utilities.Time;


/**
 * Mode to handle user input and visualization of station creation.
 */
public final class StationCreationMode implements IMode {

    /**
     * The cells to highlight.
     */
    private final List<Vector2f> highlightedCells;
    /**
     * The currently selected station.
     */
    private final Station station;
    /**
     * The terrain of the world.
     */
    private final Terrain terrain;
    /**
     * The instance of resource loader.
     */
    private final ResourcesLoader resourcesLoader;
    /**
     * modes controller.
     */
    private final GameModesController modesController;
    /**
     * The x position of the selected cell.
     */
    private int stationX;
    /**
     * The y position of the selected cell.
     */
    private int stationY;

    /**
     * Default empty constructor.
     *
     * @param stationSize
     *         the size of the station.
     * @param newModesController
     *         the game mode controller.
     */
    public StationCreationMode(final StationSizes stationSize, final GameModesController newModesController) {
        this.station = new Station(0, 0, stationSize);
        this.modesController = newModesController;
        this.highlightedCells = new ArrayList<Vector2f>();
        this.resourcesLoader = ResourcesLoader.getInstance();
        this.terrain = Game.getInstance().getWorld().getTerrain();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int clickX, final int clickY, final int clickCount) {
        if (Time.getInstance().isPaused()) {
            return;
        }
        final Vector2f position = CoordinatesManager.getInstance().getPositionByCoordinates(clickX, clickY);
        if (button == SystemConstants.MOUSE_BUTTON_LEFT) {
            final int posX = (int) position.getX();
            final int posY = (int) position.getY();
            if (this.terrain.areCoordinatesInRange(posX, posY)) {
                if (this.terrain.canBuildStation(posX, posY, this.station.getRadius())) {
                    if (this.terrain.getCell(posX, posY).getStructure() != null) {
                        if (Game.getInstance().getPlayer().canAfford(this.station.getCost() + GameConstants.STATION_FOREST_CUT)) {
                            this.createStation(posX, posY, this.station.getCost() + GameConstants.STATION_FOREST_CUT);
                        } else {
                            Log.screen("You don't have enough money to build on forest!");
                        }
                    } else if (Game.getInstance().getPlayer().canAfford(this.station.getCost())) {
                        this.createStation(posX, posY, this.station.getCost());
                    } else {
                        Log.screen("You don't have enough money!");
                        // TODO: inform player
                    }
                } else {
                    Log.screen("You cannot build a station here!");
                    // TODO: inform player
                }
            } else {
                Log.screen("You cannot build outside the boudaries of the world!");
                // TODO: inform player. Is this actually possible?
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int posX, final int posY) {
        // implement if needed
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int posX, final int posY) {
        // implement if needed
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        // implement if needed
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        if (!Time.getInstance().isPaused()) {
            final Vector2f position = CoordinatesManager.getInstance().getPositionByCoordinates(newX, newY);
            this.stationX = (int) position.getX();
            this.stationY = (int) position.getY();
            this.updateHighlightedCells();
        }
    }

    /**
     * Updates the cells to highlight.
     */
    private void updateHighlightedCells() {
        this.highlightedCells.clear();
        for (int y = -this.station.getRadius(); y <= this.station.getRadius(); ++y) {
            for (int x = -this.station.getRadius(); x <= this.station.getRadius(); ++x) {
                if (this.terrain.areCoordinatesInRange(x + this.stationX, y + this.stationY) && (x != 0 || y != 0)) {
                    this.highlightedCells.add(new Vector2f(x + this.stationX, y + this.stationY));
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        // implement if needed
    }

    /**
     * Builds a station at given position and cost.
     *
     * @param posX
     *         x position in the world.
     * @param posY
     *         y position in the world.
     * @param cost
     *         cost of the station.
     */
    private void createStation(final int posX, final int posY, final int cost) {
        final Station newStation = new Station(posX, posY, this.station.getSize());
        final ICell cell = this.terrain.getCell(posX, posY);
        if (cell.containsStructure() && cell.getStructure().isRemovable()) {
            cell.removeStructure();
        }
        cell.setStructure(newStation);
        this.terrain.markCellsAsOwned(posX, posY, newStation.getRadius(), newStation);
        Game.getInstance().getPlayer().subtractMoney(cost);
        Game.getInstance().getWorld().getStationManager().addStation(newStation);
        newStation.attachTracks();

        this.modesController.enterMode(GameModes.NORMAL);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        final CoordinatesManager manager = CoordinatesManager.getInstance();
        final Vector2f lowBoundaries = manager.getLowBoundaries();
        final Vector2f highBoundaries = manager.getHighBoundaries();
        for (int y = (int) lowBoundaries.getY(); y < highBoundaries.getY(); ++y) {
            for (int x = (int) lowBoundaries.getX(); x < highBoundaries.getX(); ++x) {
                final Vector2f coords = manager.getCoordinatesByPosition(x, y);
                if (manager.areCoordinatesOnScreen(coords)) {
                    final ICell cell = this.terrain.getCell(x, y);
                    if (cell.isOwned()
                        && (x < this.stationX - this.station.getRadius()
                            || x > this.stationX + this.station.getRadius()
                            || y < this.stationY - this.station.getRadius() || y > this.stationY
                            + this.station.getRadius())) {
                        final Image texture = ResourcesLoader.getInstance().getImage("HIGHLIGHT_POSITIVE");
                        texture.setAlpha(0.5f);
                        texture.draw(coords.getX(), coords.getY() - manager.getHeightOffset(cell), manager.getScale());
                    }
                }
            }
        }

        final ICell stationCell = this.terrain.getCell(this.stationX, this.stationY);
        final boolean allInvalid = !stationCell.canBuildBuilding() || !this.validateCellHighlighting(stationCell);
        for (final Vector2f pos : this.highlightedCells) {
            final Vector2f coords = manager.getCoordinatesByPosition(pos.getX(), pos.getY());

            if (manager.areCoordinatesOnScreen(coords)) {
                final ICell cell = this.terrain.getCell((int) pos.getX(), (int) pos.getY());
                final Image texture;
                if (allInvalid || cell.isOwned() || stationCell.getCellType() != CellTypes.GROUND) {
                    texture = this.resourcesLoader.getImage("HIGHLIGHT_NEGATIVE");
                } else {
                    texture = this.resourcesLoader.getImage("HIGHLIGHT_POSITIVE");
                }
                texture.setAlpha(0.5f);
                texture.draw(coords.getX(), coords.getY() - manager.getHeightOffset(cell), manager.getScale());
            }
        }
        final ICell cell = this.terrain.getCell(this.stationX, this.stationY);
        final Vector2f coords = manager.getCoordinatesByPosition(this.stationX, this.stationY);
        final Image stationImage = this.resourcesLoader.getImage(this.station.getTexture());
        final float yOffset = stationImage.getHeight() - 64;
        stationImage.draw(coords.getX(), coords.getY() - manager.getHeightOffset(cell) - yOffset * manager.getScale(),
            manager.getScale());


        if (!allInvalid) {
            GraphicsUtils.drawCostBox(graphics,
                this.station.getCost() + cell.getCost(),
                container.getInput().getMouseX() + 50,
                container.getInput().getMouseY() + 50);
        }
    }

    /**
     * Method validates if the cell contains a structure or not or if the
     * structure can be removed. According to return it can be highlighted correctly.
     *
     * @param stationCell
     *            The cell to check.
     * @return Returns true if the cell can be highlighted, false otherwise.
     */
    private boolean validateCellHighlighting(final ICell stationCell) {
        if (stationCell.containsStructure()) {
            return stationCell.getStructure().isRemovable();
        } else {
            return true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        // Implement if needed.
    }
}
