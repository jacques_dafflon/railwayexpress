package ch.usi.inf.saiv.railwayempire.gui.elements;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;

/**
 * A label shaped as a rectangle.
 */
public class Label extends AbstractGuiComponent {
    
    /**
     * The text of the label.
     */
    private String text;
    
    /**
     * Indicate if the label as a tooltip.
     */
    private boolean tooltip;
    
    /**
     * The text of the tool-tip.
     */
    protected String tooltipText;
    
    /**
     * True if the mouse is over the area.
     */
    protected boolean over;
    
    /**
     * Alignment.
     */
    private Alignment alignment;
    
    /**
     * Indicates if the text must be centered vertically.
     */
    private final boolean verticalCenter;
    
    /**
     * The image of the background.
     * 
     * Default is none.
     */
    private Image backgroundImage;
    
    /**
     * The color of the background.
     * 
     * Default is none.
     */
    private Color backgroundColor;
    
    /**
     * The font of the label. (If set, the default font otherwise).
     */
    private Font font;
    
    /**
     * Padding for the text.
     */
    private int padding;
    
    /**
     * A text label.
     * 
     * @param posX
     *            coordinate x.
     * @param posY
     *            coordinate y.
     * @param width
     *            width of the shape.
     * @param height
     *            height of the shape.
     * @param text
     *            the text to be displayed.
     */
    public Label(final int posX, final int posY, final int width, final int height, final String text, final int padding) {
        this(posX, posY, width, height, text, Alignment.LEFT, false);
        this.padding = padding;
    }
    
    /**
     * A text label.
     * 
     * @param posX
     *            coordinate x.
     * @param posY
     *            coordinate y.
     * @param width
     *            width of the shape.
     * @param height
     *            height of the shape.
     * @param text
     *            the text to be displayed.
     */
    public Label(final int posX, final int posY, final int width, final int height, final String text) {
        this(posX, posY, width, height, text, Alignment.LEFT, false);
    }
    
    /**
     * A text label.
     * 
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     * @param width
     *            width of the rectangle.
     * @param height
     *            height of the rectangle.
     * @param text
     *            text to be displayed.
     * @param alignment
     *            alignment.
     */
    public Label(final int posX,
        final int posY,
        final int width,
        final int height,
        final String text,
        final Alignment alignment) {
        this(posX, posY, width, height, text, alignment, false);
    }
    
    /**
     * A text label.
     * 
     * @param posX
     *            x coordinate.
     * @param posY
     *            y coordinate.
     * @param width
     *            width of the rectangle.
     * @param height
     *            height of the rectangle.
     * @param text
     *            text to be displayed.
     * @param alignment
     *            alignment.
     * @param vCenter
     *            if <code>true</code> the text will be vertically centered based on the given height.
     */
    public Label(final int posX,
        final int posY,
        final int width,
        final int height,
        final String text,
        final Alignment alignment,
        final boolean vCenter) {
        
        super(new Rectangle(posX, posY, width, height));
        this.text = text;
        this.over = false;
        this.tooltipText = "";
        this.tooltip = false;
        this.alignment = alignment;
        this.verticalCenter = vCenter;
        this.backgroundColor = null;
        this.backgroundImage = null;
        this.setFont(null);
    }
    
    /**
     * Set the tool-tip text.
     * 
     * @param text
     *            the text to be set.
     */
    public final void setTooltip(final String text) {
        this.tooltipText = text;
        this.tooltip = true;
    }
    
    /**
     * Set the new alignment.
     * 
     * @param newAlignment
     *            new alignment.
     */
    public final void setAlignment(final Alignment newAlignment) {
        this.alignment = newAlignment;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        
        final Font renderFont = this.font == null ? graphics.getFont() : this.font;
        
        if (null != this.backgroundColor) {
            final Color previousColor = graphics.getColor();
            graphics.setColor(this.backgroundColor);
            graphics.fill(this.getShape());
            graphics.setColor(previousColor);
        }
        
        if (null != this.backgroundImage) {
            graphics.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight(), this.backgroundImage, 0, 0);
        }
        
        final int posY = this.getY() +
            (this.verticalCenter ? (this.getHeight() - renderFont.getLineHeight()) / 2 : 0);
        
        GraphicsUtils.drawShadowText(graphics, this.text, this.getX() + this.padding, posY + this.padding,
            this.getWidth(),
            this.alignment, renderFont);
        
        if (this.over && this.tooltip) {
            graphics.setColor(new Color(0, 0, 0, 100));
            graphics.fillRect(this.getShape().getX(), this.getShape().getY() - 20, this.getShape().getWidth(), this
                .getShape().getHeight());
            graphics.setColor(Color.white);
            graphics.drawString(this.tooltipText, this.getShape().getX() + this.padding, this.getShape().getY() - 20
                + this.padding);
        }
        
    }
    
    /**
     * Set the color of the background.
     * 
     * @param color
     *            The new color for the background.
     */
    public void setBackground(final Color color) {
        this.backgroundColor = color;
    }
    
    /**
     * Set the image for the background.
     * 
     * @param image
     *            The new image for the background.
     */
    public void setBackground(final Image image) {
        this.backgroundImage = image;
    }
    
    /**
     * Set the font used to render the label.
     * 
     * @param font
     *            The new font used to render the label
     */
    public void setFont(final Font font) {
        this.font = font;
    }
    
    /**
     * Get the font used to render the label.
     * 
     * @return The font used to render the label or <code>null</code> if it was never set.
     */
    public Font getFont() {
        return this.font;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mousePressed(final int button, final int coordX, final int coordY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseReleased(final int button, final int coordX, final int coordY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        this.over = this.contains(newX, newY);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final void keyPressed(final int key, final char character) {
        // Implement if needed.
    }
    
    /**
     * Change the text of the label.
     * 
     * @param text
     *            new text.
     */
    public void setText(final String text) {
        this.text = text;
    }
}
