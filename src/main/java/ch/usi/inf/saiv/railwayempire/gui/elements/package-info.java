/**
 * Provides the application with custom Graphical User Interface hierarchy.
 */
package ch.usi.inf.saiv.railwayempire.gui.elements;