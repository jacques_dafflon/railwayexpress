package ch.usi.inf.saiv.railwayempire.gui.elements.center.components;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.controllers.TrainSelectionController;
import ch.usi.inf.saiv.railwayempire.gui.elements.AbstractPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.LayoutPanel;
import ch.usi.inf.saiv.railwayempire.model.trains.Locomotive;
import ch.usi.inf.saiv.railwayempire.model.trains.Train;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;
import ch.usi.inf.saiv.railwayempire.model.stores.TrainComponentStore;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils;
import ch.usi.inf.saiv.railwayempire.utilities.FontUtils.Alignment;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Train Components Panel.
 */
public final class ComponentEditorPanel extends AbstractPanel {
    
    /**
     * Height of a train component entry in the list.
     */
    private static final int ELEM_HEIGHT = 120;
    
    /**
     * Height of the panel displaying the current train.
     */
    private static int TRAIN_DISPLAY_HEIGHT = 150;
    
    /**
     * The panel displaying the representation of the current train
     */
    private final CompositionEditorPanel topPanel;
    
    /**
     * The list of all locomotives types.
     */
    private final LayoutPanel locomotivesPanel;
    
    /**
     * The list of all wagons types.
     */
    private final LayoutPanel wagonsPanel;
    
    /**
     * The store from which to get all the train components to display.
     */
    private final TrainComponentStore store;
    
    /**
     * Instantiate in the given rectangle and controlled by the given controller.
     * 
     * @param rectangle
     *            Shape of the panel
     * @param trainSelectionController
     *            The controller in charge of this panel
     */
    public ComponentEditorPanel(final Rectangle rectangle,
        final TrainSelectionController trainSelectionController,
        final GameContainer container) {
        super(rectangle);
        this.topPanel = new CompositionEditorPanel(new Rectangle(this.getX(), this.getY(),
            this.getWidth(), ComponentEditorPanel.TRAIN_DISPLAY_HEIGHT), trainSelectionController, container);
        this.topPanel.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND"));
        this.add(this.topPanel);
        
        this.store = TrainComponentStore.getUniqueInstance();
        
        final Label locomotivesLabel = new Label(this.getX(), this.topPanel.getY() + this.topPanel.getHeight(),
            this.getWidth() / 2, 30, "Locomotives", Alignment.CENTER, true);
        locomotivesLabel.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND_DARK"));
        locomotivesLabel.setFont(FontUtils.LARGE_FONT);
        this.add(locomotivesLabel);
        
        this.locomotivesPanel = new LayoutPanel(new Rectangle(this.getX(),
            locomotivesLabel.getY() + locomotivesLabel.getHeight(),
            this.getWidth() * 0.5f, this.getHeight() - this.topPanel.getHeight() - locomotivesLabel.getHeight()),
            LayoutPanel.VERTICAL);
        this.add(this.locomotivesPanel);
        
        final Label wagonsLabel = new Label(locomotivesLabel.getX() + locomotivesLabel.getWidth(),
            this.topPanel.getY() + this.topPanel.getHeight(),
            this.getWidth() / 2, 30, "Wagons", Alignment.CENTER, true);
        wagonsLabel.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND_DARK"));
        wagonsLabel.setFont(FontUtils.LARGE_FONT);
        this.add(wagonsLabel);
        
        this.wagonsPanel = new LayoutPanel(new Rectangle(this.getX() + this.getWidth() * 0.5f,
            wagonsLabel.getY() + wagonsLabel.getHeight(),
            this.getWidth() * 0.5f, this.getHeight() - this.topPanel.getHeight() - wagonsLabel.getHeight()),
            LayoutPanel.VERTICAL);
        this.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND_DARK"));
        this.add(this.wagonsPanel);
        
        this.initLocomotivesPanel();
        this.initWagonsPanel();
        
        trainSelectionController.initTrainEditor(this);
    }
    
    /**
     * Set the current train to work with.
     * 
     * @param train
     *            The train to work with.
     */
    public void setTrain(final Train train) {
        this.topPanel.setTrain(train);
        
    }
    
    /**
     * Initialize the list of locomotives.
     */
    private void initLocomotivesPanel() {
        for (final Locomotive loc : this.store.getPurchasableLocomotives()) {
            final AbstractComponentPanel panel = new LocomotiveCompositionPanel(new Rectangle(0, 0,
                this.locomotivesPanel.getWidth(), ComponentEditorPanel.ELEM_HEIGHT), loc, this.topPanel);
            panel.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND"));
            this.locomotivesPanel.add(panel);
        }
        
    }
    
    /**
     * Initialize the list of wagons.
     */
    private void initWagonsPanel() {
        for (final IWagon wagon : this.store.getPurchasableWagons()) {
            final AbstractComponentPanel panel = new WagonCompositionPanel(new Rectangle(0, 0,
                this.wagonsPanel.getWidth(), ComponentEditorPanel.ELEM_HEIGHT), wagon, this.topPanel);
            panel.setBackground(ResourcesLoader.getInstance().getImage("BACKGROUND"));
            this.wagonsPanel.add(panel);
        }
    }
    
}
