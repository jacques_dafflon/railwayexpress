package ch.usi.inf.saiv.railwayempire.gui.elements.bottom;

import java.text.NumberFormat;
import java.util.Locale;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.Game;
import ch.usi.inf.saiv.railwayempire.model.Player;
import ch.usi.inf.saiv.railwayempire.model.generators.NamesGenerator;
import ch.usi.inf.saiv.railwayempire.controllers.GameModesController;
import ch.usi.inf.saiv.railwayempire.gui.elements.Button;
import ch.usi.inf.saiv.railwayempire.gui.elements.Label;
import ch.usi.inf.saiv.railwayempire.gui.elements.MouseListener;
import ch.usi.inf.saiv.railwayempire.gui.viewers.CoordinatesManager;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.utilities.GraphicsUtils;
import ch.usi.inf.saiv.railwayempire.utilities.Log;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Bottom Panel for utility display according to selected production site if selected at all.
 */
public final class ProductionSiteUtilityPanel extends AbstractWorldInfoUtilityPanel {

    /**
     * Label width.
     */
    private final static int LABEL_WIDTH = 180;
    /**
     * Label height.
     */
    private final static int LABEL_HEIGHT = 20;
    /**
     * The margin for the logo.
     */
    private final static int IMAGE_MARGIN = 8;
    /**
     * Margin.
     */
    private final static int MARGIN = 5;
    /**
     * Selected production site.
     */
    private final IProductionSite productionSite;
    /**
     * Button for buying production site.
     */
    final Button buyButton;
    /**
     * Button for selling production site.
     */
    final Button sellButton;
    /**
     * The image and button of the production site.
     */
    private final Button productionSiteLogo;

    /**
     * Creates the panel for displaying selected production site information.
     * Also allows the player to buy the site if not owned or has enough money.
     *
     * @param rectangle
     *            utility rectangle.
     * @param productionSite
     *            selected production site.
     * @param modesController
     *            modes controller.
     */
    public ProductionSiteUtilityPanel(final Rectangle rectangle, final IProductionSite productionSite,
        final GameModesController modesController) {
        super(rectangle);

        this.productionSite = productionSite;
        final ResourcesLoader loader = ResourcesLoader.getInstance();

        this.productionSiteLogo =
            new Button(loader.getImage(productionSite.getTexture() + "_BUTTON"),
                loader.getImage(productionSite.getTexture() + "_BUTTON_HOVER"),
                loader.getImage(productionSite.getTexture() + "_BUTTON_PRESSED"),
                this.getX(),
                this.getY());
        this.productionSiteLogo.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                CoordinatesManager.getInstance().centerCameraOn(productionSite.getX(), productionSite.getY());
            }
        });

        this.add(this.productionSiteLogo);

        this.add(new Label(this.getX() + ProductionSiteUtilityPanel.MARGIN
            + this.productionSiteLogo.getWidth()
            + 2
            * ProductionSiteUtilityPanel.IMAGE_MARGIN,
            this.getY(),
            ProductionSiteUtilityPanel.LABEL_WIDTH,
            ProductionSiteUtilityPanel.LABEL_HEIGHT,
            productionSite.getHoverText()));

        this.add(new Label(this.getX() + ProductionSiteUtilityPanel.MARGIN + this.productionSiteLogo.getWidth() + 2
            * ProductionSiteUtilityPanel.IMAGE_MARGIN,
            this.getY() + ProductionSiteUtilityPanel.MARGIN + ProductionSiteUtilityPanel.LABEL_HEIGHT,
            ProductionSiteUtilityPanel.LABEL_WIDTH,
            ProductionSiteUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String owner = "Owned by " + productionSite.getOwnersName();
                GraphicsUtils.drawShadowText(graphics, owner, this.getX(), this.getY());
            }
        });

        this.add(new Label(this.getX() + ProductionSiteUtilityPanel.MARGIN + this.productionSiteLogo.getWidth() + 2
            * ProductionSiteUtilityPanel.IMAGE_MARGIN,
            this.getY() + ProductionSiteUtilityPanel.MARGIN + ProductionSiteUtilityPanel.LABEL_HEIGHT * 2,
            ProductionSiteUtilityPanel.LABEL_WIDTH,
            ProductionSiteUtilityPanel.LABEL_HEIGHT,
            "Price: " + GraphicsUtils.formatCost(productionSite.getCost())));

        this.add(new Label(this.getX() + ProductionSiteUtilityPanel.MARGIN + this.productionSiteLogo.getWidth() + 2
                * ProductionSiteUtilityPanel.IMAGE_MARGIN,
                this.getY() + ProductionSiteUtilityPanel.MARGIN + ProductionSiteUtilityPanel.LABEL_HEIGHT * 3,
                ProductionSiteUtilityPanel.LABEL_WIDTH,
                ProductionSiteUtilityPanel.LABEL_HEIGHT,
                "Produces " + productionSite.getWare().getTypeName()));

        this.add(new Label(this.getX() + ProductionSiteUtilityPanel.MARGIN + this.productionSiteLogo.getWidth() + 2
            * ProductionSiteUtilityPanel.IMAGE_MARGIN,
            this.getY() + ProductionSiteUtilityPanel.MARGIN + ProductionSiteUtilityPanel.LABEL_HEIGHT * 4,
            ProductionSiteUtilityPanel.LABEL_WIDTH,
            ProductionSiteUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String amount = "Max amount: " + productionSite.getAmountCapacity();
                GraphicsUtils.drawShadowText(graphics, amount, this.getX(), this.getY());
            }
        });

        this.add(new Label(this.getX() + ProductionSiteUtilityPanel.MARGIN + this.productionSiteLogo.getWidth() + 2
            * ProductionSiteUtilityPanel.IMAGE_MARGIN,
            this.getY() + ProductionSiteUtilityPanel.MARGIN + ProductionSiteUtilityPanel.LABEL_HEIGHT * 5,
            ProductionSiteUtilityPanel.LABEL_WIDTH,
            ProductionSiteUtilityPanel.LABEL_HEIGHT,
            null) {
            @Override
            public void render(final GUIContext container, final Graphics graphics) {
                final String capital = "Current amount: " + productionSite.getAmount();
                GraphicsUtils.drawShadowText(graphics, capital, this.getX(), this.getY());
            }
        });

        this.buyButton =
            new Button(loader.getImage("BUY_BUTTON"),
                loader.getImage("BUY_BUTTON_HOVER"),
                loader.getImage("BUY_BUTTON_PRESSED"),
                loader.getImage("BUY_BUTTON_DISABLED"),
                this.getX() + this.productionSiteLogo.getWidth() + 3 * ProductionSiteUtilityPanel.IMAGE_MARGIN,
                this.getY() + this.getHeight() - loader.getImage("BUY_BUTTON").getHeight()
                - ProductionSiteUtilityPanel.IMAGE_MARGIN);
        this.buyButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                if (!ProductionSiteUtilityPanel.this.productionSite.isOwnedByPlayer()) {
                    ProductionSiteUtilityPanel.this.buyAction();
                    ProductionSiteUtilityPanel.this.toggleButtons();
                }
            }
        });
        this.add(this.buyButton);

        this.sellButton =
            new Button(ResourcesLoader.getInstance().getImage("SELL_BUTTON"),
                loader.getImage("SELL_BUTTON_HOVER"),
                loader.getImage("SELL_BUTTON_PRESSED"),
                loader.getImage("SELL_BUTTON_DISABLED"),
                this.getX() + this.productionSiteLogo.getWidth() + this.buyButton.getWidth()
                + 4 * ProductionSiteUtilityPanel.IMAGE_MARGIN,
                this.getY() + this.getHeight() - loader.getImage("SELL_BUTTON").getHeight()
                - ProductionSiteUtilityPanel.IMAGE_MARGIN);
        this.sellButton.setListener(new MouseListener() {
            @Override
            public void actionPerformed(final Button button) {
                if (ProductionSiteUtilityPanel.this.productionSite.isOwnedByPlayer()) {
                    ProductionSiteUtilityPanel.this.sellAction();
                    ProductionSiteUtilityPanel.this.toggleButtons();
                }
            }
        });
        this.add(this.sellButton);
        this.toggleButtons();
    }

    /**
     * Toggles the state of sell and buy buttons according to production site owner.
     */
    private void toggleButtons() {
        if (this.productionSite.isOwnedByPlayer()) {
            this.buyButton.disableButton();
            this.sellButton.enableButton();
        } else {
            this.buyButton.enableButton();
            this.sellButton.disableButton();
        }
    }

    /**
     * Sells the currently selected production site for 20% less of the initial cost.
     */
    private void sellAction() {
        final Player player = Game.getInstance().getPlayer();
        final int price = this.productionSite.getCost();
        final NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
        final String someone = NamesGenerator.getInstance().generatePersonFullName();
        player.addMoney((long) (price - price * 0.2));
        this.productionSite.setOwnersName(someone);
        Log.screen(this.productionSite.getHoverText() + " has been sold to " + someone + " for "
            + formatter.format(price));
    }

    /**
     * Buys the currently selected production site.
     */
    private void buyAction() {
        final Player player = Game.getInstance().getPlayer();
        final int price = this.productionSite.getCost();
        final NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
        if (player.canAfford(price)) {
            player.subtractMoney(price);
            this.productionSite.setOwnersName(player.getPlayerName());
            Log.screen(this.productionSite.getHoverText() + " has been bought for " + formatter.format(price));
            this.buyButton.disableButton();
        } else {
            Log.screen("Can't afford to buy the site for " + formatter.format(price));
        }
    }
}
