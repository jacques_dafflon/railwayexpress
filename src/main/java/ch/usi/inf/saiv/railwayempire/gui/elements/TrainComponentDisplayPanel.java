package ch.usi.inf.saiv.railwayempire.gui.elements;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.gui.GUIContext;

import ch.usi.inf.saiv.railwayempire.model.trains.TrainComponent;
import ch.usi.inf.saiv.railwayempire.utilities.ResourcesLoader;

/**
 * Panel displaying details about a train component (locomotive or wagon).
 * 
 */
public class TrainComponentDisplayPanel extends AbstractGuiComponent {
    
    private final TrainComponent component;
    private final Image componentImage;
    
    /**
     * Create a new TrainComponentDisplayPanel.
     * 
     * @param shape
     *            The shape of the panel
     * @param component
     *            The component to display.
     */
    protected TrainComponentDisplayPanel(final Shape shape, final TrainComponent component) {
        super(shape);
        this.component = component;
        this.componentImage = ResourcesLoader.getInstance().getImage(component.getTextureName() + "_SIDE");
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final GUIContext container, final Graphics graphics) {
        this.componentImage.draw(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final int button, final int coordX, final int coordY, final int clickCount) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final int button, final int coordX, final int coordY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final int button, final int coordX, final int coordY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseDragged(final int oldX, final int oldY, final int newX, final int newY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseMoved(final int oldX, final int oldY, final int newX, final int newY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final int charge, final int coordX, final int coordY) {
        // Implement if needed.
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final int key, final char character) {
        // Implement if needed.
    }
}
