package ch.usi.inf.saiv.railwayempire.controllers;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.BottomPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.TrainBuyComponentsInfoPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.TrainComponentsInfoPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.TrainRouteInfoPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.WorldInfoPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.CenterPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.TrainBuyComponentPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.WorldPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.components.ComponentEditorPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.route.TrainRoutePanel;

/**
 * Controller used to handle the transitions between center panels and message passing between those and the bottom
 * panel.
 */
public final class CenterPanelsController {
    /**
     * The game modes controller.
     */
    private final GameModesController modesController;
    /**
     * Train selection controller.
     */
    private final TrainSelectionController trainSelectionController;
    /**
     * World panel.
     */
    private WorldPanel worldPanel;
    /**
     * Train editor panel.
     */
    private ComponentEditorPanel trainEditorPanel;
    /**
     * Train buy components panel.
     */
    private TrainBuyComponentPanel trainBuyComponentPanel;
    /**
     * Train route panel.
     */
    private TrainRoutePanel trainRoutePanel;
    /**
     * Center panel (the container).
     */
    private CenterPanel centerPanel;
    
    /**
     * Rectangle that is the shape of the bottom info panel.
     */
    private Rectangle bottomRectangle;
    /**
     * The Bottom panel.
     */
    private BottomPanel bottomPanel;
    /**
     * The slick2d game container.
     */
    private final GameContainer gameContainer;
    
    
    /**
     * Constructor.
     * 
     * @param newModesController
     *            the modes controller.
     * @param newTrainSelectionController
     *            the train selection controller.
     * @param container
     *            the slick2d game container.
     */
    public CenterPanelsController(final GameModesController newModesController,
        final TrainSelectionController newTrainSelectionController, final GameContainer container) {
        this.modesController = newModesController;
        this.trainSelectionController = newTrainSelectionController;
        this.gameContainer = container;
    }
    
    /**
     * Initialize. To be called from the center panel exactly once.
     * 
     * @param rectangle
     *            Shape of the center panel.
     * @param panel
     *            center panel.
     */
    public void initCenter(final Rectangle rectangle, final CenterPanel panel) {
        this.worldPanel = new WorldPanel(rectangle, this.modesController);
        this.trainEditorPanel = new ComponentEditorPanel(rectangle, this.trainSelectionController, this.gameContainer);
        this.trainBuyComponentPanel = new TrainBuyComponentPanel(rectangle);
        this.trainRoutePanel = new TrainRoutePanel(rectangle, this.trainSelectionController);
        
        this.centerPanel = panel;
        this.centerPanel.setCurrentPanel(this.worldPanel);
    }
    
    /**
     * Initialize. To be called from the bottom panel exactly once.
     * 
     * @param rectangle
     *            shape of the info panels.
     * @param panel
     *            bottom panel.
     */
    public void initBottom(final Rectangle rectangle, final BottomPanel panel) {
        this.bottomRectangle = rectangle;
        this.bottomPanel = panel;
        this.bottomPanel.setInfoPanel(new WorldInfoPanel(rectangle, this.modesController));
    }
    
    /**
     * Enter world panel.
     */
    public void enterWorldPanel() {
        this.centerPanel.setCurrentPanel(this.worldPanel);
        this.bottomPanel.setInfoPanel(new WorldInfoPanel(this.bottomRectangle, this.modesController));
    }
    
    /**
     * Enter train editor panel.
     */
    public void enterTrainEditorPanel() {
        this.centerPanel.setCurrentPanel(this.trainEditorPanel);
        this.bottomPanel.setInfoPanel(new TrainComponentsInfoPanel(this.bottomRectangle));
    }
    
    /**
     * Enter train buy components panel.
     */
    public void enterTrainBuyComponentsPanel() {
        this.centerPanel.setCurrentPanel(this.trainBuyComponentPanel);
        this.bottomPanel.setInfoPanel(new TrainBuyComponentsInfoPanel(this.bottomRectangle));
    }
    
    /**
     * Enter train route panel.
     */
    public void enterTrainRoutePanel() {
        this.trainRoutePanel.redraw();
        this.centerPanel.setCurrentPanel(this.trainRoutePanel);
        this.bottomPanel.setInfoPanel(new TrainRouteInfoPanel(this.bottomRectangle));
    }
}
