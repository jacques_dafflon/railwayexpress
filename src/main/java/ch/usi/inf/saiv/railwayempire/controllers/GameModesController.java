package ch.usi.inf.saiv.railwayempire.controllers;

import org.newdawn.slick.geom.Rectangle;

import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.BuildingCreationUtilityPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.LocomotiveUtilityPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.EmptyUtilityPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.ProductionSiteUtilityPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.StationCreationUtilityPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.StationUtilityPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.TrackCreationUtilityPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.WagonUtilityPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.bottom.WorldInfoPanel;
import ch.usi.inf.saiv.railwayempire.gui.elements.center.WorldPanel;
import ch.usi.inf.saiv.railwayempire.gui.modes.BuildingCreationMode;
import ch.usi.inf.saiv.railwayempire.gui.modes.NormalMode;
import ch.usi.inf.saiv.railwayempire.gui.modes.StationCreationMode;
import ch.usi.inf.saiv.railwayempire.gui.modes.TrackCreationMode;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.structures.Station;
import ch.usi.inf.saiv.railwayempire.model.structures.StationSizes;
import ch.usi.inf.saiv.railwayempire.model.structures.StructureTypes;
import ch.usi.inf.saiv.railwayempire.model.trains.Locomotive;
import ch.usi.inf.saiv.railwayempire.model.wagons.IWagon;

/**
 * Controller used to switch modes easily.
 * This class is responsible for updating both the mode in the world panel and the utility panel in the bottom panel.
 */
public final class GameModesController {
    /**
     * World Panel.
     */
    private WorldPanel worldPanel;
    /**
     * World info panel.
     */
    private WorldInfoPanel worldInfoPanel;
    /**
     * Rectangle that is the size of the Utility panel.
     */
    private Rectangle utilityRectangle;
    /**
     * Rectangle that is the size of the center panel.
     */
    private Rectangle centerRectangle;
    /**
     * Used to remember the current mode to restore it when we stop viewing info about something.
     */
    private GameModes currentMode;
    
    /**
     * Enter a new mode.
     *
     * @param mode
     *            mode to enter.
     */
    public void enterMode(final GameModes mode) {
        this.worldPanel.setMountainTransparent(false);
        switch (mode) {
            case NORMAL:
                this.worldPanel.setCurrentMode(new NormalMode(this.centerRectangle, this));
                break;
            case BUILDING:
                this.worldPanel.setCurrentMode(new NormalMode(this.centerRectangle, this));
                this.worldInfoPanel.setUtilityPanel(new BuildingCreationUtilityPanel(this.utilityRectangle, this));
                this.currentMode = mode;
                break;
            case STATIONS_ALL:
                this.worldPanel.setCurrentMode(new NormalMode(this.centerRectangle, this));
                this.worldInfoPanel.setUtilityPanel(new StationCreationUtilityPanel(this.utilityRectangle, this));
                this.currentMode = mode;
                break;
            case STATION_SMALL:
                this.worldPanel.setCurrentMode(new StationCreationMode(StationSizes.SMALL, this));
                break;
            case STATION_MEDIUM:
                this.worldPanel.setCurrentMode(new StationCreationMode(StationSizes.MEDIUM, this));
                break;
            case STATION_LARGE:
                this.worldPanel.setCurrentMode(new StationCreationMode(StationSizes.LARGE, this));
                break;
            case TRACKS:
                final TrackCreationMode tcm = new TrackCreationMode(this.centerRectangle);
                this.worldPanel.setCurrentMode(tcm);
                this.worldInfoPanel.setUtilityPanel(new TrackCreationUtilityPanel(this.utilityRectangle, tcm));
                this.worldPanel.setMountainTransparent(true);
                this.currentMode = mode;
                break;
            default:
                throw new RuntimeException("Code unreachable");
        }
    }
    
    /**
     * Initialize. To be called from the world panel exactly once.
     *
     * @param rectangle
     *            shape of the center panel.
     * @param newWorldPanel
     *            world panel.
     */
    public void initWorldPanel(final Rectangle rectangle, final WorldPanel newWorldPanel) {
        this.centerRectangle = rectangle;
        this.worldPanel = newWorldPanel;
        this.worldPanel.setCurrentMode(new NormalMode(rectangle, this));
        this.currentMode = GameModes.NORMAL;
    }
    
    /**
     * Initialize. To be called from the world info panel exactly once.
     * 
     * @param newUtilityRectangle
     *            the shape of the utility panel.
     * @param newWorldInfoPanel
     *            world info panel.
     */
    public void initInfoPanel(final Rectangle newUtilityRectangle, final WorldInfoPanel newWorldInfoPanel) {
        this.worldInfoPanel = newWorldInfoPanel;
        this.utilityRectangle = newUtilityRectangle;
        this.worldInfoPanel.setUtilityPanel(new EmptyUtilityPanel(this.utilityRectangle, this));
    }
    
    /**
     * Removes utility panel if no station, production site, locomotive or wagon is currently selected.
     * Sets back last displayed utility panel before displaying selected item information.
     */
    public void removeInfo() {
        this.enterMode(this.currentMode);
        this.worldPanel.highlightSelected(null);
        this.worldPanel.enableStationRadius(null);
    }
    
    /**
     * Creates the panel for displaying selected station information.
     *
     * @param station
     *            selected station.
     */
    public void displayInfo(final Station station) {
        this.worldInfoPanel.setUtilityPanel(new StationUtilityPanel(this.utilityRectangle, station, this));
        this.worldPanel.enableStationRadius(station);
    }
    
    /**
     * Creates the panel for displaying selected production site information.
     * Also enables the option to buy selected site if not owned already by the player.
     *
     * @param productionSite
     *            selected production site.
     */
    public void displayInfo(final IProductionSite productionSite) {
        this.worldInfoPanel.setUtilityPanel(new ProductionSiteUtilityPanel(this.utilityRectangle, productionSite, this));
        this.worldPanel.highlightSelected(productionSite);
    }
    
    /**
     * Creates the panel for displaying selected locomotive information.
     *
     * @param locomotive
     *            selected locomotive.
     */
    public void displayInfo(final Locomotive locomotive) {
        final LocomotiveUtilityPanel locomotivePanel = new LocomotiveUtilityPanel(this.utilityRectangle,
            locomotive,
            this);
        this.worldInfoPanel.setUtilityPanel(locomotivePanel);
    }
    
    /**
     * Creates the panel for displaying selected locomotive information.
     *
     * @param wagon
     *            selected wagon.
     */
    public void displayInfo(final IWagon wagon) {
        final WagonUtilityPanel wagonPanel = new WagonUtilityPanel(this.utilityRectangle, wagon, this);
        this.worldInfoPanel.setUtilityPanel(wagonPanel);
    }
    
    /**
     * Enter building creation mode and give the provided structure type.
     * 
     * @param structure
     *            structure type to create.
     */
    public void enterBuildingCreationMode(final StructureTypes structure) {
        this.worldPanel.setMountainTransparent(false);
        this.worldPanel.setCurrentMode(new BuildingCreationMode(structure, this));
    }
}
