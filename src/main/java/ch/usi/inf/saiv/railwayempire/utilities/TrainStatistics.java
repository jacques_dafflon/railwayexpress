package ch.usi.inf.saiv.railwayempire.utilities;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ch.usi.inf.saiv.railwayempire.model.City;
import ch.usi.inf.saiv.railwayempire.model.wares.IWare;
import ch.usi.inf.saiv.railwayempire.model.wares.WareTypes;


/**
 * Class representing the statistics for the final ranking.
 */
public class TrainStatistics {

    /**
     * The quantities of goods traded by the train.
     */
    private final Map<WareTypes, Double> tradedGoods;
    /**
     * The set of cities reached by the train.
     */
    private final Set<City> reachedCities;


    /**
     * Standard constructor.
     */
    public TrainStatistics() {
        this.tradedGoods = new HashMap<WareTypes, Double>();
        this.reachedCities = new HashSet<City>();
    }



    /**
     * Returns the cities the train has reached.
     * @return The cities reached by the train.
     */
    public Set<City> getReachedCities() {
        return this.reachedCities;
    }

    /**
     * Adds the performed trade to the statistics.
     *
     * @param ware
     *         The ware traded.
     * @param quantity
     *         The traded quantity.
     */
    public void addTrade(final IWare ware, final double quantity) {
        if (this.tradedGoods.containsKey(ware.getType())) {
            this.tradedGoods.put(ware.getType(), this.tradedGoods.get(ware.getType()) + quantity);
        } else {
            this.tradedGoods.put(ware.getType(), quantity);
        }
    }
}
