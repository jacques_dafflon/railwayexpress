package ch.usi.inf.saiv.railwayempire.model.cells;

/**
 * CellTypes enum, used to get the type of Cells.
 */
public enum CellTypes {
    /**
     * Ground Cell.
     */
    GROUND,
    
    /**
     * Sea Cell.
     */
    SEA,
    
    /**
     * River Cell.
     */
    RIVER,
    
    /**
     * Mountain Cell.
     */
    MOUNTAIN;
}
