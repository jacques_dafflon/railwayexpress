package ch.usi.inf.saiv.railwayempire.model.structures;

import org.junit.Assert;
import org.junit.Test;

import ch.usi.inf.saiv.railwayempire.model.City;
import ch.usi.inf.saiv.railwayempire.model.production_sites.Building;
import ch.usi.inf.saiv.railwayempire.model.production_sites.CoalMine;
import ch.usi.inf.saiv.railwayempire.model.production_sites.OilRig;
import ch.usi.inf.saiv.railwayempire.model.production_sites.Farm;
import ch.usi.inf.saiv.railwayempire.model.production_sites.FoodFactory;
import ch.usi.inf.saiv.railwayempire.model.production_sites.IronMine;
import ch.usi.inf.saiv.railwayempire.model.production_sites.PostOffice;
import ch.usi.inf.saiv.railwayempire.model.production_sites.RockCave;
import ch.usi.inf.saiv.railwayempire.model.production_sites.SawMill;
import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;

public class TestProductionSites {
    
    @Test
    public void testProductionSite() {
        final CoalMine coalMine = new CoalMine(10, 12);
        Assert.assertEquals(10, coalMine.getX());
        Assert.assertEquals(12, coalMine.getY());
        Assert.assertEquals(GameConstants.COAL_MINE_PRODUCTION_RATE, coalMine.getAmountPerMinute(), 0.0);
        Assert.assertEquals(0.0, coalMine.getAmount(), 0.0);
        coalMine.update(0);
        Assert.assertEquals(GameConstants.COAL_MINE_PRODUCTION_RATE, coalMine.getAmount(), 0.0);
        coalMine.update(0);
        Assert.assertEquals(2 * GameConstants.COAL_MINE_PRODUCTION_RATE, coalMine.getAmount(), 0.0);
        coalMine.update(0);
        Assert.assertEquals(3 * GameConstants.COAL_MINE_PRODUCTION_RATE, coalMine.getAmount(), 0.0);
        coalMine.update(0);
        Assert.assertEquals(4 * GameConstants.COAL_MINE_PRODUCTION_RATE, coalMine.getAmount(), 0.0);
    }
    
    @Test
    public void testIronMine() {
        final IronMine ironMine = new IronMine(10, 12);
        Assert.assertEquals(StructureTypes.IRON_MINE, ironMine.getStructureType());
    }
    
    @Test
    public void testDrillRig() {
        final OilRig rig = new OilRig(10, 12);
        Assert.assertEquals(StructureTypes.OIL_RIG, rig.getStructureType());
    }
    
    @Test
    public void testFoodFactory() {
        final FoodFactory foodFactry = new FoodFactory(10, 12);
        Assert.assertEquals(StructureTypes.FOOD_FACTORY, foodFactry.getStructureType());
    }
    
    @Test
    public void testFarm() {
        final Farm farm = new Farm(10, 12);
        Assert.assertEquals(StructureTypes.FARM, farm.getStructureType());
    }
    
    @Test
    public void testCoalMine() {
        final CoalMine coalMine = new CoalMine(10, 12);
        Assert.assertEquals(StructureTypes.COAL_MINE, coalMine.getStructureType());
    }
    
    @Test
    public void testSawMill() {
        final SawMill sawMill = new SawMill(10, 12);
        Assert.assertEquals(StructureTypes.SAW_MILL, sawMill.getStructureType());
    }
    
    @Test
    public void testPostOffice() {
        final PostOffice postOffice = new PostOffice(10, 12);
        Assert.assertEquals(StructureTypes.POST_OFFICE, postOffice.getStructureType());
    }
    
    @Test
    public void testRockCave() {
        final RockCave rockCave = new RockCave(10, 12);
        Assert.assertEquals(StructureTypes.ROCK_CAVE, rockCave.getStructureType());
    }
    
    @Test
    public void testBuilding() {
        final Building building = new Building(10, 12, 2.0, 5.0, new City(10, 12));
        Assert.assertEquals(StructureTypes.BUILDING, building.getStructureType());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNegativeCoords() {
        @SuppressWarnings("unused")
        final PostOffice postOffice = new PostOffice(-5, 2);
    }
}
