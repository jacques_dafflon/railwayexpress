package ch.usi.inf.saiv.railwayempire.model;


import org.junit.Test;

import ch.usi.inf.saiv.railwayempire.model.production_sites.IProductionSite;
import ch.usi.inf.saiv.railwayempire.model.production_sites.RockCave;
import ch.usi.inf.saiv.railwayempire.utilities.WorldSettings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

public class TestWorld {

    @Test
    public void testWorldTerrain() {
        final World testWorld = new World(new WorldSettings());
        final Terrain terrain = testWorld.getTerrain();
        assertNotNull("Terrain shouldn't be null", terrain);
    }

    @Test
    public void testAddAndGetCities() {
        final World testWorld = new World(new WorldSettings());
        final int listSize = testWorld.getCities().size();
        final City c = new City(1, 2);
        testWorld.addCity(c);
        assertTrue("The list of city should contains the right city", testWorld.getCities().contains(c));
        assertNotSame("The list of trains shouldn't be empty", testWorld.getCities().size(), 0);
        assertEquals("The list of cities should contains the right number of cities", testWorld.getCities().size(), listSize + 1);
    }

    @Test
    public void testCitiesCollision() {
        final World testWorld = new World(new WorldSettings());
        final City c = new City(1, 2);
        testWorld.addCity(c);
        final City c2 = new City(4, 5);
        final City c3 = new City(1, 2);
        assertFalse("Cities should't collide", testWorld.isCollidingWithOtherCity(c2));
        assertTrue("Cities should collide", testWorld.isCollidingWithOtherCity(c3));
    }

    @Test
    public void testAddProductionSites() {
        final World testWolrd = new World(new WorldSettings());
        final int sitesBefore = testWolrd.getProductionSites().size();
        final IProductionSite site = new RockCave(0, 0);
        testWolrd.addExtractionSite(site);
        assertNotNull("Site shouldn't be null", testWolrd.getProductionSites());
        assertEquals("Sites size should be one more", sitesBefore + 1, testWolrd.getProductionSites().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullAddSite() {
        final World testWorld = new World(new WorldSettings());
        testWorld.addExtractionSite(null);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testAddExistingCity() {
        final World testWorld = new World(new WorldSettings());
        final City c1 = new City(1, 2);
        final City c2 = new City(1, 2);
        testWorld.addCity(c1);
        testWorld.addCity(c2);
    }

}
