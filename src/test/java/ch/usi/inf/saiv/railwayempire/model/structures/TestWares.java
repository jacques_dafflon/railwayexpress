package ch.usi.inf.saiv.railwayempire.model.structures;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ch.usi.inf.saiv.railwayempire.model.wares.Food;
import ch.usi.inf.saiv.railwayempire.model.wares.Mail;
import ch.usi.inf.saiv.railwayempire.model.wares.Passengers;
import ch.usi.inf.saiv.railwayempire.model.wares.materials.Coal;
import ch.usi.inf.saiv.railwayempire.model.wares.materials.Rock;
import ch.usi.inf.saiv.railwayempire.model.wares.materials.Wood;

public class TestWares {
    
    @Test
    public void testWares() {
        final Food food = new Food(0);
        assertEquals(0, food.getQuantity(), 0.0);
        food.add(3.0);
        assertEquals(3.0, food.getQuantity(), 0.0);
        food.remove(2.0);
        assertEquals(1.0, food.getQuantity(), 0.0);
        food.clear();
        assertEquals(0.0, food.getQuantity(), 0.0);
        food.add(2.0);
        food.remove(2.0);
        assertEquals(0.0, food.getQuantity(), 0.0);
    }
    
    @Test
    public void testAllWares() {
        new Food(1.0);
        new Mail(2.0);
        new Passengers(3.0);
        new Coal(4.0);
        new Rock(5.0);
        new Wood(6.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeAdd() {
        new Mail(3).add(-5);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNegativeRemove() {
        new Coal(3).remove(-5);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testRemoveTooMuch() {
        new Passengers(5.0).remove(7.0);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNegativeConstructor() {
        new Rock(-5);
    }
}
