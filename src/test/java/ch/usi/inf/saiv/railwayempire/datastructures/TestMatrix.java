package ch.usi.inf.saiv.railwayempire.datastructures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.saiv.railwayempire.model.datastructures.Matrix;



/**
 * a.
 */
public class TestMatrix {

    private static final int MAXWIDTH = 1000;
    private static final int MAXHEIGHT = 1000;

    Random random;

    @Before
    public void initializeTests() {
        this.random = new Random(0x666);
    }

    @Test
    public void testMatrixCreation() {
        final int matrixWidth = this.random.nextInt(MAXWIDTH);
        final int matrixHeight = this.random.nextInt(MAXHEIGHT);
        final Matrix<Integer> matrix = new Matrix<Integer>(matrixWidth, matrixHeight);
        assertNotNull("[FAILED] The entry set should not be null upon matrix creation!", matrix.getEntrySet());
        assertEquals("[FAILED] The width of the matrix should have been " + matrixWidth, matrixWidth, matrix.getWidth());
        assertEquals("[FAILED] The height of the matrix should have been " + matrixHeight, matrixHeight, matrix.getHeight());
    }

    @Test
    public void testMatrixGet() {
        final Matrix<Integer> matrix = new Matrix<Integer>(10, 10);
        assertNull("[FAILED] The matrix should not contain objects upon creation", matrix.get(this.random.nextInt(matrix.getWidth()), this.random.nextInt(matrix.getHeight())));
    }

    @Test
    public void testMatrixSet() {
        final Matrix<Integer> matrix = new Matrix<Integer>(10, 10);
        matrix.set(6, 6, 6);
        assertEquals("[FAILED] The value matrix should have returned the new value!", matrix.get(6, 6),
                new Integer(6));

    }

    @Test
    public void testMatrixInRange() {
        final int matrixWidth = this.random.nextInt(MAXWIDTH);
        final int matrixHeight = this.random.nextInt(MAXHEIGHT);
        final Matrix<Integer> matrix = new Matrix<Integer>(matrixWidth, matrixHeight);
        final int widthUpperBound = matrixWidth - 1;
        final int heightUpperBound = matrixHeight - 1;
        assertTrue("[FAILED] The point should be in the matrix!", matrix.isInRange(widthUpperBound, heightUpperBound));
        assertTrue("[FAILED] The point should be in the matrix!", matrix.isInRange(0, 0));
        assertFalse("[FAILED] The point should not be in the matrix", matrix.isInRange(matrixWidth, matrixHeight));
        assertFalse("[FAILED] The point should not be in the matrix", matrix.isInRange(-1, -1));
        assertTrue("[FAILED] The point should be in the matrix", matrix.isInRange(matrixWidth / 2, matrixHeight / 2));
        assertFalse("[FAILED] The point should not be in the matrix", matrix.isInRange(-1, matrixHeight/2));
        assertFalse("[FAILED] The point should not be in the matrix", matrix.isInRange(matrixWidth, matrixHeight/2));
        assertFalse("[FAILED] The point should not be in the matrix", matrix.isInRange(matrixWidth/2, -1));
        assertFalse("[FAILED] The point should not be in the matrix", matrix.isInRange(matrixHeight/2, -1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidGet() {
        final Matrix<Integer> matrix = new Matrix<Integer>(10, 10);
        matrix.get(-1, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidSet() {
        final Matrix<Integer> matrix = new Matrix<Integer>(10, 10);
        matrix.set(-1, -1, 6);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroRowsMatrix () {
        new Matrix<Integer>(0, 10);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testZeroColsMatrix () {
        new Matrix<Integer>(10, 0);
    }
}
