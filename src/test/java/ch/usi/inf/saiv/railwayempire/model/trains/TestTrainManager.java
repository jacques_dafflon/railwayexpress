package ch.usi.inf.saiv.railwayempire.model.trains;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 *
 */
public class TestTrainManager {

    @Test
    public void testTrainManagerCreation() {
        final TrainManager trainManager = new TrainManager();
        assertEquals("A new train manager should not contain trains!", 0, trainManager.getTrains().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullAddTrain() {
        final TrainManager trainManager = new TrainManager();
        trainManager.addTrain(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNUllAddTrains() {
        final TrainManager trainManager = new TrainManager();
        trainManager.addTrains(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddNullTrains() {
        final ArrayList<Train> trains = new ArrayList<Train>();
        while (trains.size() < 10) {
            trains.add(null);
        }

        final TrainManager trainManager = new TrainManager();
        trainManager.addTrains(trains);
    }

    @Test
    public void testAddAndGetTrains() {
        // TODO: Fix these tests
        /*
         * final Train t = new Train(new Locomotive(), new Route());
         * testWorld.addTrain(t);
         * assertEquals("The list of trains should contains the right train",
         * testWorld.getTrains().get(0), t);
         * assertNotSame("The list of trains shouldn't be empty",
         * testWorld.getTrains().size(), 0); assertEquals(
         * "The list of trains should contains the right number of trains",
         * testWorld.getTrains().size(), 1); final ArrayList<Train> trains = new
         * ArrayList<Train>(); trains.add(new Train(new Locomotive(), new
         * Route())); testWorld.addTrains(trains);
         * assertEquals("The list of trains should contains the right train",
         * testWorld.getTrains().get(1), trains.get(0)); assertEquals(
         * "The list of trains should contains the right number of trains",
         * testWorld.getTrains().size(), 2);
         */
    }
}
