package ch.usi.inf.saiv.railwayempire;

import ch.usi.inf.saiv.railwayempire.model.Player;
import org.junit.Assert;
import org.junit.Test;

import ch.usi.inf.saiv.railwayempire.utilities.GameConstants;

/**
 *
 */
public class PlayerTest {
    
    @Test
    public void testGetPlayerName() {
        final Player player = new Player();
        Assert.assertEquals("[FAILED] The player should have the default name!", GameConstants.DEFAULT_PLAYER_NAME,
                player.getPlayerName());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSetPlayerNullName() throws Exception {
        final Player player = new Player();
        player.setPlayerName(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSetPlayerEmptyName() {
        final Player player = new Player();
        player.setPlayerName("  ");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testAddNegativeMoney() {
        final Player player = new Player();
        player.addMoney(-5);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSubtractNegativeMoney() {
        final Player player = new Player();
        player.subtractMoney(-5);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testSubtractLotOfMoney() {
        final Player player = new Player();
        player.subtractMoney(2 * GameConstants.INITIAL_MONEY);
    }
    
    @Test
    public void testSetPlayerName() {
        final String name = "This is the player's name";
        final Player player = new Player();
        player.setPlayerName(name);
        Assert.assertEquals("[FAILED] The player should have changed name!", name, player.getPlayerName());
    }
    
    @Test
    public void testGetAvailableMoney() {
        final Player player = new Player();
        Assert.assertEquals("[FAILED]  The player should start with the default amount of money!", GameConstants.INITIAL_MONEY,
                player.getAvailableMoney());
    }
    
    @Test
    public void testAddMoney() {
        final Player player = new Player();
        final int toAdd = 50;
        final long playerMoney = player.getAvailableMoney();
        player.addMoney(toAdd);
        Assert.assertEquals("The player money should have increased!", playerMoney + toAdd, player.getAvailableMoney());
    }
    
    @Test
    public void testSubtractMoney() {
        final Player player = new Player();
        final int toRemove = 50;
        final long playerMoney = player.getAvailableMoney();
        player.subtractMoney(toRemove);
        Assert.assertEquals("The player money should have decreased!", playerMoney - toRemove, player.getAvailableMoney());
    }
    
    @Test
    public void testCanAfford() {
        final Player player = new Player();
        final long available = player.getAvailableMoney();
        Assert.assertTrue(player.canAfford(available / 2));
        Assert.assertTrue(player.canAfford(available - 1));
        Assert.assertTrue(player.canAfford(available));
        Assert.assertFalse(player.canAfford(available + 1));
    }
}
