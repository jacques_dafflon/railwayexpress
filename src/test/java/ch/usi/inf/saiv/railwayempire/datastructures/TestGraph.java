package ch.usi.inf.saiv.railwayempire.datastructures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import ch.usi.inf.saiv.railwayempire.model.datastructures.Graph;

public class TestGraph {
    
    @Test
    public void testGraph() {
        final Integer node1 = new Integer(1);
        final Integer node2 = new Integer(2);
        final Graph<Integer> graph = new Graph<Integer>();
        assertNotNull("[FAILED] Created Graph should be null!", graph);
        assertEquals("Bad dot representation of empty graph", "digraph javaGraph {\n}\n", graph.toDotString());
        graph.addNode(node1);
        assertEquals("[FAILED] Unlinked node should have no neighbors", 0, graph.getNeighbors(node1).size());
        graph.addNode(node2);
        assertEquals("Cost should be -1!", -1, graph.getCost(node1, node2));
        graph.setLink(node1, node2, 10);
        assertEquals("[FAILED] Wrong previous node", node2, graph.getNexHop(node1, node2));
        assertEquals("[FAILED] Wrong previous node", node1, graph.getNexHop(node2, node1));
        assertEquals("[FAILED] Should have 1 neighbor!", 1, graph.getNeighbors(node1).size());
        assertEquals("[FAILED] Should have 1 neighbor!", 1, graph.getNeighbors(node2).size());
        assertEquals("Wrong cost!", 10, graph.getCost(node1, node2));
        assertEquals("Wrong cost!", 10, graph.getCost(node2, node1));
        
        final Integer node3 = new Integer(3);
        graph.addNode(node3);
        graph.setLink(node2, node3, 6);
        assertEquals("[FAILED] Wrong previous node", node2, graph.getNexHop(node3, node1));
        assertEquals("[FAILED] Wrong previous node", node2, graph.getNexHop(node1, node3));
        assertEquals("[FAILED] Should have 2 neighbor!", 2, graph.getNeighbors(node2).size());
        
        final Integer node4 = new Integer(4);
        graph.addNode(node4);
        graph.addNode(node4); //add node twice, it should not break.
        graph.setLink(node4, node3, 4);
        assertEquals("[FAILED] Wrong previous node", node3, graph.getNexHop(node4, node1));
        assertEquals("[FAILED] Wrong previous node", node2, graph.getNexHop(node1, node4));
        assertEquals("[FAILED] Wrong previous node", node3, graph.getNexHop(node2, node4));
        assertEquals("[FAILED] Wrong previous node", node2, graph.getNexHop(node3, node1));
        
        graph.setLink(node3, node1, 3);
        assertEquals("[FAILED] Wrong previous node", node3, graph.getNexHop(node1, node4));
        assertEquals("[FAILED] Wrong previous node", node3, graph.getNexHop(node4, node1));
        assertEquals("[FAILED] Wrong previous node", node3, graph.getNexHop(node1, node2));
        assertEquals("[FAILED] Wrong previous node", node3, graph.getNexHop(node2, node1));
        
        graph.removeLink(node2, node3);
        assertEquals("[FAILED] Wrong previous node", node1, graph.getNexHop(node3, node2));
        assertEquals("[FAILED] Wrong previous node", node1, graph.getNexHop(node2, node3));
        
        graph.removeNode(node1);
        graph.removeNode(node1); // remove twice, it should not break.
        assertNull("[FAILED] The path shouldn't exist", graph.getNexHop(node2, node4));
        assertEquals("Wrong cost!", -1, graph.getCost(node2, node4));
        assertEquals("[FAILED] Should have 1 neighbor!", 1, graph.getNeighbors(node3).size());
        
        assertEquals("Failed complex dot representation, probably graph is wrong", "digraph javaGraph {\nnode1[label="
                + '"' + 2 + '"' + "];\nnode2[label=" + '"' + 3 + '"' + "];\nnode3[label=" + '"' + 4 + '"'
                + "];\nnode2 -> node3[label=4];\nnode3 -> node2[label=4];\n}\n", graph.toDotString());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testIllegalNodeCost1() {
        final Graph<Integer> graph = new Graph<Integer>();
        final Integer node1 = new Integer(1);
        graph.addNode(node1);
        graph.addNode(new Integer(2));
        graph.getCost(node1, new Integer(3));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testIllegalNodeCost2() {
        final Graph<Integer> graph = new Graph<Integer>();
        final Integer node1 = new Integer(1);
        graph.addNode(node1);
        graph.addNode(new Integer(2));
        graph.getCost(new Integer(3), node1);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testIllegalNodeNexHop() {
        final Graph<Integer> graph = new Graph<Integer>();
        final Integer node1 = new Integer(1);
        graph.addNode(node1);
        graph.addNode(new Integer(2));
        graph.getNexHop(node1, new Integer(3));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNullNodeHop1() {
        final Graph<Integer> graph = new Graph<Integer>();
        final Integer node1 = new Integer(1);
        graph.addNode(node1);
        graph.addNode(new Integer(0));
        graph.getNexHop(node1, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNullNodeHop2() {
        final Graph<Integer> graph = new Graph<Integer>();
        final Integer node1 = new Integer(1);
        graph.addNode(node1);
        graph.addNode(new Integer(0));
        graph.getNexHop(null, node1);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testEqualOriginDestination() {
        final Graph<Integer> graph = new Graph<Integer>();
        final Integer node1 = new Integer(1);
        graph.addNode(node1);
        graph.addNode(new Integer(0));
        graph.getNexHop(node1, node1);
    }
}
