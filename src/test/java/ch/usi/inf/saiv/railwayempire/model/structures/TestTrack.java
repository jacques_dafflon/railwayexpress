package ch.usi.inf.saiv.railwayempire.model.structures;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.usi.inf.saiv.railwayempire.model.datastructures.Graph;
import ch.usi.inf.saiv.railwayempire.utilities.Log;

public class TestTrack {
    
    private Graph<Track> graph;
    
    @Before
    public void initGraph() {
        this.graph = new Graph<Track>();
    }
    
    @Test
    public void testTrackCreation() {
        final Track track = new Track(10, 15, this.graph, TrackTypes.NORMAL);
        Assert.assertNotNull("[FAILED] Created Track should not be null", track);
    }
    
    @Test
    public void testAddRemoveNeighbor() {
        final Track track = new Track(10, 15, this.graph, TrackTypes.NORMAL);
        final Track neighbor1 = new Track(10, 16, this.graph, TrackTypes.NORMAL);
        final Track neighbor2 = new Track(11, 15, this.graph, TrackTypes.NORMAL);
        track.addNeighbor(neighbor1);
        Assert.assertEquals("[FAILED] Track should have one neighbor", 1, track.getNeighbors().size());
        track.addNeighbor(neighbor1);
        Assert.assertEquals("[FAILED] Track should add only one instance of the same track", 1, track.getNeighbors().size());
        track.addNeighbor(neighbor2);
        Assert.assertEquals("[FAILED] Track should have two neighbor", 2, track.getNeighbors().size());
        track.removeNeighbor(neighbor1);
        Assert.assertEquals("[FAILED] Track should have removed the track", 1, track.getNeighbors().size());
        track.removeNeighbor(neighbor1);
        Assert.assertEquals("[FAILED] Track should remove the track only once", 1, track.getNeighbors().size());
        track.removeNeighbor(neighbor2);
        Assert.assertEquals("[FAILED] Track should be empty now", 0, track.getNeighbors().size());
    }
    
    @Test
    public void testTrackGetPos() {
        final int x = 10;
        final int y = 15;
        final Track track = new Track(x, y, this.graph, TrackTypes.NORMAL);
        Assert.assertEquals("[FAILED] getX() failed", x, track.getX());
        Assert.assertEquals("[FAILED] getY() failed", y, track.getY());
    }
    
    @Test
    public void testTrackGraph() {
        final Track track2 = new Track(10, 10, this.graph, TrackTypes.NORMAL);
        final Track track3 = new Track(10, 11, this.graph, TrackTypes.NORMAL);
        final Track track4 = new Track(10, 12, this.graph, TrackTypes.NORMAL);
        final Track track5 = new Track(10, 13, this.graph, TrackTypes.NORMAL);
        final Track track6 = new Track(11, 13, this.graph, TrackTypes.NORMAL);
        final Track track7 = new Track(12, 13, this.graph, TrackTypes.NORMAL);
        final Track track8 = new Track(13, 13, this.graph, TrackTypes.NORMAL);
        final Track track9 = new Track(13, 12, this.graph, TrackTypes.NORMAL);
        final Track track10 = new Track(13, 11, this.graph, TrackTypes.NORMAL);
        final Track track11 = new Track(13, 10, this.graph, TrackTypes.NORMAL);
        final Track track12 = new Track(12, 10, this.graph, TrackTypes.NORMAL);
        final Track track13 = new Track(11, 10, this.graph, TrackTypes.NORMAL);
        final Track track14 = new Track(11, 11, this.graph, TrackTypes.NORMAL);
        final Track track15 = new Track(12, 11, this.graph, TrackTypes.NORMAL);
        final Track track16 = new Track(11, 12, this.graph, TrackTypes.NORMAL);
        final Track track17 = new Track(12, 12, this.graph, TrackTypes.NORMAL);
        
        track2.addNeighbor(track3);
        track3.addNeighbor(track2);
        
        track3.addNeighbor(track4);
        track4.addNeighbor(track3);
        
        track4.addNeighbor(track5);
        track5.addNeighbor(track4);
        
        track5.addNeighbor(track6);
        track6.addNeighbor(track5);
        
        track6.addNeighbor(track7);
        track7.addNeighbor(track6);
        
        track7.addNeighbor(track8);
        track8.addNeighbor(track7);
        
        track8.addNeighbor(track9);
        track9.addNeighbor(track8);
        
        track9.addNeighbor(track10);
        track10.addNeighbor(track9);
        
        track10.addNeighbor(track11);
        track11.addNeighbor(track10);
        
        track11.addNeighbor(track12);
        track12.addNeighbor(track11);
        
        track12.addNeighbor(track13);
        track13.addNeighbor(track12);
        
        track13.addNeighbor(track2);
        track2.addNeighbor(track13);
        
        track3.addNeighbor(track14);
        track14.addNeighbor(track3);
        
        track15.addNeighbor(track14);
        track14.addNeighbor(track15);
        
        track15.addNeighbor(track10);
        track10.addNeighbor(track15);
        
        track4.addNeighbor(track16);
        track16.addNeighbor(track4);
        
        track16.addNeighbor(track17);
        track17.addNeighbor(track16);
        
        track7.addNeighbor(track17);
        track17.addNeighbor(track7);
        
        Log.toFile(this.graph.toDotString(), "graph.dot");
        // file created: /tmp/graph.dot.
        // open it with graphviz or any other dot visualization software.
        
        Assert.assertEquals("[FAILED] The path should have the right cost", 3, this.graph.getCost(track3, track10));
        Assert.assertEquals("[FAILED] The path should have the right cost", 3, this.graph.getCost(track4, track7));
        Assert.assertEquals("[FAILED] The path should have the right cost", 1, this.graph.getCost(track3, track4));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void neighborTooBigXException() {
        final Track track = new Track(10, 10, this.graph, TrackTypes.NORMAL);
        track.addNeighbor(new Track(12, 10, this.graph, TrackTypes.NORMAL));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void neighborTooBigYException() {
        final Track track = new Track(10, 10, this.graph, TrackTypes.NORMAL);
        track.addNeighbor(new Track(10, 12, this.graph, TrackTypes.NORMAL));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void neighborTooSmallException() {
        final Track track = new Track(10, 10, this.graph, TrackTypes.NORMAL);
        track.addNeighbor(new Track(8, 10, this.graph, TrackTypes.NORMAL));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void neighborTooSmallYException() {
        final Track track = new Track(10, 10, this.graph, TrackTypes.NORMAL);
        track.addNeighbor(new Track(10, 8, this.graph, TrackTypes.NORMAL));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void diagonalNeighborException() {
        final Track track = new Track(10, 10, this.graph, TrackTypes.NORMAL);
        track.addNeighbor(new Track(11, 11, this.graph, TrackTypes.NORMAL));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void neighborSameTrack() {
        final Track track = new Track(10, 10, this.graph, TrackTypes.NORMAL);
        track.addNeighbor(new Track(10, 10, this.graph, TrackTypes.NORMAL));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionX() {
        new Track(-5, 10, this.graph, TrackTypes.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionY() {
        new Track(3, -10, this.graph, TrackTypes.NORMAL);
    }
}